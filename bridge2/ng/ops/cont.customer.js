/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/18/2016
 * Description: Customer page controller
 */

impulseBridge.controller('customerController', ['$scope', '$filter', '$sce', '$http', '$mdSidenav',
    function ($scope, $filter, $sce, $http, $mdSidenav) {
        $scope.allowed = false;
        $http({
            method: 'GET',
            url: 'https://impulsealarm.com/xapi/get_login_status'
        }).then(function successCallback(response) {
            $scope.status = response.data;
            var logStatus = $scope.status.status;
            if(logStatus == "loggedin") {
                $scope.allowed = true;
            } else {
                window.location.assign("#/start");
            }
        }, function errorCallback(response) {
            console.log(response.data);
        });
}]);