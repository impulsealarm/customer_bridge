/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/18/2016
 * Description: Main router of the app
 */
impulseBridge.config(function ($routeProvider) {
    $routeProvider
        .when('/start',
            {
                controller: 'mainController',
                templateUrl: 'views/pg.main.html',
                title: 'Start - Bridge'
            })
        .when('/customer',
            {
                controller: 'customerController',
                templateUrl: 'views/pg.customer.html',
                title: 'Customer - Bridge'
            })
        .when('/realtor/dashboard',
            {
                controller: 'realtorDashController',
                templateUrl: 'views/dashboards/dash.realtor.html',
                title: 'Realtor - Dashboard - Bridge'
            })
        .otherwise({ redirectTo: '/start' });
});