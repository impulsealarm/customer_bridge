/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/18/2016
 * Description: Toolbar global controller
 */

impulseBridge.controller('toolbarController',
    ['$scope', '$rootScope', '$filter', '$sce', '$http', '$mdSidenav', '$mdToast', '$document',
        function ($scope, $rootScope, $filter, $sce, $http, $mdSidenav, $mdToast, $document) {
            $scope.logMeOut = function () {
                console.log("clicked");
                $http({
                    method: 'GET',
                    url: 'https://impulsealarm.com/xapi/logout-json/'
                }).then(function successCallback(response) {
                    $scope.status = response.data;
                    setTimeout(function() {window.location.assign("#/start");}, 1500)
                }, function errorCallback(response){})
            }
        }]);

