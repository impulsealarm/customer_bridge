/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/18/2016
 * Description: Login controller
 */

impulseBridge.controller('loginController',
		['$scope', '$rootScope', '$filter', '$sce', '$http', '$mdSidenav', '$mdToast', '$document', '$location',
			function ($scope, $rootScope, $filter, $sce, $http, $mdSidenav, $mdToast, $document, $location) {

				$scope.showMessage = false;
				$scope.processLogin = false;

				$scope.logMeIn = function () {
					$scope.processLogin = true;
					user = $scope.user;
					pass = $scope.pass;
					$http({
						method: 'GET',
						url: 'https://impulsealarm.com/api/get_nonce/?controller=auth&method=generate_auth_cookie'
					}).then(function successCallback(response) {
						$scope.nonce = response.data;
						params = {
							"nonce": $scope.nonce.nonce,
							"username": $scope.user,
							"password": $scope.pass
						}
						$http({
							method: 'GET',
							url: 'https://impulsealarm.com/api/auth/generate_auth_cookie/',
							params: params
						}).then(function successCallback(response) {
							$scope.generate_auth_cookie = response.data;
							$scope.status = $scope.generate_auth_cookie.status;
							$scope.msgClass = $scope.status;
							$scope.processLogin = false;
							if($scope.status == "error") {
								console.warn("error: username and/or password incorrect");
								$scope.showMessage = true;
								$scope.message = $scope.generate_auth_cookie.error;
							}
							if($scope.status == "ok") {
								$scope.cookieAuth = $scope.generate_auth_cookie.cookie;
								$scope.cookieName = $scope.generate_auth_cookie.cookie_name;
								$http({
									method: 'GET',
									url: 'https://impulsealarm.com/api/auth/validate_auth_cookie/?cookie=' + $scope.cookieAuth
								}).then(function successCallback(response) {
									var urlenc = encodeURIComponent($scope.cookieAuth);
									document.cookie = $scope.cookieName + "=" + urlenc + "; expires=Thu, 18 Dec 2020 12:00:00 UTC; path=/";
									$scope.showMessage = true;
									$scope.message = "We're redirecting you to your page...";
									setTimeout(function() {
										window.location.assign("#/customer");
									}, 2500)
								}, function errorCallback(response){});
							}
						}, function errorCallback(response) {});
					}, function errorCallback(response) {

					});
				}

				$http({
					method: 'GET',
					url: 'https://impulsealarm.com/get_login_status'
				}).then(function successCallback(response) {

				}, function errorCallback(response) {
					console.log(response.data);
				});

}]);

impulseBridge.controller('messageCtrl', function($scope, $mdToast) {
  $scope.closeToast = function() {
    $mdToast.hide();
  };
});