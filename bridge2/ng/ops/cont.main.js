/*
 * Impulse Alarm Bridge V2.0
 * Last revision 02/18/2016
 * Description: Main Controller is used to show elements on the main page.
 */

impulseBridge.controller('mainController', ['$scope', '$rootScope', '$filter', '$sce', '$http', '$mdSidenav', 'apiImpulseBridgeLoginStatus', function ($scope, $rootScope, $filter, $sce, $http, $mdSidenav, apiImpulseBridgeLoginStatus) {
    $scope.loggedin = false;
    $scope.guest = false;
    $scope.permLoggedin = false;
    $http({
        method: 'GET',
        url: 'https://impulsealarm.com/xapi/get_login_status'
    }).then(function successCallback(response) {
        $rootScope.status = response.data;
        var logStatus = $rootScope.status.status;
        if(logStatus == "loggedin") {
            $scope.loggedin = true;
            setTimeout(function() {
                window.location.assign("#/customer");
                $scope.loggedin = false;
                $scope.guest = false;
                $scope.permLoggedin = true;
            }, 2500)
        } else {
            $scope.guest = true;
            setTimeout(function() {
                $(".loginBox").fadeIn("slow");
            }, 1500);
        }
    }, function errorCallback(response) {
        console.log(response.data);
    });
}]);