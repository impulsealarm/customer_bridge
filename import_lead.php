<?php

	// If a lead donot have phone and email then not to do anything. 
	include("wp-load.php");

	$form_id = "23";
	
	$total_leads = RGFormsModel::get_form_counts( $form_id );
	
	$total_leads = $total_leads['total'];
	
	$i = 0;
	
	$lead_not_associate_realtor = array();
		
	while($i<=$total_leads){
		
		$offset = $i;
			
		$leads = GFFormsModel::get_leads($form_id, "0", 'DESC', '', $offset);
		
		$i += 30;	
		
		$form = GFFormsModel::get_form_meta($form_id); 
	
	    foreach($leads as $lead){
	
	    	$lead_values = array();
	    	
	    	$lead_values['source_url'] = $lead['source_url'];
		    
	    	foreach( $form['fields'] as $field ) {
		    	
		    	$field_label = $field['label'];
		    		
		    	if(!empty($field['inputs']) && $field_label != "Time"){
		    		
		    		foreach($field['inputs'] as $sub_field){
		    		
		    			$sublead_label = $field_label." ".$sub_field['label'];
		    			
		    			$lead_values[$sublead_label] =  $lead[$sub_field['id']];
		    			
		    		}
		    	} else {
		    		
		    		if($field_label == "Time" && $lead[$field['id']] != ""){
		    			
		    			if($field['timeFormat'] == "12")
		    				$lead_values[$field['label']] = date("H:i:s", strtotime($lead[$field['id']]));
		    			else
		    				$lead_values[$field['label']] = $lead[ $field['id'] ];
		    		} else {
		    			$lead_values[$field['label']] = $lead[ $field['id'] ];
		    		}
		    	}
		    }
		    
		    if(!empty($lead_values)){
		    	
		    	$response = CreateLead($lead_values);
		    	
				if(
		    		!empty($response) && isset($response['lead_id']) && $response['lead_id'] != '' &&
		    		isset($response['realtorid']) && $response['realtorid'] == '0'
		    	){
		    		$lead_not_associate_realtor[$lead['id']] = $response['lead_id'];
		    	}
		    }
		}
	}
	
	if(!empty($lead_not_associate_realtor)){
		print_r("List of leads not associate with realtor are : ");
		echo '<br><pre>';
		print_r($lead_not_associate_realtor);
		echo '</pre>';
	}
		
	exit;
	
    function CreateLead($data){
    	
    	$lead_data = array();
    	
    	$field_mapping = array(
    		"realtor_phone" => "Realtor Phone",
    		"realtor_email" => "Realtor Email",
    		"lead_first_name" => "Name First",
			"lead_last_name" => "Name Last",
    		"lead_email" => "Email",
    		"lead_phone" => "Phone",
    		"sales_rep_email" => "Sales Rep Email",
			"lead_notes" => "Lead Notes",
			"realor_gift_notes" => "Realtor Gift Notes",
			"lead_call_back" => "Callback Date",
			"lead_call_back_time" => "Time",
			"lead_zip" => "Service Address ZIP / Postal Code",
			"lead_city" => "Service Address City",
			"lead_state" => "Service Address State / Province",
			"lead_address_line1" => "Service Address Street Address",
			"lead_address_line2" => "Service Address Address Line 2",
    		"lead_country" => "Service Address Country",
    		"source_url" => "source_url"
		);

		foreach($field_mapping as $field_name => $field_label){
			$lead_data[$field_name] = $data[$field_label];
		}
		
		$url = "https://crm.impulsealarm.com/import_lead.php";
			
		$ch = curl_init();
			
		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_POST, true);
        
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $lead_data);
          	
		$response = curl_exec($ch);
			
		if(!$response)
			$response = curl_error($ch);
		
		curl_close($ch);
		
		$response = json_decode($response, true);
		
		return $response;
    }
?>