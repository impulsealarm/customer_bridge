<!DOCTYPE html>
<!-- HTML5 Hello world by kirupa - http://www.kirupa.com/html5/getting_your_feet_wet_html5_pg1.htm -->
<html lang="en-us">

<head>
<meta charset="utf-8">
<title>Impulse Alarm</title>
<link rel="shortcut icon" type="image/ico" href="https://impulsealarm.com/wp-content/themes/flatsome/favicon.ico" />
<link rel="stylesheet" type="text/css" href="https://impulsealarm.com/wp-content/themes/flatsome/capture.css">
<link href='https://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,500,300,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300,100' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Nunito:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<?php
if (is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
	echo "<script type='text/javascript'>";
	echo 'var affwp_scripts = {"ajaxurl":"'.site_url().'/wp-admin/admin-ajax.php"};';
	echo "</script> \n";
	echo '<script src="'.AFFILIATEWP_PLUGIN_URL . 'assets/js/tracking.min.js"></script>';
	echo "\n";
	echo '<script src="'.AFFILIATEWP_PLUGIN_URL . 'assets/js/jquery.cookie.min.js"></script>';
	echo "\n";
	$tracking = affiliate_wp()->tracking->header_scripts();
}
?>

    <?php
    $url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    $parts = Explode('/', $url);
    $user_url = $parts[3];
    $referrer = get_user_by( 'login', $user_url );
    ?>
    <script>
        !function(){var a,b,c,d=window,e=document,f=arguments,g="script",h=["config","track","trackForm","trackClick","identify","visit","push","call"],i=function(){var a,b=this,c=function(a){b[a]=function(){return b._e.push([a].concat(Array.prototype.slice.call(arguments,0))),b}};for(b._e=[],a=0;a<h.length;a++)c(h[a])};for(d.__woo=d.__woo||{},a=0;a<f.length;a++)d.__woo[f[a]]=d[f[a]]=d[f[a]]||new i;b=e.createElement(g),b.async=1,b.src="//static.woopra.com/js/w.js",c=e.getElementsByTagName(g)[0],c.parentNode.insertBefore(b,c)}("woopra");

        woopra.config({
            domain: "impulsealarm.com"
        });

        woopra.track();

        jQuery(document).ready(function($){
            telephone = jQuery("input[type=tel]");
            telephone.change(function() {
                telname = $(this).attr("name");
                telnumber = $(this).val();
                $(this).after("<input type=hidden name=" + telname + "_h value="+telnumber+" />") ;
            });
            email = jQuery("input[type=email]");
            email.change(function() {
                emname = $(this).attr("name");
                emval = $(this).val();
                $(this).after("<input type=hidden name=" + emname + "_h value="+emval+" />") ;
                $(this).after("<input type=hidden name=name value=\"<?php echo $referrer->first_name . ' ' .  $referrer->last_name; ?>\" />") ;
            });
        });
    </script>

</head>


<body>

    <div class="body">

        <div class="header">

            <div class="header-inner">
              <div class="logo">
                <img src="https://impulsealarm.com/wp-content/uploads/2016/01/impulse-white-1.png" alt="" />
              </div>
              <div class="adt-logo">
                <img src="https://impulsealarm.com/wp-content/uploads/2016/01/adt-white.png" alt="" />
              </div>
              <div class="phone">
                <span>866-266-6908</span>
              </div>

              <div class="clear">

              </div>
            </div>
        </div>

        <div class="container">
            <div class="container-box">


                    <div class="price-table col-3">
                        <div class="price-title blue">
                            <h2>Pulse Security</h1>
                        </div>

                        <div class="price-out">
                          <img src="https://impulsealarm.com/wp-content/uploads/2016/01/price-out-1.png" alt="" />
                        </div>

                        <div class="price-box blue">
                            <h1>$49.99</h1>
                        </div>


                        <div>
                           <i class="fa fa-check-circle"></i>
                            <span class="super">Waived Activation</span>
                        </div>

                        <div>
                            <i class="icon-1 super"></i>
                            <span>9057 Keypanel with Included 2 Way Voice / Verified Response</span>
                        </div>

                        <div>
                           <i class="icon-7"></i>
                            <span>Wireless door/window points</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Motion Sensor</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Keychain Remote</span>
                        </div>

                        <div>
                            <i class="fa fa-times-circle grey"></i>
                            <span class="super">Light Control</span>
                        </div>

                        <div>
                           <i class="fa fa-times-circle grey"></i>
                            <span class="super">Outdoor Camera</span>
                        </div>

                        <div id="picksec" class="pick" onclick="formPop()" onmouseover="formZoom()" onmouseout="formZoomOut()">
                            Pick Security
                        </div>

                    </div>

                    <div class="price-table col-3">
                        <div class="price-title blue">
                            <h2>Pulse Video</h1>
                        </div>

                        <div class="price-out">
                          <img src="https://impulsealarm.com/wp-content/uploads/2016/01/price-out-2.png" alt="" />
                        </div>

                        <div class="price-box blue">
                            <h1>$58.99</h1>
                        </div>


                        <div>
                           <i class="fa fa-check-circle"></i>
                            <span class="super">Waived Activation</span>
                        </div>

                        <div>
                            <i class="icon-1 super"></i>
                            <span>9057 Keypanel with Included 2 Way Voice / Verified Response</span>
                        </div>

                        <div>
                           <i class="icon-7"></i>
                            <span>Wireless door/window points</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Motion Sensor</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Keychain Remote</span>
                        </div>

                        <div>
                            <i class="fa fa-check-circle"></i>
                            <span class="super">Light Control</span>
                        </div>

                        <div>
                           <i class="fa fa-check-circle"></i>
                            <span class="super">Outdoor Camera</span>
                        </div>

                        <div id="pickvid" class="pick" onclick="formPop()" onmouseover="formZoom()" onmouseout="formZoomOut()">
                            Pick Video
                        </div>

                    </div>



                <div id="form-container" class="form-container col-3">
                  <h2>Let us call you</h2>
                  <?php echo do_shortcode('[gravityform id="35" title="false" description="false"]'); ?>

                </div> <!--form-container-->

                <div class="clear">

                </div>
            </div> <!--container-box-->
        </div> <!--container-->

        <div class="features">
          <div class="row">
            <div class="column">
              <h2>Two Way Voice</h2>
              <p>
                In recent years many cities and counties throughout California have begun to require or suggest that a homeowners alarm system support features that allow for a verified response.  That's why both of these packages include ADT's Two Way Voice at no additional charge.  With two way voice, ADT will attempt to call you directly through your keypanel.  If you cannot verbally verify your abort code or the ADT operator believes you are under any kind of duress, the police will be dispatched to your residence for a distress alarm (that means more cops, a lot faster).  And don't worry, if you aren't home ADT's still going to call your cell and anyone else you list on your account.
              </p>
            </div>
            <div class="column">
              <h2>Burglary Monitoring</h2>
              <p>
                Your home and your life demand unwavering security. Sensors on your windows and doors connect to your alarm, and to our trained professionals, so you can keep your stress-level down and intruders out.  With the included Pulse App, you'll see everything ADT sees, down to the zone and sensor that triggered an alarm.  All of it at your finger tips in real time, right on your mobile or tablet.
              </p>
            </div>
            <div class="column">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
          </div>

          <div class="row">
            <div class="column">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
            <div class="column">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
            <div class="column">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
          </div>

          <div class="clear">

          </div>
        </div>

        </div>

        <div class="policies">
          <div class="policies-inner">
                    <h2>Thanks for choosing to work with Impulse Alarm LLC!</h2>
                    <p style="text-align: justify">$99.00 Customer Installation Charge. 24-Month Monitoring Agreement required at $49.99 per month ($1,199.76). Form of payment must be by credit card or electronic charge to your checking or savings account. Offer applies to homeowners only. Local permit fees may be required. Satisfactory credit history required. Certain restrictions may apply. Offer valid for new ADT Authorized Dealer customers only and not on purchases from ADT LLC. Other rate plans available. Cannot be combined with any other offer. ADT Pulse Interactive Solutions Services, which help you manage your home environment and family lifestyle, requires the purchase and/or activation of an ADT alarm system with monitored burglary service and a compatible computer, cell phone or PDA with Internet and email access. These ADT Pulse Interactive Solutions Services do not cover the operation or maintenance of any household equipment/systems that are connected to the ADT Pulse Interactive Solutions Services/Equipment. All ADT Pulse Interactive Solutions Services are not available with the various levels of ADT Pulse Interactive Solutions Services. All ADT Pulse Interactive Solutions Services may not be available in all geographic areas. Standard message and data rates may apply to text alerts. You may be required to pay additional charges to purchase equipment required to utilize the ADT Pulse Interactive Solutions Services features you desire.</p>
          </div>
        </div>

        <div class="footer">
          <div class="footer-inner">
                <div class="copyright">
                    <br>
                    Copyright 2016 &copy; <a href="https://impulsealarm.com/" target="_new">Impulse Alarm</a>. An <a href="http://www.adt.com/" target="_new">ADT</a> Authorized Dealer.
                </div>
                <div class="powered">
                    <a href="https://secure.comodo.com/ttb_searcher/trustlogo?v_querytype=W&v_shortname=SCAS&v_search=https://impulsealarm.com/account-information/&x=6&y=5" target="_new" class="powered-image-link"><img src="https://impulsealarm.com/wp-content/uploads/2016/01/comodo-1.png"></img></a>
                    <a href="https://stripe.com/" target="_new" class="powered-image-link"><img src="https://impulsealarm.com/wp-content/uploads/2016/01/stripe-1.png"></img></a>
                </div>
          </div>
        </div> <!--footer-->





        <script type="text/javascript">

        var blockIt = false;
        //var flag = false;
          function formPop() {
            document.getElementById("form-container").style.transform = "scale(1.05)";
            document.getElementById('input_35_7').focus();
            window.blockIt = true;
          }

          function formZoom() {
            if(!blockIt){
              document.getElementById("form-container").style.transform = "scale(1.05)";
            }

          }

          function formZoomOut() {
            if(!blockIt){
              //if(!flag){
                document.getElementById("form-container").style.transform = "scale(1)";
                ///console.log('zoomOut');
              //}else{
                //window.flag = false;
              //}

            }
          }

          document.getElementsByTagName('body')[0].onclick = function( e ) {
          if(e.target != document.getElementById('form-container')) {

            switch(e.target){
              case document.getElementById('pickvid'):
          document.getElementById("form-container").style.transform = "scale(1.05)";
          document.getElementById('input_35_7').focus();
          break;
          case document.getElementById('picksec'):
          document.getElementById("form-container").style.transform = "scale(1.05)";
          document.getElementById('input_35_7').focus();
          break;
          default:
          document.getElementById("form-container").style.transform = "scale(1)";
          window.blockIt = false;
            }
          }
        }
        </script>


</body>
</html>
