<!DOCTYPE html>
<!-- HTML5 Hello world by kirupa - http://www.kirupa.com/html5/getting_your_feet_wet_html5_pg1.htm -->
<html lang="en-us">

<head>
<meta charset="utf-8">
<title>Impulse alarm</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/css/foundation.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.2/js/foundation.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
<link rel="stylesheet" type="text/css" href="https://impulsealarm.com/wp-content/themes/flatsome/landing.css">
<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,500,300,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300,100' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Nunito:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>


<body>
    <div class="body">

        <div class="header">

            <div class="header-inner">
              <div class="logo">
                <img src="https://impulsealarm.com/wp-content/uploads/2016/01/impulse-white.png" alt="" />
              </div>
              <div class="adt-logo">
                <img src="https://impulsealarm.com/wp-content/uploads/2016/01/adt-white.png" alt="" />
              </div>
              <div class="phone">
                <span>866-266-6908</span>
              </div>
            </div>
        </div>

        <div class="container">
            <div class="container-box">


                    <div class="price-table large-4">
                        <div class="price-title blue">
                            <h2>Pulse Security</h1>
                        </div>

                        <div class="price-out">
                          <img src="https://impulsealarm.com/wp-content/uploads/2016/01/price-out-1.png" alt="" />
                        </div>

                        <div class="price-box blue">
                            <h1>$49.99</h1>
                        </div>


                        <div>
                           <i class="fa fa-check-circle"></i>
                            <span class="super">Waived Activation</span>
                        </div>

                        <div>
                            <i class="icon-1 super"></i>
                            <span>9057 Keypanel with Included 2 Way Voice / Verified Response</span>
                        </div>

                        <div>
                           <i class="icon-7"></i>
                            <span>DW Contacts</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Motion Sensor</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Keychain Remote</span>
                        </div>

                        <div>
                            <i class="fa fa-times-circle grey"></i>
                            <span class="super">Light Control</span>
                        </div>

                        <div>
                           <i class="fa fa-times-circle grey"></i>
                            <span class="super">Outdoor Camera</span>
                        </div>

                        <div class="pick">
                            <span><a>Pick Security</a></span>
                        </div>

                    </div>

                    <div class="price-table large-4">
                        <div class="price-title blue">
                            <h2>Pulse Video</h1>
                        </div>

                        <div class="price-out">
                          <img src="https://impulsealarm.com/wp-content/uploads/2016/01/price-out-2.png" alt="" />
                        </div>

                        <div class="price-box blue">
                            <h1>$58.99</h1>
                        </div>


                        <div>
                           <i class="fa fa-check-circle"></i>
                            <span class="super">Waived Activation</span>
                        </div>

                        <div>
                            <i class="icon-1 super"></i>
                            <span>9057 Keypanel with Included 2 Way Voice / Verified Response</span>
                        </div>

                        <div>
                           <i class="icon-7"></i>
                            <span>DW Contacts</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Motion Sensor</span>
                        </div>

                        <div>
                            <i class="icon-1"></i>
                            <span>Keychain Remote</span>
                        </div>

                        <div>
                            <i class="fa fa-check-circle"></i>
                            <span class="super">Light Control</span>
                        </div>

                        <div>
                           <i class="fa fa-check-circle"></i>
                            <span class="super">Outdoor Camera</span>
                        </div>

                        <div class="pick">
                            <span><a>Pick Video</a></span>
                        </div>

                    </div>



                <div class="form-container large-3">
                  <h2>Let us call you</h2>
                  <?php echo do_shortcode('[gravityform id="35" title="false" description="false"]'); ?>

                </div> <!--form-container-->
            </div> <!--container-box-->
        </div> <!--container-->

        <div class="features">
          <div class="">
            <div class="large-4 columns">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
            <div class="large-4 columns">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
            <div class="large-4 columns">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
          </div>

          <div class="">
            <div class="large-4 columns">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
            <div class="large-4 columns">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
            <div class="large-4 columns">
              <h2>Lorem Ipsum</h2>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
          </div>
        </div>

        </div>

        <div class="footer">
          <div class="footer-inner">
            <?php echo "Copyright 2016 © Impulse Alarm. An ADT Authorized Dealer"; ?>
          </div>

        </div> <!--footer-->





    </div>


</body>
</html>
