<?php
/*
Template name: Slider
*/?>

<!DOCTYPE html>
<!-- HTML5 Hello world by kirupa - http://www.kirupa.com/html5/getting_your_feet_wet_html5_pg1.htm -->
<html lang="en-us" ng-app>

<head>
<meta charset="utf-8">
<title>Impulse alarm</title>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://impulsealarm.com/wp-content/themes/flatsome/css/slider.css">
<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,500,300,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,300,100' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Nunito:400,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>


<body>
    <div class="body">

        <div class="header">

            <div class="header-inner">
              <div class="logo">
                <img src="https://impulsealarm.com/wp-content/uploads/2016/01/impulse-white.png" alt="" />
              </div>
              <div class="adt-logo">
                <img src="https://impulsealarm.com/wp-content/uploads/2016/01/adt-white.png" alt="" />
              </div>
              <div class="phone">

                <span>866-266-6908</span>
              </div>
            </div>
        </div>

        <div class="container">
                           
          
                
            
            <div class="price-table-container">
              <div class="price-table" id="price-one">
                  <div class="price-title blue">
                    <h2 id="plan-holder"></h2>
                  </div>
                          
                      <div class="price-box">
                          <h1 id="price"></h1>
                      </div>
                          
                          
                      <div>
                         <i class="fa fa-check-circle"></i>
                          <span class="super" id="waived-activation"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1 super"></i>
                          <span id="keypanel"></span>
                      </div>
                      
                      <div>
                         <i class="icon-7 super"></i>
                          <span id="contacts"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="motion"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="remonte"></span>
                      </div>
                      
                      <div>
                          <i class="fa fa-times-circle grey" id="check"></i>
                          <span class="super" id="light-control"></span>
                      </div>
                      
                      <div>
                         <i class="fa fa-times-circle grey" id="check1"></i>
                          <span class="super" id="outdoor-camera"></span>
                      </div>
                      <div class="pick">
                          <span id="pick"></span>
                      </div>
                      <br>
                      <input type="text" ng-model="username" id="username" placeholder="Enter your username" value="">
                      <br>
                      <input id="copyTarget" value="https://impulsealarm.com/{{username}}"> <button id="copyButton">Copy</button>
                  
                </div>

                <div class="price-table" id="price-two">
                  <div class="price-title blue">
                    <h2 id="plan-holder1"></h2>
                  </div>
                          
                      <div class="price-box">
                          <h1 id="price1"></h1>
                      </div>
                      <div>
                         <i class="fa fa-check-circle"></i>
                          <span class="super" id="waived-activation1"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1 super"></i>
                          <span id="keypanel1"></span>
                      </div>
                      
                      <div>
                         <i class="icon-7 super"></i>
                          <span id="contacts1"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="motion1"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="remonte1"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1" id="check1"></i>
                          <span class="super" id="light-control1"></span>
                      </div>
                      
                      <div>
                         <i class="icon-1" id="check11"></i>
                          <span class="super" id="outdoor-camera1"></span>
                      </div>
                      <div class="pick">
                          <span id="pick1"></span>
                      </div>
                      <br>
                      <input type="text" ng-model="username" id="username" placeholder="Enter your username" value="">
                      <br>
                      <input id="copyTarget" value="https://impulsealarm.com/ref/{{username}}"> <button id="copyButton">Copy</button>
                </div>


                <div class="price-table" id="price-three">
                  <div class="price-title blue">
                    <h2 id="plan-holder2"></h2>
                  </div>
                          
                      <div class="price-box">
                          <h1 id="price2"></h1>
                      </div>
                          
                          
                      <div>
                         <i class="fa fa-check-circle"></i>
                          <span class="super" id="waived-activation2"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1 super"></i>
                          <span id="keypanel2"></span>
                      </div>
                      
                      <div>
                         <i class="icon-7 super"></i>
                          <span id="contacts2"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="motion2"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="remonte2"></span>
                      </div>
                      
                      <div>
                          <i class="fa fa-times-circle grey" id="check2"></i>
                          <span class="super" id="light-control2"></span>
                      </div>
                      
                      <div>
                         <i class="fa fa-times-circle grey" id="check12"></i>
                          <span class="super" id="outdoor-camera2"></span>
                      </div>
                      <div class="pick">
                          <span id="pick2"></span>
                      </div>
                      <br>
                      <input type="text" ng-model="username" id="username" placeholder="Enter your username" value="">
                      <br>
                      <input id="copyTarget" value="https://impulsealarm.com/ref/{{username}}"> <button id="copyButton">Copy</button>
                  
                </div>

                <div class="price-table" id="price-four">
                  <div class="price-title blue">
                    <h2 id="plan-holder13"></h2>
                  </div>
                          
                      <div class="price-box">
                          <h1 id="price13"></h1>
                      </div>
                      <div>
                         <i class="fa fa-check-circle"></i>
                          <span class="super" id="waived-activation13"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1 super"></i>
                          <span id="keypanel13"></span>
                      </div>
                      
                      <div>
                         <i class="icon-7 super"></i>
                          <span id="contacts13"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="motion13"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1"></i>
                          <span id="remonte13"></span>
                      </div>
                      
                      <div>
                          <i class="icon-1" id="check13"></i>
                          <span class="super" id="light-control13"></span>
                      </div>
                      
                      <div>
                         <i class="icon-1" id="check113"></i>
                          <span class="super" id="outdoor-camera13"></span>
                      </div>
                      <div class="pick">
                          <span id="pick13"></span>
                      </div>
                      <br>
                      <input type="text" ng-model="username" id="username" placeholder="Enter your username" value="">
                      <br>
                      <input id="copyTarget" value="https://impulsealarm.com/ref/{{username}}"> <button id="copyButton">Copy</button>
                </div>
                
                
            </div>
            </div>


        </div>
         <div class="range-holder">
                <div id="pr-slider" class="dragdealer">
                <div class="stripe">

                <div id="green-highlight"></div>
                <div id="orange-highlight"></div>
                <div id="blue-highlight"></div>

                
                
                <div class="handle">
                  <div class="square">
                    <span class="value"></span>
                    <span class="menu-line"></span>
                    <span class="menu-line"></span>
                    <span class="menu-line"></span>
                  </div>  
                </div> 
              </div>
      </div>
    
        </div> <!--container-->

        

        </div>

        <div class="footer">
          <div class="footer-inner">
            <?php echo "Copyright 2016 © Impulse Alarm. An ADT Authorized Dealer"; ?>
          </div>

        </div> <!--footer-->





    </div>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script type="text/javascript" src="https://impulsealarm.com/wp-content/themes/flatsome/js/slider.js"></script>

</body>
</html>
<script type="text/javascript">
document.getElementById("copyButton").addEventListener("click", function() {
    copyToClipboardMsg(document.getElementById("copyTarget"), "msg");
});

document.getElementById("copyButton2").addEventListener("click", function() {
    copyToClipboardMsg(document.getElementById("copyTarget2"), "msg");
});

document.getElementById("pasteTarget").addEventListener("mousedown", function() {
    this.value = "";
});


function copyToClipboardMsg(elem, msgElem) {
    var succeed = copyToClipboard(elem);
    var msg;
    if (!succeed) {
        msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
    } else {
        msg = "Text copied to the clipboard."
    }
    if (typeof msgElem === "string") {
        msgElem = document.getElementById(msgElem);
    }
    msgElem.innerHTML = msg;
    setTimeout(function() {
        msgElem.innerHTML = "";
    }, 2000);
}

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
</script>