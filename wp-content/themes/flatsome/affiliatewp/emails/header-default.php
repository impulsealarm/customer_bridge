<?php
/**
 * Email Header
 *
 * @package AffiliateWP/Templates/Emails
 * @version 1.6
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Arvo:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700,300" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:400,500,700,300,300italic,400italic,500italic,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css" />
    <style type="text/css">a {
                text-decoration: none !important;
                color: #0064A9;
            }
            body {
                background-color: #EAECED;
            }
            
            .container {
                max-width: 700px;
                margin: 0 auto;
                height: auto;
                color: #7e8890;
                font-family: 'Alegreya Sans', sans-serif;
                padding-top: 30px;
            }
            
            .header {
                border-radius: 6px 6px 0px 0px;
                padding-top: 2% !important;
                height: 180px;
                background-image: url("https://impulsealarm.com/wp-content/uploads/2016/01/impulse-email-header-bg.jpg");
                
            }
            
            .impulse-logo {
                width: 100%;
                height: 60px;
                padding-top:50px;
                text-align: center;
            }
            
            .adt-logo {
                float: right;
                vertical-align: bottom;
            }
            
            .adt-logo img {
                
            }
            
            .impulse-logo img {
                
            }
            
            .content {
                
                line-height: 24px;
                font-size: 0.9em;

            }
            
            .body h2 {
                font-family: 'Arvo';
                font-size: 20px;
                font-weight: 500;
                color: #0061a8;
            }
            
            .body {
                padding-top: 30px !important;
                padding-bottom: 30px !important;
                overflow: auto;
                background-color: #f8f8f8;
                text-align: justify;
            }
            
            .padding-side {
                padding-right:10%;
                padding-left: 10%;
            }
            .footer {
                
            }
            .signature {
                border-top: 2px solid #DADADA;
                padding-top: 20px;
                padding-bottom: 20px;
                background-color: #f8f8f8;
                overflow: auto;
            }
            
            .signature-text {
                color: #7e8890;
                display: inline-block;
                font-size: 0.8em;
            }
            
            .signature-logo {
                display: inline-block;
                float: right;
            }
            
            .signature-logo img {
                height: 50px;
            }
            
            .blue {
                color: #0061A8;
            }
            .policies {
                background-color: #0061a8;
                overflow: auto;
                color: #fff;
                font-size: 11px;
                font-weight: 300;
                border-radius: 0px 0px 6px 6px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .copyright, .powered {
                text-align: center;
                
                padding: 20px;
                font-size: 12px ;
            }
            .powered-image-link {
                display: inline-block;
            }
            
            .powered a {
                padding:0px 5px;
            }
            @media only screen and (-webkit-min-device-pixel-ratio: 2) and (max-width: 420px) {
                .header {
                    background-size: cover !important;
                    height: 120px !important;
                    
                }
                
               .container {
                    padding-top: 0px !important;
                    width: 100% !important;
                }
                .impulse-logo {
                    padding-top: 5% !important;
                }
                .impulse-logo img {
                    width: 65% !important;
                }
                .adt-logo {
                    float: none !important;
                    text-align: right !important;
                }
                .adt-logo img {
                    padding-top: 0.7% !important;
                    
                }
                .padding-side {
                    padding-right:6% !important;
                    padding-left: 6% !important;
                }
                
                
                .signature {
                    padding-top: 40px !important;
                    padding-bottom: 40px !important;
                }
                .signature-text {
                    width: 70% !important;
                }
                .signature-logo {
                    width: 20% !important;
                }
                .signature-logo img {
                    height: 30px !important;
                }
                .copyright {
                    padding: 20px 0 !important;
                    font-size: 1.1em !important;
                }
                .powered a {
                    padding: 0px 11% !important;
                    width: 27% !important;
                }
                .powered img {
                    width: 100% !important;
                }
                
                .policies {
                    font-size: 0.8em !important;
                    text-align: justify !important;
                }
            }
    </style>
</head>
<body class="scayt-enabled" style="background-color:#EAECED;">
<div class="container" style="max-width:700px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;height:auto;color:#7e8890;font-family:'Alegreya Sans', sans-serif;padding-top:30px;">
<div class="header padding-side" style="border-radius:6px 6px 0px 0px;padding-top:2% !important;height:180px;background-image:url('https://impulsealarm.com/wp-content/uploads/2016/01/impulse-email-header-bg.jpg');padding-right:10%;padding-left:10%;">
<div class="impulse-logo" style="width:100%;height:60px;padding-top:50px;text-align:center;"><a href="https://impulsealarm.com/" style="text-decoration:none !important;color:#0064A9;" target="_new"><img src="https://impulsealarm.com/wp-content/uploads/2016/02/bridge-impulse-1.png" /></a></div>

<div class="adt-logo" style="float:right;vertical-align:bottom;"><a href="http://www.adt.com" style="text-decoration:none !important;color:#0064A9;" target="_new"><img src="https://impulsealarm.com/wp-content/uploads/2016/01/logo-adt-email.png" /></a></div>
</div>
<!--header-->

<div class="body padding-side" style="padding-top:30px !important;padding-bottom:30px !important;overflow:auto;background-color:#f8f8f8;text-align:justify;padding-right:10%;padding-left:10%;">
