<?php
/**
 * Email Footer
 *
 * @package AffiliateWP/Templates/Emails
 * @version 1.6
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


?>
</div>
<!--body-->

<div class="signature padding-side" style="padding-right:10%;padding-left:10%;border-top-width:2px;border-top-style:solid;border-top-color:#DADADA;padding-top:20px;padding-bottom:20px;background-color:#f8f8f8;overflow:auto;">
<div class="signature-text" style="color:#7e8890;display:inline-block;font-size:0.8em;"><b>David Hatfield</b>, COO<br />
<a href="mailto:david@impulsealarm.com" style="text-decoration:none !important;color:#0064A9;" target="_new">david@impulsealarm.com</a> | <a href="tel:866-266-6908" style="text-decoration:none !important;color:#0064A9;" target="_new">866-266-6908</a><br />
<br />
<a href="https://impulsealarm.com/" style="text-decoration:none !important;color:#0064A9;" target="_new"><b class="blue" style="color:#0061A8;">Impulse Alarm LLC</b></a> | <a href="tel:866-266-6908" style="text-decoration:none !important;color:#0064A9;" target="_new">866-266-6908</a><br />
4101 Dublin Blvd Ste F #405 Dublin, CA 94568</div>

<div class="signature-logo" style="display:inline-block;float:right;"><a href="https://impulsealarm.com/" style="text-decoration:none !important;color:#0064A9;" target="_new"><img height="75" src="https://impulsealarm.com/wp-content/uploads/2016/02/bridge-grey-no-impulse-75.png" style="height:50px;" /></a></div>
</div>
<!--signature-->

<div class="policies padding-side" style="padding-right:10%;padding-left:10%;background-color:#0061a8;overflow:auto;color:#fff;font-size:11px;font-weight:300;border-radius:0px 0px 6px 6px;padding-top:20px;padding-bottom:20px;">
<p>Thanks for choosing to work with Impulse Alarm LLC!</p>

<p><span style="font-size:8px;">$99.00 Customer Installation Charge. 24-Month Monitoring Agreement required at $49.99 per month ($1,199.76). Form of payment must be by credit card or electronic charge to your checking or savings account. Offer applies to homeowners only. Local permit fees may be required. Satisfactory credit history required. Certain restrictions may apply. Offer valid for new ADT Authorized Dealer customers only and not on purchases from ADT LLC. Other rate plans available. Cannot be combined with any other offer. ADT Pulse Interactive Solutions Services, which help you manage your home environment and family lifestyle, requires the purchase and/or activation of an ADT alarm system with monitored burglary service and a compatible computer, cell phone or PDA with Internet and email access. These ADT Pulse Interactive Solutions Services do not cover the operation or maintenance of any household equipment/systems that are connected to the ADT Pulse Interactive Solutions Services/Equipment. All ADT Pulse Interactive Solutions Services are not available with the various levels of ADT Pulse Interactive Solutions Services. All ADT Pulse Interactive Solutions Services may not be available in all geographic areas. Standard message and data rates may apply to text alerts. You may be required to pay additional charges to purchase equipment required to utilize the ADT Pulse Interactive Solutions Services features you desire.</span></p>
</div>

<div class="footer padding-side" style="padding-right:10%;padding-left:10%;">
<div class="copyright" style="text-align:center;padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;font-size:12px;">
Copyright 2016 &copy; <a href="https://impulsealarm.com/" style="text-decoration:none !important;color:#0064A9;" target="_new">Impulse Alarm</a>. An <a href="http://www.adt.com/" style="text-decoration:none !important;color:#0064A9;" target="_new">ADT</a> Authorized Dealer.</div>

<div class="powered" style="text-align:center;padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;font-size:12px;"><a class="powered-image-link" href="https://secure.comodo.com/ttb_searcher/trustlogo?v_querytype=W&amp;v_shortname=SCAS&amp;v_search=https://impulsealarm.com/account-information/&amp;x=6&amp;y=5" style="text-decoration:none !important;color:#0064A9;display:inline-block;padding-top:0px;padding-bottom:0px;padding-right:5px;padding-left:5px;" target="_new"><img src="https://impulsealarm.com/wp-content/uploads/2016/01/comodo-1.png" /></a> <a class="powered-image-link" href="https://stripe.com/" style="text-decoration:none !important;color:#0064A9;display:inline-block;padding-top:0px;padding-bottom:0px;padding-right:5px;padding-left:5px;" target="_new"><img src="https://impulsealarm.com/wp-content/uploads/2016/01/stripe-1.png" /></a></div>
</div>
<!--footer--></div>
<!--container--></body>
</html>