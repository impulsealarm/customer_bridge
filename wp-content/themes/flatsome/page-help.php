<?php
/*
Template name: Help
*/
get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div class="page-wrapper page-right-sidebar">
<div class="row">

<div id="content" role="main">
	<div class="page-inner">

			<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() )
							comments_template();
					?>

			<?php endwhile; // end of the loop. ?>

	</div><!-- .page-inner -->
</div><!-- .#content large-9 left -->
<div class="help-categories">

	<?php if ( is_active_sidebar( 'widget-one' ) ) : ?>
	<div style="display: block;">
		<div class="large-4 columns">
	   		<?php dynamic_sidebar('widget-one'); ?>        
		</div><!-- end row -->
	</div><!-- end widget -->
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'widget-two' ) ) : ?>
	<div style="display: block;">
		<div class="large-4 columns">
	   		<?php dynamic_sidebar('widget-two'); ?>        
		</div><!-- end row -->
	</div><!-- end widget -->
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'widget-three' ) ) : ?>
	<div style="display: block;">
		<div class="large-4 columns">
	   		<?php dynamic_sidebar('widget-three'); ?>        
		</div><!-- end row -->
	</div><!-- end widget -->
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'widget-four' ) ) : ?>
	<div style="display: block;">
		<div class="large-4 columns">
	   		<?php dynamic_sidebar('widget-four'); ?>        
		</div><!-- end row -->
	</div><!-- end widget -->
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'widget-five' ) ) : ?>
	<div style="display: block;">
		<div class="large-4 columns">
	   		<?php dynamic_sidebar('widget-five'); ?>        
		</div><!-- end row -->
	</div><!-- end widget -->
	<?php endif; ?>

	<?php if ( is_active_sidebar( 'widget-six' ) ) : ?>
	<div style="display: block;">
		<div class="large-4 columns">
	   		<?php dynamic_sidebar('widget-six'); ?>        
		</div><!-- end row -->
	</div><!-- end widget -->
	<?php endif; ?>
	
</div>

</div><!-- .row -->
</div><!-- .page-right-sidebar container -->

<?php get_footer(); ?>
