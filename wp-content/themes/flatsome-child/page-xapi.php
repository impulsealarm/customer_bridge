<?php
/*
* Template name: xAPI page holder
 *
 * @package flatsome
 * Angular JSON page holder
 */

if ( is_user_logged_in() ) {
    echo '{"status": "loggedin", "email": "'.$current_user->first_name.'"}';
} else {
    echo '{"status": "guest", "name": "Guest"}';
}

?>