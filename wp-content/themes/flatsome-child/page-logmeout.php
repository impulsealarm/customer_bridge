<?php
/*
* Template name: Log Me Out
 *
 * @package flatsome
 * Angular JSON logout
 */

if ( is_user_logged_in() ) {
    wp_logout();
    echo '{"status": "loggedout", "name": "Guest"}';
} else {
    echo '{"status": "loggedout", "name": "Guest"}';
}

?>