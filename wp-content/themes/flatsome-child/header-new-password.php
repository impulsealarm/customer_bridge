<?php
/* header 
*/
?>
<!DOCTYPE html>
<html>
<head>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="p:domain_verify" content="ab2e951198e52ddae6351f94ce610369"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href="<? echo get_stylesheet_directory_uri(); ?>/bs/css/bootstrap.min.css" rel="stylesheet">    
    <style>
        body {
            background-image: url("https://impulsealarm.com/wp-content/uploads/2016/02/gg-bridge-1920x1080.jpg")!important;
            background-size: cover!important;
            background-repeat: no-repeat!important;
            font-family: 'Roboto', sans-serif!important;
        }
        .row {
            text-align: center;
        }
        .message1{
            background: rgba(255, 255, 255, 0.8)!important;
            padding: 25px!important;
            font-weight:300;
            font-size: 20px;
            line-height: 22px;
            color: #000000;
        }
        .message2{
            background: rgba(255, 255, 255, 0.8)!important;
            padding: 25px!important;
            font-weight:300;
            font-size: 20px;
            line-height: 22px;
            color: #000000;
        }
        .message3 {
            background: rgba(255, 255, 255, 0.8)!important;
            padding: 25px!important;
            font-weight:300;
            font-size: 20px;
            line-height: 22px;
            color: #000000;
        }

        .message4 {
            margin-top: 25px!important;
        }
    </style>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>
<script>
    jQuery(document).ready(function($){
        setTimeout(function() {
            $(".message1").fadeIn("slow");
            $(".progress-bar").attr("aria-valuenow", "25");
            $(".progress-bar").css("width", "25%");
        }, 500);
        setTimeout(function() {
            $(".message2").fadeIn("slow");
            $(".progress-bar").attr("aria-valuenow", "45");
            $(".progress-bar").css("width", "45%");
        }, 1500);
        setTimeout(function() {
            $(".message3").fadeIn("slow");
            $(".progress-bar").attr("aria-valuenow", "78");
            $(".progress-bar").css("width", "78%");
        }, 2500);
        setTimeout(function() {
            $(".message4").fadeIn("slow");
            $(".progress-bar").attr("aria-valuenow", "100");
            $(".progress-bar").css("width", "100%");
        }, 3500);
    });
</script>
</head>

<body <?php body_class(); ?>>

