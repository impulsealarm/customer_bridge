<?php
/*
* Template name: TOS
 *
 * @package flatsome
 */

get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<?php
if ( is_user_logged_in() ) {
    ?>
    <div  class="page-wrapper">
        <div class="row">


            <div id="content" class="large-12 columns" role="main">

                <?php

                if(isset($_POST['submit'])) { // we will work the form here
                    if ($_POST['user_tos_checked'] == 'checked') { // if chekcbox is checked
                        $sql = "UPDATE wp_users SET user_tos_checked=\"checked\" WHERE ID=".$user_ID; // create query
                        $sql = $wpdb->prepare($sql,$user_ID); // prepare the query
                        $wpdb->query($sql); // run the query
                    }
                }

                $user_id = get_current_user_id(); // get current user ID
                $check_if_tos_is_checked="SELECT user_tos_checked FROM wp_users WHERE ID=".$user_id; // create a query to check if TOS is already checked
                $check_if_tos_is_checked =$wpdb->prepare($check_if_tos_is_checked, $user_id); // prepare the query so we can run it inside WP
                $tos = $wpdb->get_row($check_if_tos_is_checked); // run the query and get the row we created and prepared



                $check_if_affiliate = "SELECT user_id FROM wp_affiliate_wp_affiliates WHERE user_id=".$user_id; // create a query to check if user is affiliate
                $check_if_affiliate = $wpdb->prepare($check_if_affiliate, $user_id); // pere the query so we can run it inside WP
                $if_affiliate = $wpdb->get_row($check_if_affiliate); // run the query and get the row we created and prepared

                if ($user_id != $if_affiliate->user_id) { // check if user is not an affiliate
                    echo "You are not affiliate!"; // message if not
                    ?>
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                <?php
                } else { // do stuff if user is an affiliate
                if ($tos->user_tos_checked === "checked") { // checked if TOS is already checked
                echo "You agreed to our Terms of Service. Redirecting you to your bridge page..."; // if yes notice
                ?>
                    <script>
                        setTimeout(function() {window.location.href="https://impulsealarm.com/bridge"}, 1500);
                    </script>
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                <?php
                } else { // if TOS is not already checked show the form, we have to set action="" so our form works inside this page.
                ?>

                <?php get_template_part( 'content', 'page' );?>
                    <form method="post" action="">
                        <label for="user_tos_checked"><input type="checkbox" name="user_tos_checked" value="checked"> I agree to Terms of Service</label>
                        <br />
                        <input type="submit" name="submit" value="Submit">
                    </form>
                    <?php
                }
                }

                ?>


                <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() )
                    comments_template();
                ?>



            </div><!-- #content -->

        </div><!-- .row -->
    </div><!-- .page-wrapper -->
    <?php
} else {
    ?>
   <div  class="page-wrapper">
<div class="row">


<div id="content" class="large-12 columns" role="main">

<p> Only logged in users can see this page. Redirecting you to login page...</p>
    <script>
        setTimeout(function() {window.location.href="https://impulsealarm.com/bridge"}, 1500);
    </script>
    <p>&nbsp;</p><br />
    <p>&nbsp;</p><br />
    <p>&nbsp;</p><br />
    <p>&nbsp;</p><br />
    <p>&nbsp;</p><br />
    <p>&nbsp;</p><br />
    <p>&nbsp;</p><br />
    <p>&nbsp;</p><br />
</div><!-- #content -->

</div><!-- .row -->
</div><!-- .page-wrapper -->
    <?php
}
?>




<?php get_footer(); ?>