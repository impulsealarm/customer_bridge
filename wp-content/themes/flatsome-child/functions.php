<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}



class WP_Affiliate_Custom_Shortcode {
    
    public function __construct() {
    	
    	if($this->plugin_is_active("affiliate-wp")){
        	$this->init();
    	}
    }
    
    public function init(){
    	
		//[affiliate_area_custom_referrals]
		add_shortcode( 'affiliate_area_custom_referrals', array( $this, 'affiliate_area_custom_referrals' ) );
		
		//[affiliate_area_custom_urls]
		add_shortcode( 'affiliate_area_custom_urls', array( $this, 'affiliate_area_custom_urls' ) );
		    	
    }
	
    function plugin_is_active($plugin_var) {
		return in_array( $plugin_var. '/' .$plugin_var. '.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ); 
	}
	
	public function affiliate_area_custom_referrals(){
		
		if ( ! ( affwp_is_affiliate() && affwp_is_active_affiliate() ) ) {
    		return;
    	}

    	ob_start();

    	echo '<div id="affwp-affiliate-dashboard">';

    	get_template_part( 'dashboard-tab', 'referrals' );

    	echo '</div>';

    	$content = ob_get_clean();

    	return do_shortcode( $content );
	}
	
	public function affiliate_area_custom_urls(){
		
		if ( ! ( affwp_is_affiliate() && affwp_is_active_affiliate() ) ) {
    		return;
    	}

    	ob_start();

    	echo '<div id="affwp-affiliate-dashboard">';

    	get_template_part( 'dashboard-tab', 'urls' );

    	echo '</div>';

    	$content = ob_get_clean();

    	return do_shortcode( $content );
	}
}

new WP_Affiliate_Custom_Shortcode();


add_filter("referral_fields", "get_referral_extra_fields");

function get_referral_extra_fields($referral){

	$affiliate_id = $referral->affiliate_id;
	
	$referral_id = $referral->referral_id;
	
	$referral_other_fields_info = getReferralOtherFieldsInfo($affiliate_id, $referral_id);
	
	$referral->referral_name = $referral_other_fields_info->referral_name;
	$referral->referral_phone = $referral_other_fields_info->referral_phone;
	$referral->referral_email = $referral_other_fields_info->referral_email;
	$referral->referral_city = $referral_other_fields_info->referral_city;
	$referral->referral_package = $referral_other_fields_info->referral_package;
	
	return $referral;
}

function getReferralOtherFieldsInfo($affiliate_id, $referral_id){
	
	global $wpdb;
	
	return $wpdb->get_row("select * from wp_vtiger_affiliate_referral_info where affiliate_id = $affiliate_id and referral_id = $referral_id");
}

function getAffiliateUrls($affiliate_id){

	global $wpdb;
	
	if(!$affiliate_id){
		$affiliate_id = affwp_get_affiliate_id();
	}
	
	return $wpdb->get_var("select affiliate_data from wp_vtiger_affiliate where affiliate_id = $affiliate_id");
}

add_filter("arconix_faq_return", "show_faq_when_user_logged_in");

function show_faq_when_user_logged_in($content){
	
	if(is_user_logged_in()){
		return 	$content;
	} else 
		return false;
}

add_shortcode("custom_referral_header", "show_faq_header");

function show_faq_header(){
	
	global $current_user;
	
	get_currentuserinfo();

	if(is_user_logged_in()){
		
		$user = $current_user->user_firstname." ".$current_user->user_lastname;
		
		if(trim($user) == '')
			$user = $current_user->user_login;
			
		ob_start();

		echo "Hello ". $user;
		
		$content = ob_get_clean();

    	return do_shortcode( $content );
	}		
}
// Dino

function loginpage_impulse_link() {
    return 'http://impulsealarm.com';
}
add_filter('login_headerurl','loginpage_impulse_link');

function loginpage_impule_title() {
    return 'Impulse Alarm - ADT Authorized Dealer';
}
add_filter('login_headertitle', 'loginpage_impule_title');

function impulse_login() {
    if ( wp_is_mobile() ) {
        ?>
        <script type='text/javascript' src='https://impulsealarm.com/wp-includes/js/jquery/jquery.js?ver=5358cea4694070527d4f91fc05ec04b3'></script>
        <script type='text/javascript' src='https://impulsealarm.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=5358cea4694070527d4f91fc05ec04b3'></script>
        <style>
            body {
                background-image: url("https://impulsealarm.com/wp-content/uploads/2016/02/gg-bridge-1920x1080.jpg");
                background-size: cover;
                background-repeat: no-repeat;
            }
            .login #backtoblog a, .login #nav a, .login h1 a {
                text-decoration: none;
                color: rgba(255, 254, 254, 0.66);
            }
            .login h1 a {
                background-image: url(https://impulsealarm.com/wp-content/uploads/2015/10/Full_Logo1.png);
                padding-bottom: 30px;
                width: 100%;
                background-size: 100%;
                background-size: contain;
            }
            #loginform {
                background: transparent;
                box-shadow: none!important;
            }
            .newsociallogins {
                display: none;
            }
            h3 {
                display: none;
            }
            #loginform input[type="text"], #login input[type="password"] {
                background: transparent!important;
                border: none!important;
                border-bottom: 1px solid rgba(255, 255, 255, 0.45)!important;
                border-right: 1px solid rgba(255, 255, 255, 0.45)!important;
                padding-left: 30px!important;
                color: rgb(255, 255, 255)!important;
                font-size: 15px;
                box-shadow: none!important;
            }
            ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
                color: white;
                text-transform: lowercase;
            }
            ::-moz-placeholder { /* Firefox 19+ */
                color: white;
                text-transform: lowercase;
            }
            :-ms-input-placeholder { /* IE 10+ */
                color: white;
                text-transform: lowercase;
            }
            :-moz-placeholder { /* Firefox 18- */
                color: white;
                text-transform: lowercase;
            }
            .forgetmenot {
                display: none;
            }
            .iconInput{
                position: absolute!important;
                top: 10px!important;
                left: 10px!important;
                color: rgba(255, 255, 255, 0.45)!important;
            }
            #loginform p {
                position: relative;
            }
            p.submit {
                width: 100%;
                overflow: hidden;
            }

            input#wp-submit {
                width: 100%;
            }
        </style>
        <script>
            jQuery(document).ready(function($){
                username = $("#user_login");
                password = $("#user_pass");
                remember = $("#rememberme");
                submitit = $("#wp-submit");
                username.addClass("usernameLogin");
                password.addClass("passwordLogin");
                remember.addClass("rememberLogin");
                submitit.addClass("submititLogin");
                username.before( "<i class=\"iconInput glyphicon glyphicon-user\"></i>" );
                password.before( "<i class=\"iconInput glyphicon glyphicon-lock\"></i>" );
            });
        </script>
        <link href="<? echo get_stylesheet_directory_uri(); ?>/bs/css/bootstrap.min.css" rel="stylesheet">
        <script src="<? echo get_stylesheet_directory_uri(); ?>/bs/js/bootstrap.min.js"></script>
        <?php
    } else {
        ?>
        <script type='text/javascript' src='https://impulsealarm.com/wp-includes/js/jquery/jquery.js?ver=5358cea4694070527d4f91fc05ec04b3'></script>
        <script type='text/javascript' src='https://impulsealarm.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=5358cea4694070527d4f91fc05ec04b3'></script>
            <style>

                body {
                    background-image: url("https://impulsealarm.com/wp-content/uploads/2016/02/gg-bridge-1920x1080.jpg");
                    background-size: cover;
                    background-repeat: no-repeat;
                }
                .login #backtoblog a, .login #nav a, .login h1 a {
                    text-decoration: none;
                    color: rgba(255, 254, 254, 0.66);
                }
                .login h1 a {
                    background-image: url(https://impulsealarm.com/wp-content/uploads/2015/10/Full_Logo1.png);
                    padding-bottom: 30px;
                    width: 100%;
                    background-size: 100%;
                    background-size: contain;
                }
                #loginform {
                    background: transparent;
                    box-shadow: none!important;
                }
                .newsociallogins {
                    display: none;
                }
                h3 {
                    display: none;
                }
                #loginform input[type="text"], #login input[type="password"] {
                    background: transparent!important;
                    border: none!important;
                    border-bottom: 1px solid rgba(255, 255, 255, 0.45)!important;
                    border-right: 1px solid rgba(255, 255, 255, 0.45)!important;
                    padding-left: 30px!important;
                    color: rgb(255, 255, 255)!important;
                    font-size: 15px;
                    box-shadow: none!important;
                }
                ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
                    color: white;
                    text-transform: lowercase;
                }
                ::-moz-placeholder { /* Firefox 19+ */
                    color: white;
                    text-transform: lowercase;
                }
                :-ms-input-placeholder { /* IE 10+ */
                    color: white;
                    text-transform: lowercase;
                }
                :-moz-placeholder { /* Firefox 18- */
                    color: white;
                    text-transform: lowercase;
                }
                .forgetmenot {
                    display: none;
                }
                .iconInput{
                    position: absolute!important;
                    top: 10px!important;
                    left: 10px!important;
                    color: rgba(255, 255, 255, 0.45)!important;
                }
                #loginform p {
                    position: relative;
                }
                p.submit {
                    width: 100%;
                    overflow: hidden;
                }

                input#wp-submit {
                    width: 100%;
                }
            </style>
            <script>
                jQuery(document).ready(function($){
                    username = $("#user_login");
                    password = $("#user_pass");
                    remember = $("#rememberme");
                    submitit = $("#wp-submit");
                    username.addClass("usernameLogin");
                    password.addClass("passwordLogin");
                    remember.addClass("rememberLogin");
                    submitit.addClass("submititLogin");
                    username.before( "<i class=\"iconInput glyphicon glyphicon-user\"></i>" );
                    password.before( "<i class=\"iconInput glyphicon glyphicon-lock\"></i>" );
                });
            </script>
            <link href="<? echo get_stylesheet_directory_uri(); ?>/bs/css/bootstrap.min.css" rel="stylesheet">
            <script src="<? echo get_stylesheet_directory_uri(); ?>/bs/js/bootstrap.min.js"></script>
        <?php
    }
}
add_action( 'login_enqueue_scripts', 'impulse_login' );

function impulse_shortcode($atts, $content = null) {
	ob_start();
	do_action("woopra_track", $content);
	return ob_get_clean();
}
add_shortcode('impulstracker', 'impulse_shortcode');

function mainpagewoopraform_shortcode( $atts, $content = null ) {
    return '<script>woopra.call("trackForm", "main_page_form", "#gform_35", {exclude: ["state_35", "is_submit_35", "gform_field_values", "gform_unique_id", "gform_source_page_number_35", "gform_submit", "gform_target_page_number_35"]});</script>';
}
add_shortcode( 'mainpagewoopraform', 'mainpagewoopraform_shortcode' );

function unsubscribeform_shortcode( $atts, $content = null ) {
    return '<script>woopra.call("trackForm", "unsubscribe_form", "#gform_9", {exclude: ["state_9", "is_submit_9", "gform_field_values", "gform_unique_id", "gform_source_page_number_9", "gform_submit", "gform_target_page_number_9"]});</script>';
}
add_shortcode( 'unsubscribeform', 'unsubscribeform_shortcode' );

function bankdetailstrack_shortcode( $atts, $content = null ) {
    return '<script>woopra.call("trackForm", "bankdetailstrack_form", "#bank_details", {exclude: ["state_9", "is_submit_9", "gform_field_values", "gform_unique_id", "gform_source_page_number_9", "gform_submit", "gform_target_page_number_9"]});</script>';
}
add_shortcode( 'bankdetailstrack', 'bankdetailstrack_shortcode' );

function testthreestepform_shortcode( $atts, $content = null ) {
    return '<script>woopra.call("trackForm", "threestepform_form", "#gform_54", {exclude: ["state_54", "is_submit_54", "gform_field_values", "gform_unique_id", "gform_source_page_number_54", "gform_submit", "gform_target_page_number_54"]});</script>';
}
add_shortcode( 'threestepform', 'testthreestepform_shortcode' );