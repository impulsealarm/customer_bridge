<div id="affwp-affiliate-dashboard-url-generator" class="affwp-tab-content">

	<h4><?php _e( 'Referral URLs', 'affiliate-wp' ); ?></h4>

	<?php do_action("affwp_referral_other_urls", affwp_get_affiliate_id()); ?>
	
	<?php 
		$affiliate_id = affwp_get_affiliate_id();
	
		$affiliate_urls = getAffiliateUrls($affiliate_id);
	
		if($affiliate_urls){
		
		$affiliate_urls = json_decode($affiliate_urls, true);
		
		foreach($affiliate_urls as $bonus_url => $affiliate_url){
			echo "<p> $bonus_url : <a href = '". $affiliate_url . "' target = '_blank'>$affiliate_url</a></p>";
		}
	}?>

</div>
