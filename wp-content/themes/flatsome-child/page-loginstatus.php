<?php
/*
* Template name: Login status
 *
 * @package flatsome
 */

if ( is_user_logged_in() ) {
    $current_user = wp_get_current_user();
    echo '{"status": "loggedin", "name": "'.$current_user->user_firstname.'"}';
} else {
    echo '{"status": "guest", "name": "Guest"}';
}

?>