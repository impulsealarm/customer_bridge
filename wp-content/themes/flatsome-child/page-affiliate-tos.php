<?php
/*
* Template name: Bridge TOS
 *
 * @package flatsome
 */

get_header("aff"); ?>

<?php if( has_excerpt() ) { ?>
    <div class="page-header">
        <?php the_excerpt(); ?>
    </div>
<?php } ?>

<?php
if ( is_user_logged_in() ) {
   ?>
    <div  class="page-wrapper">
    <div class="row">
        <?php
        do_action("woopra_track", "bridge_event");
        ?>

        <div id="content" class="large-12 columns" role="main">

            <?php

            $user_id = get_current_user_id(); // get current user ID
            $check_if_tos_is_checked="SELECT user_tos_checked FROM wp_users WHERE ID=".$user_id; // create a query to check if TOS is already checked
            $check_if_tos_is_checked =$wpdb->prepare($check_if_tos_is_checked, $user_id); // prepare the query so we can run it inside WP
            $tos = $wpdb->get_row($check_if_tos_is_checked); // run the query and get the row we created and prepared



            $check_if_affiliate = "SELECT user_id FROM wp_affiliate_wp_affiliates WHERE user_id=".$user_id; // create a query to check if user is affiliate
            $check_if_affiliate = $wpdb->prepare($check_if_affiliate, $user_id); // pere the query so we can run it inside WP
            $if_affiliate = $wpdb->get_row($check_if_affiliate); // run the query and get the row we created and prepared

            if ($user_id != $if_affiliate->user_id) { // check if user is not an affiliate
                echo "You are not affiliate!"; // message if not
            } else { // do stuff if user is an affiliate
                if ($tos->user_tos_checked != "checked") { // checked if TOS is not checked
                    ?>
                    <p>You need to agree to terms first.</p><br />
                    Click <a href='/tos'>here</a> to read our Terms of Service.
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <p>&nbsp;</p><br />
                    <?php
                } else { // if TOS is already checked show the bridge page. ?>



                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <div class="entry-content">


                    <?php
                    $user_ID = get_current_user_id();
                    $useragent=$_SERVER['HTTP_USER_AGENT'];

                    $check_if_signed = "SELECT user_sign_status FROM {$wpdb->prefix}users WHERE ID=".$user_ID;
                    $check_if_signed = $wpdb->prepare($check_if_signed,$user_ID);
                    $status_of_signature = $wpdb->get_row($check_if_signed);
                    if ($status_of_signature->user_sign_status == "signed") {
                        echo do_shortcode( '[affiliate_content]
                                <h2>Welcome  [affiliate_name]</h2>
                                &nbsp;
                                <div class="add-pay-info"><a id="trackHref" href="https://impulsealarm.com/account-information/"><i class="fa fa-cc-stripe"></i>Add Your Payment Info</a></div><br><p>You have signed contract</p>
                                [/affiliate_content]' );
                    } else {
                        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
                        {
                            echo do_shortcode( '[affiliate_content]
                                <h2>Welcome  [affiliate_name]</h2>
                                &nbsp;
                                <div class="add-pay-info"><a href="https://impulsealarm.com/account-information/"><i class="fa fa-cc-stripe"></i>Add Your Payment Info</a></div>
                                <div class="sign-agr" style="margin-left: 6px;"><button id="myBtn"><i class="fa fa-file-text"></i>Sign your agreement</button>
                                    <div id="myModal" class="modal">
                                        <div class="modal-content"><span class="close">x</span>
                                            <p style="padding: 20px; background: white; margin: 10px;">You can\'t sign your contract on mobile</p>
                                        </div>
                                    </div>
                                </div>
                                [/affiliate_content]' );
                        } else {
                            echo do_shortcode( '[affiliate_content]
                                <h2>Welcome  [affiliate_name]</h2>
                                &nbsp;
                                <div class="add-pay-info"><a href="https://impulsealarm.com/account-information/"><i class="fa fa-cc-stripe"></i>Add Your Payment Info</a></div>
                                <div class="sign-agr" style="margin-left: 6px;"><button id="myBtn"><i class="fa fa-file-text"></i>Sign your agreement</button>
                                    <div id="myModal" class="modal">
                                        <div class="modal-content"><span class="close">x</span>
                                            <iframe frameborder="0" height="500" scrolling="no" src="https://rightsignature.com/forms/Bridge-Referral-B-b8d649/embedded/044e5c7f0c6?height=500&after_sign_location=https%3A%2F%2Fimpulsealarm.com%2Fbridge%2Fafter-signing%2F" width="706"></iframe>
                                        </div>
                                    </div>
                                </div>
                                [/affiliate_content]' );
                        }

                    }


                    while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . __( 'Pages:', 'flatsome' ),
                            'after'  => '</div>',
                        ) );
                        ?>

                        </div><!-- .entry-content -->
                        </article><!-- #post-## -->


                        <?php
                        // If comments are open or we have at least one comment, load up the comment template
                        if ( comments_open() || '0' != get_comments_number() )
                            comments_template();

                    endwhile; // end of the loop.
                }
            } ?>

        </div><!-- #content -->

    </div><!-- .row -->
   <?php
} else {
   ?>
    <div  class="page-wrapper">
        <div class="row">
            <?php
            do_action("woopra_track", "bridge_event");
            ?>

            <div id="content" class="large-12 columns" role="main">


                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <div class="entry-content">


                        <?php
                        $user_ID = get_current_user_id();
                        $useragent=$_SERVER['HTTP_USER_AGENT'];

                        $check_if_signed = "SELECT user_sign_status FROM {$wpdb->prefix}users WHERE ID=".$user_ID;
                        $check_if_signed = $wpdb->prepare($check_if_signed,$user_ID);
                        $status_of_signature = $wpdb->get_row($check_if_signed);
                        if ($status_of_signature->user_sign_status == "signed") {
                            echo do_shortcode( '[affiliate_content]
						<h2>Welcome  [affiliate_name]</h2>
						&nbsp;
						<div class="add-pay-info"><a id="trackHref" href="https://impulsealarm.com/account-information/"><i class="fa fa-cc-stripe"></i>Add Your Payment Info</a></div><br><p>You have signed contract</p>
						[/affiliate_content]' );
                        } else {
                            if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
                            {
                                echo do_shortcode( '[affiliate_content]
						<h2>Welcome  [affiliate_name]</h2>
						&nbsp;
						<div class="add-pay-info"><a href="https://impulsealarm.com/account-information/"><i class="fa fa-cc-stripe"></i>Add Your Payment Info</a></div>
						<div class="sign-agr" style="margin-left: 6px;"><button id="myBtn"><i class="fa fa-file-text"></i>Sign your agreement</button>
							<div id="myModal" class="modal">
								<div class="modal-content"><span class="close">x</span>
									<p style="padding: 20px; background: white; margin: 10px;">You can\'t sign your contract on mobile</p>
								</div>
							</div>
						</div>
						[/affiliate_content]' );
                            } else {
                                echo do_shortcode( '[affiliate_content]
						<h2>Welcome  [affiliate_name]</h2>
						&nbsp;
						<div class="add-pay-info"><a href="https://impulsealarm.com/account-information/"><i class="fa fa-cc-stripe"></i>Add Your Payment Info</a></div>
						<div class="sign-agr" style="margin-left: 6px;"><button id="myBtn"><i class="fa fa-file-text"></i>Sign your agreement</button>
							<div id="myModal" class="modal">
								<div class="modal-content"><span class="close">x</span>
									<iframe frameborder="0" height="500" scrolling="no" src="https://rightsignature.com/forms/Bridge-Referral-B-b8d649/embedded/044e5c7f0c6?height=500&after_sign_location=https%3A%2F%2Fimpulsealarm.com%2Fbridge%2Fafter-signing%2F" width="706"></iframe>
								</div>
							</div>
						</div>
						[/affiliate_content]' );
                            }

                        }


                        while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . __( 'Pages:', 'flatsome' ),
                            'after'  => '</div>',
                        ) );
                        ?>

                    </div><!-- .entry-content -->
                </article><!-- #post-## -->


                <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() )
                    comments_template();
                ?>

                <?php endwhile; // end of the loop. ?>

            </div><!-- #content -->

        </div><!-- .row -->
    </div><!-- .page-wrapper -->
    <?php
}
?>


    </div><!-- .page-wrapper -->


<?php get_footer("aff"); ?>