<!DOCTYPE html>
<?php global $woo_options, $woocommerce, $flatsome_opt;?>
<!--[if lte IE 9 ]><html class="ie lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="p:domain_verify" content="ab2e951198e52ddae6351f94ce610369"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href="<? echo get_stylesheet_directory_uri(); ?>/bs/css/bootstrap.min.css" rel="stylesheet">

    <?php wp_head(); ?>
    <script>
        jQuery(document).ready(function($){
            username = $(".login-username input");
            password = $(".login-password input");
            remember = $(".login-remember input");
            submitit = $(".login-submit input");

            username.addClass("usernameLogin");
            password.addClass("passwordLogin");
            remember.addClass("rememberLogin");
            submitit.addClass("submititLogin");
            username.before( "<i class=\"iconInput glyphicon glyphicon-user\"></i>" );
            password.before( "<i class=\"iconInput glyphicon glyphicon-lock\"></i>" );

        });
    </script>
</head>

<body <?php body_class(); ?>>

