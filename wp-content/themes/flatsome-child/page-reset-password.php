<?php
/*
* Template name: Reset password
 *
 * @package flatsome
 */


get_header("new-password");

    global $wpdb;

    $error = '';
    $success = '';

    if( isset( $_GET['your_email'] ))
    {
        $email = $_GET['your_email'];

        $check_if_already_generated="SELECT password_generated FROM wp_users WHERE user_email=\"".$email."\""; // create a query to check if TOS is already checked
        $check_if_already_generated =$wpdb->prepare($check_if_already_generated, $email); // prepare the query so we can run it inside WP
        $is_generated = $wpdb->get_row($check_if_already_generated); // run the query and get the row we created and prepared
        if( empty( $email ) ) {
            ?>
            ?>
            <div  class="page-wrapper">
                <div class="row">
                    <div id="content" class="large-12 columns" role="main">
                        <div class="message alert alert-danger"><strong>ERROR:</strong> No email address selected.</div>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <?php
        } else if( ! is_email( $email )) {
            ?>
            <div  class="page-wrapper">
                <div class="row">
                    <div id="content" class="large-12 columns" role="main">
                        <div class="message"><strong>ERROR:</strong> Please access this page through email we sent you.</div>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <?php
        } else if( $is_generated->password_generated === "generated") {
            ?>
            <div  class="page-wrapper">
                <div class="row">
                    <div id="content" class="large-12 columns" role="main">
                        <div class="message alert alert-warning">We've sent your password to <?php echo $email; ?>.  If you do not see an email, please contact us for assistance.</div>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <?php
        } else if( ! email_exists($email) ) {
            ?>
            <div  class="page-wrapper">
                <div class="row">
                    <div id="content" class="large-12 columns" role="main">
                        <div class="message alert alert-danger"><strong>ERROR:</strong> We cannot associate new user with that email, please access this page through email we sent you.</div>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <?php
        } else {

            $random_password = wp_generate_password( 12, false );
            $user = get_user_by( 'email', $email );

            $update_user = wp_update_user( array (
                    'ID' => $user->ID,
                    'user_pass' => $random_password
                )
            );
            if ( is_wp_error( $update_user ) ) {
                echo 'Error.';
            } else {
                $url = "<a href='https://impulsealarm.com/my-account/edit-account/'>account settings</a>";
                $to = $email;
                $subject = '[Impulse Alarm - ADT Authorized Dealer] Your new password';
                $sender = get_option('name');

                $message = "Hi,"."<br><br>";
                $message .= "Your new password is: ".$random_password."<br><br>";
                $message .= "Please go to your ".$url." and change it.";


                $headers[] = 'MIME-Version: 1.0' . "\r\n";
                $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers[] = "X-Mailer: PHP \r\n";
                $headers[] = 'From: '.$sender.' < '.$email.'>' . "\r\n";

                $mail = wp_mail( $to, $subject, $message, $headers );

                $sql = "UPDATE wp_users SET password_generated=\"generated\" WHERE user_email=\"".$email."\""; // create query
                $sql = $wpdb->prepare($sql,$user_ID); // prepare the query
                $wpdb->query($sql); // run the query
                ?>
                <div  class="page-wrapper">
                    <div class="row">
                        <div id="content" class="large-12 columns" role="main">
                            <div style="text-align: center;">
                                <img src="https://impulsealarm.com/wp-content/uploads/2016/02/bridge-logo-login.png" />
                            </div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0"
                                     aria-valuemin="0" aria-valuemax="100" style="width:0%">
                                </div>
                            </div>
                            <div class="message1" style="display: none;">Hi, let us create a profile for you...</div>
                            <div class="message2" style="display: none;">Now, we are going to generate a password for you...</div>
                            <div class="message3" style="display: none;">Password generated, we are sending it to your email...</div>
                            <div class="message4 alert alert-success" style="display: none;">We have successfully created your profile, please check your email (<?php echo $email; ?>) for further instructions</div>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>

                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }

?>


<?php get_footer("new-password"); ?>