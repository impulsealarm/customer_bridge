<?php
/*
* Template name: Login page
 *
 * @package flatsome
 */

get_header('login'); ?>

<?php
    if ( is_user_logged_in() ) {
        ?>
        <h1>You are already logged in. Redirecting you to main page...</h1>
        <script>
            setTimeout(function() {
                window.location.assign("https://impulsealarm.com/");
            }, 1500);
        </script>
        <?php
    } else {
        if ( wp_is_mobile() ) {
            ?>This is mobile login page.<?php
        } else {
            ?>
            <style>
                body {
                    background-image: url("https://impulsealarm.com/wp-content/uploads/2016/02/gg-bridge-1920x1080.jpg");
                    background-size: cover;
                    background-repeat: no-repeat;
                }
            </style>
            <div class="col-md-12">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="col-md-12">
                        <a class="logoHref" href="https://impulsealarm.com/">
                            <img src="https://impulsealarm.com/wp-content/uploads/2015/10/Full_Logo1.png">
                        </a>
                    </div>
                    <div class="col-md-12">
                        <p class="statusLogin"></p>
                    </div>
                    <div class="col-md-12">
                        <form id="login" action="login" method="post">
                            <p class="login-username">
                                <label for="user_login"></label>
                                <i class="iconInput glyphicon glyphicon-user"></i><input type="text" name="log" id="user_login" class="input usernameLogin" value="" size="20">
                            </p>
                            <p class="login-password">
                                <label for="user_pass"></label>
                                <i class="iconInput glyphicon glyphicon-lock"></i><input type="password" name="pwd" id="user_pass" class="input passwordLogin" value="" size="20">
                            </p>
                            <p class="login-submit">
                                <input type="submit" name="submit" id="submit" class="button-primary submititLogin" value="Log In">
                                <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
            <?php
        }
    }
?>

<?php get_footer('login'); ?>