=== Plugin Name ===
Contributors: zakx
Tags: userlike, livechat, livehelp, live, chat, help
Requires at least: 3.3
Tested up to: 4.4
Stable tag: 1.4

This plugin integrates the Userlike Live Chat into your website.

== Description ==

This plugin integrates the [Userlike](https://www.userlike.com/ "Live Chat for Website Suppor") Live Chat into your website.

Userlike is a live chat software with which you can chat with the visitors of your website. Offer an outstanding online service, get to know your visitors, and raise your conversion rate! This plugin allows you to embed Userlike into your WordPress website within seconds. Please be aware that you need an account with Userlike to use this plugin. 

== Installation ==

1. Extract `userlike.zip` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to 'Settings' -> 'Userlike' and enter your Userlike Widget Secret, which can be found in the Userlike Widget Editor in the [install tab](https://www.userlike.com/en/dashboard/config/chat_widget/edit/default#install-credentials).

== Changelog ==

= 1.4 =
* Add direct link to Widget Secret and update readme.txt

= 1.3 =
* Changed API host

= 1.2 =
* Updated for new Userlike API (you'll only need your secret now)
* Removed dependency on cURL support

= 1.1 =
* has been left out due to distribution issues

= 1.0 =
* First release

== Upgrade Notice == 
= 1.3 =
This version uses the new Userlike API host, enabling faster changes to your chat widget. Update recommended.

= 1.2 =
This version uses the new Userlike API enabling you to use the new Userlike features. Update recommended.
