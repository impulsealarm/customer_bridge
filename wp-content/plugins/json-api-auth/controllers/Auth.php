<?php

/*
Controller Name: Auth
Controller Description: Authentication add-on controller for the Wordpress JSON API plugin
Controller Author: Matt Berg, Ali Qureshi
Controller Author Twitter: @parorrey
*/



require_once("SyncAffiliateUrls.php");

class JSON_API_Auth_Controller {


	public function validate_auth_cookie() {

		global $json_api;

		if (!$json_api->query->cookie) {

			$json_api->error("You must include a 'cookie' authentication cookie. Use the `create_auth_cookie` Auth API method.");

		}		

    	$valid = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in') ? true : false;

		return array(

			"valid" => $valid

		);


	}

	public function generate_auth_cookie() {
		
		global $json_api;

		/*
		$nonce_id = $json_api->get_nonce_id('auth', 'generate_auth_cookie');

		if (!wp_verify_nonce($json_api->query->nonce, $nonce_id)) {

			$json_api->error("Your 'nonce' value was incorrect. Use the 'get_nonce' API method.");
		}*/


		if (!$json_api->query->username) {

			$json_api->error("You must include a 'username' var in your request.");

		}


		if (!$json_api->query->password) {

			$json_api->error("You must include a 'password' var in your request.");

		}	
		
		if ($json_api->query->seconds) 	$seconds = (int) $json_api->query->seconds;

		else $seconds = 1209600;//14 days



    	$user = wp_authenticate($json_api->query->username, $json_api->query->password);

    	if (is_wp_error($user)) {

    		$json_api->error("Invalid username and/or password.", 'error', '401');

    		remove_action('wp_login_failed', $json_api->query->username);

    	}


    	$expiration = time() + apply_filters('auth_cookie_expiration', $seconds, $user->ID, true);

    	$cookie = wp_generate_auth_cookie($user->ID, $expiration, 'logged_in');

		preg_match('|src="(.+?)"|', get_avatar( $user->ID, 32 ), $avatar);	

		return array(
			"cookie" => $cookie,
			"cookie_name" => LOGGED_IN_COOKIE,
			"user" => array(
				"id" => $user->ID,
				"username" => $user->user_login,
				"nicename" => $user->user_nicename,
				"email" => $user->user_email,
				"url" => $user->user_url,
				"registered" => $user->user_registered,
				"displayname" => $user->display_name,
				"firstname" => $user->user_firstname,
				"lastname" => $user->last_name,
				"nickname" => $user->nickname,
				"description" => $user->user_description,
				"capabilities" => $user->wp_capabilities,
				"avatar" => $avatar[1]

			),
		);
	}

	public function create_affiliate(){
		
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");
		}	

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}
		
		if (! is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
			$json_api->error("Please Activate AffiliateWP Plugin to continue.");
		}
		
		$firstname = $json_api->query->firstname;
		
		$lastname = $json_api->query->lastname;
		
		$email = $json_api->query->email;
		
		if(!$email){
			$json_api->error("Please include email in you request.");
		}
		
		$random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );

		$is_user_exists = email_exists($email);//username_exists($email); return user ID
		
		if($is_user_exists){
			$user = get_user_by("email", $email);
			$new_user_id = $user->ID;
		} else {
			
			$userdata = array(
				'user_login'  =>  $email,
				'user_email'  =>  $email,
				'user_pass'   =>  $random_password
			);

			if($firstname)
				$userdata['first_name'] = $firstname;
			if($lastname)
				$userdata['last_name'] = $lastname;
			
			$new_user_id = wp_insert_user( $userdata ) ;
			
			if ( is_wp_error( $new_user_id  ) ) {
				$json_api->error($new_user_id->get_error_message());
			}
		}
			
		if ( ! affiliate_wp()->affiliates->get_by( 'user_id', $new_user_id ) ) {
	

			if ( affiliate_wp()->settings->get( 'require_approval' ) ) {
				$status = 'pending';
			} else {
				$status = 'active';
			}
		
			$args = array(
				'user_id'       => $new_user_id,
				'status'        => $status,
			//	'rate'          => ! empty( $data_rate] ) ? sanitize_text_field( $data_rate ) : '',
			//	'rate_type'     => ! empty( $data_type ) ? sanitize_text_field( $data_type ) : '',
				'payment_email' => ! empty( $email ) ? sanitize_text_field( $email ) : ''
			);
	
			$affiliate_id = affiliate_wp()->affiliates->add( $args );
	
			if ($affiliate_id) {
		
				wp_set_password($random_password, $new_user_id);
		
				$user_info = array("username" => $firstname, "user_pass" => $random_password);
					
				$this->SendAffiliateApprovalEmailWithUserCredentials($affiliate_id, $user_info);
				
				$user_affiliate_info = affiliate_wp()->affiliates->get_by( 'user_id', $new_user_id );
	
				/*
				
				$Affiliate_Sync_Controller = new Affiliate_Sync_Controller();
				$affiliate_urls = $Affiliate_Sync_Controller->GetBonusURLShortenLink($affiliate_id);
				if(!empty($affiliate_urls)){
					$Affiliate_Sync_Controller->saveAffiliateBonusURL($affiliate_id, $affiliate_urls);
				}
				
				*/
				
				$affiliate_urls = array();
				
				return array("affiliate_id" => $affiliate_id, "info" => $user_affiliate_info, "bonus_urls" => $affiliate_urls);
				
			} else {
				$json_api->error("Something went wrong while creating Affiliate.");
			}
		} else {
				
			$update_data = array();
			
			$affiliate_id = affwp_get_affiliate_id($new_user_id);
			
			/*$affiliateSyncController = new Affiliate_Sync_Controller();
			
			$affiliate_urls = $affiliateSyncController->GetBonusURLShortenLink($affiliate_id);
			
			if(!empty($affiliate_urls)){
				
				$affiliateSyncController->saveAffiliateBonusURL($affiliate_id, $affiliate_urls);
			}*/
			
			$affiliate_urls = array();
			
			$user_affiliate_info = affwp_get_affiliate( $affiliate_id );

			return array("affiliate_id" => $affiliate_id, "info" => $user_affiliate_info, "bonus_urls" => $affiliate_urls);
		}
		return false;
	}

	function ChangeAffiliateStatus(){
		
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");
		}	

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}
		
		if (! is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
			$json_api->error("Please Activate AffiliateWP Plugin to continue.");
		}
		
		$affiliate_id = $json_api->query->affiliate_id;
		
		$status = $json_api->query->status;
		
		if(!$affiliate_id && $status == '')
			$json_api->error("Please include affiliate id and status in your request.");
		
		//$user_id = affwp_get_affiliate_user_id($affiliate_id);
		
		$updated = false;
		
		if ( affwp_get_affiliate($affiliate_id) ) {
	
			if ( 'deactivate' === $status ) {
				$updated = affwp_set_affiliate_status( $affiliate_id, 'inactive' );
			} else if ( 'activate' === $status ) {
				$updated = affwp_set_affiliate_status( $affiliate_id, 'active' );
			}
			
			if(!$updated)
				$json_api->error("Something went wrong while $status.");
				
		} else {
			$json_api->error("Affiliate Doesnot Exists.");
		}
		return false;
	}
	
	// By default currency is USD
	public function create_referral(){
		
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");
		}	

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}
		
		if (! is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
			$json_api->error("Please Activate AffiliateWP Plugin to continue.");
		}
			
		$affiliate_id = $json_api->query->affiliate_id;
		
		if(!$affiliate_id)
			$json_api->error("Please include affiliate id in your request.");
		
		// affwp_add_referral() donot return referral id
		
		$description = $json_api->query->description;
		
		$reference = $json_api->query->reference;
		
		$context = $json_api->query->context;
		
		$amount = $json_api->query->amount;
		
		$args = array();
		
		$args['affiliate_id'] = $affiliate_id;
		
		$args['description'] = $description ? sanitize_text_field($description):"";
		
		$args['reference'] = $reference ? sanitize_text_field($reference):"";
		
		$args['context'] = $context ? sanitize_text_field($context):"";
		
		$args['amount'] = $amount;
		
		$args['status'] = 'unpaid';

		$referral_id = affiliate_wp()->referrals->add( $args );
		
		if( $referral_id ) {
			return array("referral_id" => $referral_id);
		} else {
			$json_api->error("Something Went Wrong While Creating Referral.");
		}
	
		return false;
	}
	
	/**
	 * Update Referral Fields
	 * description, status, amount, currency, custom
     * context, campaign, reference, products
     * @return true if updated otherwise false
	 */
	
	public function update_referral(){
		
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");
		}	

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}
		
		if (! is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
			$json_api->error("Please Activate AffiliateWP Plugin to continue.");
		}
		
		$referral_id = $json_api->query->referral_id;
		
		if(!$referral_id)
			$json_api->error("Please include Referral id in your request.");
		
		$referral = affwp_get_referral($referral_id);
		
		$affiliate_id = $referral->affiliate_id;
		
		if ( $referral ) {
	
			$update_ref_data = $other_fields = array();
			
			$amount = $json_api->query->amount;
				
			$status = $json_api->query->status;
			
			$reference = $json_api->query->reference;
			
			$description = $json_api->query->description;
			
			$context = $json_api->query->context;
			
			if($amount)
				$update_ref_data['amount'] = $amount;
				
			/*if($status)
				$update_ref_data['status'] = $status;
			*/
			
			if($reference)
				$update_ref_data['reference'] = $reference;
				
			if($description)	
				$update_ref_data['description'] = $description;
			
			if($context)
				$update_ref_data['context'] = $context;

			$set_zero_amount = $json_api->query->set_zero_amount;
			
			if($set_zero_amount)
				$update_ref_data['amount'] = "0";
			
			if(!empty($update_ref_data))
				$updated = affiliate_wp()->referrals->update_referral( $referral_id, $update_ref_data );
			else
				$updated = true;
				
			$other_fields['affiliate_id'] = $affiliate_id;
			$other_fields['lead_status'] = $json_api->query->lead_status;
			$other_fields['call_dispo'] = $json_api->query->call_dispo;
			$other_fields['referral_name'] = $json_api->query->referral_name;
			$other_fields['referral_phone'] = $json_api->query->referral_phone;
			$other_fields['referral_email'] = $json_api->query->referral_email;
			$other_fields['referral_city'] = $json_api->query->referral_city;
			$other_fields['referral_package'] = $json_api->query->referral_package;
			
			
			$update_other_fields = $this->updateReferralOtherFields($referral_id, $other_fields);
			
			if(!$updated || $update_other_fields === false)
				$json_api->error("Something went wrong while updating referral.");
			else 
				return $updated;
				
		} else {
			$json_api->error("Referral Doesnot Exists.");
		}
	}
		
	public function get_currentuserinfo() {
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");

		}

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}

		$user = get_userdata($user_id);
        preg_match('|src="(.+?)"|', get_avatar( $user->ID, 32 ), $avatar);

		return array(
			"user" => array(
				"id" => $user->ID,
				"username" => $user->user_login,
				"nicename" => $user->user_nicename,
				"email" => $user->user_email,
				"url" => $user->user_url,
				"registered" => $user->user_registered,
				"displayname" => $user->display_name,
				"firstname" => $user->user_firstname,
				"lastname" => $user->last_name,
				"nickname" => $user->nickname,
				"description" => $user->user_description,
				"capabilities" => $user->wp_capabilities,

				"avatar" => $avatar[1]

			)

		);

	}	
	
	/**
	 * Send Email to the user with accepted_subject and accepted_email of affiliate
	 * if $user_password is set then send email with user login credentials.
	 * @param int $affiliate_id
	 * @param int $user_id
	 * @param $user_password 
	 * @return unknown_type
	 */
	function SendAffiliateApprovalEmailWithUserCredentials($affiliate_id, $user_cred){
		
		$settings = get_option('affwp_settings');
			
		$subject = $settings['accepted_subject'] ? $settings['accepted_subject'] : __( 'Affiliate Application Accepted', 'affiliate-wp' ) ;
		
		$message = $settings['accepted_email'];
	
		if( empty( $message ) ) {
			$message  = sprintf( __( 'Congratulations %s!', 'affiliate-wp' ), affiliate_wp()->affiliates->get_affiliate_name( $affiliate_id ) ) . "\n\n";
			$message .= sprintf( __( 'Your affiliate application on %s has been accepted!', 'affiliate-wp' ), home_url() ) . "\n\n";
			$message .= sprintf( __( 'Log into your affiliate area at %s', 'affiliate-wp' ), affiliate_wp()->login->get_login_url() ) . "\n\n";
		}
		
		$emails = new Affiliate_WP_Emails;
		
		$emails->__set( 'affiliate_id', $affiliate_id );
		
		$email = affwp_get_affiliate_email( $affiliate_id );

		if(!empty($user_cred)){
			
			$username = $user_cred['username'];
			
			$pwd = $user_cred['user_pass']?$user_cred['user_pass']:"";
			
			if(strpos($message, "{user_name}") && $pwd != ''){

				$user_pass_string = "{user_name} <br/>Password: ".$pwd;
				
				$message = str_replace("{user_name}",$user_pass_string, $message);
			}
		}
		
		$send = $emails->send( $email, $subject, $message );
		
		return true;
	}
			
	/**
	 * Basic Actions like Accept, Reject, Mark as Paid, Mark as Unpaid and Delete
	 * @return unknown_type
	 */
	function ChangeReferralBasicAction(){
		
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");
		}	

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}
		
		if (! is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
			$json_api->error("Please Activate AffiliateWP Plugin to continue.");
		}
		
		$referral_id = $json_api->query->referral_id;
		
		$current_action = $json_api->query->action;
		
		$updated = false;
		
		$message = "";
		
		if(!$referral_id)
			$json_api->error("Please include Referral id in your request.");
		
		if(!$current_action)	
			$json_api->error("Please include action in your request.");
		else if(!in_array($current_action, array("delete", "reject", "accept", "mark_as_paid", "mark_as_unpaid")))
			$json_api->error("Invalid action in your request.");
			
		if ( affwp_get_referral($referral_id) ) {
	
			if ( 'delete' === $current_action ) {
				$updated = affwp_delete_referral( $referral_id );
				$message = "deleted";
			}

			if ( 'reject' === $current_action ) {
				$updated = affwp_set_referral_status( $referral_id, 'rejected' );
				$message = "rejected";
			}

			if ( 'accept' === $current_action) {
				$updated = affwp_set_referral_status( $referral_id, 'unpaid' );
				$message = "accepted";
			}

			if ( 'mark_as_paid' === $current_action ) {
				$updated = affwp_set_referral_status( $referral_id, 'paid' );
				$message = "paid";
			}

			if ( 'mark_as_unpaid' === $current_action ) {
				$updated = affwp_set_referral_status( $referral_id, 'unpaid' );
				$message = "unpaid";
			}
		} else {
			$json_api->error("Invalid Referral Id.");
		}
		return array("update_referral" => $updated, "referral_status" =>$message);
	}		
	
	function CheckAffiliateReferral(){
		
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");
		}	

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}
		
		if (! is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
			$json_api->error("Please Activate AffiliateWP Plugin to continue.");
		}
		
		$affiliate_id = $json_api->query->affiliate_id;
		
		$referral_id = $json_api->query->referral_id;
		
		if(!$affiliate_id && !$referral_id)
			$json_api->error("Please include Affiliate Id and Referral Id in your request.");
		
		if(!affiliate_wp()->affiliates->affiliate_exists( $affiliate_id ) ) {
			$json_api->error("Affiliate Doesnot Exists");
		}
		
		$referral_details = affiliate_wp()->referrals->get_by( "referral_id", $referral_id );

		if($referral_details->affiliate_id === $affiliate_id){
			return array("is_affiliate_referral" => true);
		} else {
			return array("is_affiliate_referral" => false);
		}
	}
	
	function updateReferralOtherFields($referral_id, $data){
		
		global $wpdb;
		
		$affiliate_id = "";
		
		if(isset($data['affiliate_id']) && $data['affiliate_id'] > 0){
			$affiliate_id = $data['affiliate_id'];
			unset($data['affiliate_id']);
		} else {
			$referral = affwp_get_referral($referral_id);
			$affiliate_id = $referral->affiliate_id;
		}
		
		$where = array("affiliate_id" => $affiliate_id, "referral_id" => $referral_id);
		
		$result = $wpdb->get_row("select * from wp_vtiger_affiliate_referral_info where referral_id = $referral_id and affiliate_id = $affiliate_id");
		
		if(!$result){
			
			$data = array_merge($data,$where);
			
			$wpdb->insert("wp_vtiger_affiliate_referral_info", $data);
			
			return $wpdb->insert_id;
		} else {
			return $wpdb->update("wp_vtiger_affiliate_referral_info", $data, $where);	
		}
	}
	
		
	function saveAffiliateReferralURLs(){
	
		global $json_api;

		if (!$json_api->query->cookie) {
			$json_api->error("You must include a 'cookie' var in your request. Use the `generate_auth_cookie` Auth API method.");
		}	

		$user_id = wp_validate_auth_cookie($json_api->query->cookie, 'logged_in');

		if (!$user_id) {
			$json_api->error("Invalid authentication cookie. Use the `generate_auth_cookie` Auth API method.");
		}
		
		if (! is_plugin_active( 'affiliate-wp/affiliate-wp.php' ) ) {
			$json_api->error("Please Activate AffiliateWP Plugin to continue.");
		}
		
		$affiliate_id = $json_api->query->affiliate_id;
		
		if(!$affiliate_id)
			$json_api->error("Please include Affiliate Id in your request.");
		
		if(!affiliate_wp()->affiliates->affiliate_exists( $affiliate_id ) ) {
			$json_api->error("Affiliate Doesnot Exists");
		}
		
		$affiliate_urls = $json_api->query->affiliate_urls;
	
		if(!empty($affiliate_urls)){
		
			$Affiliate_Sync_Controller = new Affiliate_Sync_Controller();
			
			$Affiliate_Sync_Controller->saveAffiliateBonusURL($affiliate_id, $affiliate_urls);
			
			return array("save_bonus_urls" => true, "bonus_urls" => $affiliate_urls);
		} else {
			$json_api->error("Please include Affiliate Referral URLs in request.");
		}
		
		return false;
	}
	
}