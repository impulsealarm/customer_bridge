-----------------------------------------------------------
1.0.2
    - Added support for delaying feed processing until payment by PayPal Standard is successfully completed.

-----------------------------------------------------------
1.0.1
    - Fixed an issue where a due date of Jan 1, 1970 was set on the card if the mapped date field was empty.

-----------------------------------------------------------
1.0
	- It's all new!