<?php

GFForms::include_feed_addon_framework();

class GFSecurityTrax extends GFFeedAddOn {

	protected $_slug = 'gravityformssecuritytrax';
	protected $_path = 'gravityformssecuritytrax/securitytrax.php';
	protected $_full_path = __FILE__;
	protected $_url = 'http://www.gravityforms.com';
	protected $_title = 'Security Trax';
	protected $_short_title = 'Security Trax';

	// Members plugin integration
	protected $_capabilities = array( 'gravityforms_securitytrax' );

	// Permissions
	protected $_capabilities_settings_page = 'gravityforms_securitytrax';
	protected $_capabilities_form_settings = 'gravityforms_securitytrax';
	protected $_enable_rg_autoupgrade = true;

	private static $_instance = null;

	public static function get_instance() {
		
		if ( self::$_instance == null ) {
			self::$_instance = new GFSecurityTrax();
			
		}

		return self::$_instance;
	}
	
	public function init() {

		parent::init();

	}

	public function init_ajax() {
		parent::init_ajax();

		add_action('wp_ajax_get_module_fields', array($this, 'get_module_fields'));

		add_action('wp_ajax_save_module_mapped_fields', array($this, 'save_module_mapped_fields'));
                        
	}

	public function init_admin() {

		parent::init_admin();

	}
	
	public function plugin_settings_fields() {
		return array(
			array(
				'fields'      => array(
					array(
						'name'              => 'gforms_securitytrax_url',
						'label'             => __( 'Site Url', 'gravityformsmailsecuritytrax' ),
						'type'              => 'text',
						'class'             => 'medium',
					),
					array(
						'name'              => 'gforms_securitytrax_username',
						'label'             => __( 'Username', 'gravityformsmailsecuritytrax' ),
						'type'              => 'text',
						'class'             => 'medium',
					),
					array(
						'name'              => 'gforms_securitytrax_password',
						'label'             => __( 'Password', 'gravityformsmailsecuritytrax' ),
						'type'              => 'text',
						'class'             => 'medium',
					),
					array(
						'name'              => 'gforms_securitytrax_partnerid',
						'label'             => __( 'Partner ID', 'gravityformsmailsecuritytrax' ),
						'type'              => 'text',
						'class'             => 'medium',
						'description'		=> "This is the unique ID that SecurityTrax assigns to the partner company. Source ID Provided by the company using SecurityTrax.",
					),
				)
			),
		);
	}
	
	public function feed_list_page() {
		self::feed_edit_page();
	}
	
	public function feed_edit_page() {
		
		$module = 'Leads';
        
		$form_id = rgget('id');
		
		$mapped_fields = self::$_instance->get_feeds($form_id);
		
		if(!empty($mapped_fields)) {
			foreach($mapped_fields as $module_fields)
				$field_details = $module_fields['meta'];
			
			$selected_module = array_keys($field_details);
            $module = $selected_module[0];
            $mapped_fields = $field_details[$module];
			$form_sync = $field_details['form_sync'];	// Changes 9Dec,2015
		}
		   
		
		$module_fields = self::get_module_fields($module);
		
        $form_field = GFFormsModel::get_form_meta($form_id);
		
		?>
		<style>
				#update_dialog_success, #update_dialog_error { display: none;}
				.wp-list-table tbody tr td { max-width: 220px;}
				#select_module { font-size: 14px; }
				
		</style>
			
        <script type="text/javascript">
			jQuery(document).ready(function(){

				jQuery("#select_module").on("change", function(){

					var selected_module = jQuery(this).val();

					if(selected_module != ''){
						jQuery("#please_wait_container").show();
						get_selected_module_fields(selected_module);
					}
				});

				jQuery("#gform_module_field_mapping_form").submit(function(e){
					e.preventDefault();

					var FormData = jQuery("#gform_module_field_mapping_form").serialize();

					jQuery.ajax({
				        type:'POST',
				        url : 'admin-ajax.php',
				        data: FormData,
						dataType:'json',
						success: function(data){
							if(data.success){
								location.reload();
								jQuery("#update_dialog_error").css("display", "none");
								jQuery("#update_dialog_success").css("display", "block");
							} else {
								jQuery("#update_dialog_error").css("display", "block");
								return;
							}
					    }
					});
				});
			});

			function get_selected_module_fields(selected_module){
				jQuery.ajax({
			        type:'POST',
			        url : 'admin-ajax.php',
			        data: {action: 'get_module_fields', module: selected_module},
					dataType:'json',

			        success: function(data){

			        	jQuery("#please_wait_container").hide();
						   
				        var items = '';

				        items += '<option value="">Select</option>';

				        jQuery.each(data, function(i, val) {
							items += '<option value="' + i + '">'+ val +'</option>';
				        });

				        jQuery(".module_fields").html(items);
				    }
	        	});
			}
		
		</script>

       	<span id="please_wait_container" style="display:none; width: 62%; right: 10px; position: absolute; opacity: 1.7; right: 10px;">
           	<img src="<?php echo SECURITY_TRAX__PLUGIN_URL; ?>images/ajax-loader.gif" class="gf_loader">
        </span>
		
		<div class="updated below-h2" id="update_dialog_success">
            <p>
               	<strong>Mapped successfully.</strong>
            </p>
       	</div>
		
		<div class="updated below-h2" id="update_dialog_error">
            <p>
               	<strong>Please select all required crm fields.</strong>
            </p>
       	</div>
						
		<form method="post" id="gform_module_field_mapping_form" action="">
			<input type="hidden" name="action" value="save_module_mapped_fields" /> 
			<input type="hidden" id="form_id" name="form_id" value="<?php echo $form_id; ?>">
			 	
			<h3>
	        	<span> 
	        		<?php _e("Security Trax Field Mapping", "gravityforms") ?>
	        	</span>
				
				<span style="font-size: 14px; margin-left: 43%;">
					<input type="checkbox" name="form_sync" value="Turn off Sync" <?php echo $form_sync == 'off' ?'checked':""; ?>/> Turn off Sync
				</span>
				<span>
	        		<select id = "select_module" name = "select_module" style="float: right;">
	        			<option value="Leads" <?php if(isset($module) && "Leads" == $module) echo 'selected';?>>Leads</option>
	        			<option value="Customers" <?php if(isset($module) && "Customers" == $module) echo 'selected';?>>Customers</option>
	        		</select>
	        	</span>
			</h3>
	        
	        <table class="form-table gforms_form_settings" cellspacing="0">

            	<tbody id="the-list">
					
					<?php 	

						$z = 0;  
						$i = 0;          				
						foreach($form_field['fields'] as $form_Field):
							
							if($z == 0 || ($z % 2) == 0)
				  				echo '<tr>';
				  				
				  			if($i > 0 && $i > $z) 
				  				$z = $i;
						
				  			if(!empty($form_Field['inputs'])){
				  				$i = $z;
								
								$heading = $form_Field['label'];
								
								foreach($form_Field['inputs'] as $adv_Field):	
									
									$form_Field = $adv_Field;
									
									if(isset($form_Field['isHidden']) && $form_Field['isHidden'] == '1') continue;
					?>
						<td>
            				<strong>
	            			<?php 
								
								$field_label = $form_Field['label'];
								
								if($heading != '')
									echo $heading." ".$field_label;
								else
									$field_label;
									
								if($heading != '')
	            					$field_label = $heading." :: ".$field_label;
								
	            			?>
	            			</strong>
            			</td>
            			<td>
            				<select name="module_fields[<?php echo $field_label; ?>]" class="module_fields">
            					<option value="">Select</option>
            			
	            			<?php 
	            				foreach($module_fields as $module_field => $module_field_name): 
	            			?>
            			
            					<option value="<?php echo $module_field; ?>" 
            						<?php 
	            						if(
	            							isset($mapped_fields[$field_label]) && 
	            							$mapped_fields[$field_label] != '' && 
	            							$mapped_fields[$field_label] == $module_field
	            						)
	            							echo 'selected';
            						?>>
            						<?php echo $module_field_name;?>
            					</option>
						
            				<?php endforeach; ?>
            				
            				</select>
            			</td>
            			<?php 
							if($i > 0 && ($i % 2) == 1) {
								echo "</tr>";
								$i++;
		    				} else{ 
								$i++;
		    				}
						?>
					
            			<?php endforeach; ?>
						<?php } else { ?>
							<td>
            				<strong>
	            			<?php 
	            					echo $form_Field['label'];
	            			?>
	            			</strong>
            			</td>
            			<td>
            				<select name="module_fields[<?php echo $form_Field['label']?>]" class="module_fields">
            					<option value="">Select</option>
            			
	            			<?php 
	            				foreach($module_fields as $module_Field => $module_field_name): 
	            			?>
            			
            					<option value="<?php echo $module_Field; ?>" 
            						<?php 
	            						if(
	            							isset($mapped_fields[$form_Field['label']]) && 
	            							$mapped_fields[$form_Field['label']] != '' && 
	            							$mapped_fields[$form_Field['label']] == $module_Field
	            						)
	            							echo 'selected';
            						?>>
            						<?php echo $module_field_name; ?>
            					</option>
						
            				<?php endforeach; ?>
            				
            				</select>
            			</td>
						<?php }?>
						<?php 
							if($i > $z)
								$z = $i;
								
		    				if($z > 0 && ($z % 2) == 1) {
								echo "</tr>";
								$z++;
		    				} else{ 
								$z++;
		    				}
						?>
					<?php endforeach;?>
	            </tbody>
        </table>
		<p class="submit">
        	<?php
            	$button_label = __("Save Changes", "gravityforms");
                $vtiger_button = '<input class="button-primary" type="submit" value="' . $button_label . '" name="save"/>';
                echo apply_filters("gform_save_changes_button", $vtiger_button);
           ?>
        </p>
    </form>
	<?php //parent::feed_edit_page( $form, $feed_id );
	}
	
	public function feed_list_columns() {
		return array(
			'feedName'            => __( 'Form Field Name', 'gravityformsmailvtiger' ),
			'mailchimp_list_name' => __( 'Vtiger Field Name', 'gravityformsmailvtiger' )
		);
	}
	
	public function get_module_fields($module = ''){
		
		$module_fields = array();
		
		if(!$module){
			
			if(isset($_POST['module']) && $_POST['module'] != ''){
				$module = $_POST['module'];	
			}
		}
		
		if($module == 'Customers')
			$module_fields = self::getCustomersFields();
		else if ($module == "Leads")
			$module_fields = self::getLeadsFields();
		
		if(!empty($_POST) && isset($_POST['module'])) 
			echo json_encode($module_fields);
		else	
			return $module_fields;
			
		die();
		
	}
	
	public function getCustomersFields(){

		$contact_fields = array(
			"fname" => "First Name", 
			"lname" => "Last Name", 
			"spouse" => "Spouse Name", 
			"address1" => "Primary Address Line 1", 
			"address2" => "Primary Address Line 2",
			"cross_street" => "Cross Street", 
			"city" => "Primary City", 
			"county" => "Primary County", 
			"state" => "Primary State", 
			"zip" => "Primary Zip",
			"phone1" => "Primary Phone",
			"phone2" => "Work Phone",
			"phone3" => "Cell Phone", 
			"phone4" => "Other Phone", 
			"email" => "Email", 
			"beacon_score" => "Beacon Score",
			"credit_tracking_number" => "Credit Tracking Number", 
			"ssn" => "SSN", 
			"account_num" => "Account Number",  
			"abort_code" => "Abort Code",
			"mrr" => 'Mrr', 
			"contact_time" => 'Contact Time', 
			"customer_purchase_price" => "Customer Purchase Price", 
			"customer_home_ownership" => "Customer Home Ownership",
			"partner_company_unique_id" => "Partner Company Unique Id", 
			"service_two_way_voice" => "Service Two Way Voice", 
			"service_cellular_backup" => "Service Cellular Backup",
			"service_guard_response" => "Service Guard Response", 
			"service_maintenance" => "Service Maintenance", 
			"activation_fee" => "Activation Fee", 
			"contract_term_length" => "Contract Term Length",
			"birthdate" => "Birthdate", 
			"lead_promo_code" => "Lead Promo Code", 
			"lead_custom1" => "Lead Custom 1", 
			"lead_custom2" => "Lead Custom 2", 
			"lead_custom3" => "Lead Custom 3",
			"lead_custom4" => "Lead Custom 4", 
			"lead_callcenter_notes" => "Lead Callcenter Notes", 
			"ContactName" => "Contact Name", 
			"PhoneNumber" => "Phone Number", 
			"PhoneType" => "Phone Type",
			"BillTypeID" => "Bill Type ID", 
			"BillCCCardType" => "Bill CC Card Type", 
			"BillCCName" => "Bill CC Name", 
			"BillCCNumber" => "Bill CC Number", 
			"BillCCExpiration" => "Bill CC Expiration", 
			"BillingUses" => "Billing Uses",
			
			//From SecurityTrax Interface
			"bill_address1" => "Billing Address 1", 
			"bill_address2" => "Billing Address 2", 
			"bill_city" => "Billing City", 
			"bill_state" => "Billing State", 
			"bill_zip" => "Bill Zip"
		);
		
		return $contact_fields;
	}

	public function getLeadsFields(){

		$leads_fields = array(
			"fname" => "First Name", 
			"lname" => "Last Name", 
			"address1" => "Address Line 1", 
			"address2" => "Address Line 2",
			"city" => "City", 
			"state" => "State", 
			"zip" => "Zip Code", 
			"email" => "Email Address",
			"phone1" => "Primary Phone", 
			"phone2" => "Work Phone",
			"phone3" => "Cell Phone", 
			"phone4" => "Other Phone", 
			"lead_comments" => "Lead Comments", 
			"contact_time" => "Best Time To Contact", 
			"lead_source_info" => "Lead Source",
			"lead_company_unique_id" => "Unique ID",//postid
			"lead_home_ownership" => "Home Ownership",
			"lead_callcenter_rep" => "Callcenter Rep", 
			"lead_callcenter_notes" => "Callcenter Comments",
			"lead_promo_code" => "Promotion Code", 
			"lead_custom1" => "Custom Lead1", 
			"lead_custom2" => "Custom Lead2",
			"lead_custom3" => "Custom Lead3", 
			"lead_custom4" => "Custom Lead4", 
			"spouse" => "Spouse Name", 
			"lead_rep_id" => "Lead Rep Id",
			"campaign_id" => "Campaign", 
			"lead_type_id" => "Lead Type", 
			"lead_purchase_price" => "Lead Purchase Price",
		);
		
		return $leads_fields;
	}
	
	
	public static function save_module_mapped_fields(){
		
		if(!empty($_POST)){
			
			$mandatory_fields = array("Customers", "Leads");
			
			$mandatory_fields['Leads'] = array("fname","lname","city","state","phone1");
			
			$mandatory_fields['Customers'] = array("fname","lname","phone1","city","state");
		
			$module_fields = $_POST["module_fields"];
			
			if(isset($mandatory_fields[$_POST['select_module']]) && !empty($mandatory_fields[$_POST['select_module']])) {
				
				foreach($mandatory_fields[$_POST['select_module']] as $key => $mandatory_field){

					if(in_array($mandatory_field, $module_fields)) {
						
						unset($mandatory_fields[$_POST['select_module']][$key]); 
					}
				}
				
				if(count($mandatory_fields[$_POST['select_module']]) > 0) {
					
					echo json_encode(array("error" => 'Please select all required crm fields'));
			    } else {
			    	
			    	$form = array();
		
					$form_id = $_POST['form_id'];
			        
			        $select_module = $_POST["select_module"];
			            
			        $form[$select_module] = $_POST["module_fields"];
			       
			        $form_feed = self::$_instance->get_feeds($form_id);
			       
					foreach($form_feed as $form_values) {
			        	$form_details = $form_values;
			        }
			        
					// START: Changes 9Dec,2015
				
					if(isset($_POST['form_sync']) && $_POST['form_sync'] == 'Turn off Sync'){
						$form['form_sync'] = "off";
					} else {
						$form['form_sync'] = "on";
					}
					// END: Changes 9Dec,2015
				
					
					if (isset($form_details['form_id']) && $form_details['form_id'] == $form_id) {
						self::$_instance->update_feed_meta($form_details['id'], $form);
					} else {
						$value = true;
						self::$_instance->insert_feed($form_id, $value , $form);
					}
				   	echo json_encode(array("success" => 'true'));
			    }
			    die();
			}
		}
		        
	}
	
	public function maybe_process_feed($entry, $form){
		
		$module_data = array();
		
		$entry_data = parent::maybe_process_feed($entry, $form);
		
		if ( $entry_data['status'] == 'active' ){
			
			$form_id = $entry_data['form_id'];
			
			$select_module_fields = self::$_instance->get_feeds($form_id);
			
			foreach($select_module_fields as $module_fields){
				
				// START: Changes 9Dec,2015
				
				if(isset($module_fields['meta']['form_sync']) && $module_fields['meta']['form_sync'] != ''){
					$check_form_sync = $module_fields['meta']['form_sync'];
				}
				// END: Changes 9Dec,2015
				
				$module = array_keys($module_fields['meta']);
				$module = $module[0];
				$module_fields = $module_fields['meta'][$module];
			}
			
			foreach($form['fields'] as $form_field){
				
				if(!empty($form_field['inputs'])){
					
					$form_label = $form_field['label']." :: ";
				
					foreach($form_field['inputs'] as $adv_value){
						
						if($form_label != '')
							$adv_label = $form_label.$adv_value['label'];
						else 
							$adv_label = $adv_value['label'];
							
						$field_label = $adv_label;
						
						$field_id = (string)$adv_value["id"];
						
						$field_value = $entry_data[$field_id];
						
						if(isset($module_fields[$field_label]) && $module_fields[$field_label] != '')
							$module_data[$module][$module_fields[$field_label]] = $field_value;
					}
				}else {
					$field_label = $form_field['label'];
					$field_id = $form_field['id'];
					$field_value = $entry_data[$field_id];
					if(isset($module_fields[$field_label]) && $module_fields[$field_label] != '')
						$module_data[$module][$module_fields[$field_label]] = $field_value;
				}
			}
		
			// Changes 9Dec,2015
			if($check_form_sync == 'on')
				self::create_select_module_Entry($module_data);
		}
		
		return $entry;
	}
	
	public function create_select_module_Entry($module_data){
    	
		$security_trax_settings = get_option("gravityformsaddon_gravityformssecuritytrax_settings");
			
		$xmlcontent = $url = $emergencycontent = "";
		
		$module = array_keys($module_data);
		
		$module = $module['0'];
		
		$partner_id = $security_trax_settings['gforms_securitytrax_partnerid'];
		
		$site_name = $security_trax_settings['gforms_securitytrax_url'];
		
		$url = $site_name;
		
		if($module == "Customers") {

			$url .= "/customerpostxml.php";
			
			$contact_data = $module_data['Customers'];

			$actual_fields = array_keys($contact_data);
			
			$xmlcontent = '<?xml version="1.0" standalone="yes"?>';
			$xmlcontent .= "<customers>";
			$xmlcontent .= "<customer>";

			$xmlcontent .= "<source_id>".$partner_id."</source_id>";
		
			foreach($contact_data as $xml_tag => $value){
				$xmlcontent .= "<".$xml_tag.">".$value."</".$xml_tag.">";	
			}
			
			$xmlcontent .= "</customer>";
			$xmlcontent .= "</customers>";
			
		} else if($module == "Leads"){
			
			$url .= "/leadpostxml.php";
			
			$lead_data = $module_data['Leads'];
			
			$xmlcontent = '<?xml version="1.0" standalone="yes"?>';
			$xmlcontent .= "<leads>";
			$xmlcontent .= "<lead>";

			$xmlcontent .= "<source_id>".$partner_id."</source_id>";
				
			foreach($lead_data as $xml_tag => $value){
				if($value == '')continue;
				if($xml_tag == 'lead_rep_id' && $value != '') {
			
					$security_trax_userid = self::GetAssignedUserId($value);
					
					if($security_trax_userid == '')
						$security_trax_userid = "1000";//"david@impulsealarm.com";
				
					$xmlcontent .= "<lead_rep_id>".$security_trax_userid."</lead_rep_id>";
						
				} else {
					$xmlcontent .= "<".$xml_tag.">".$value."</".$xml_tag.">";	
				}
			}
		
			$xmlcontent .= "</lead>";
			$xmlcontent .= "</leads>";
		}
			
		if($url != '' && $xmlcontent != '')
			self::CreateEntity($xmlcontent, $url);
		
	}
	
	function CreateEntity($xmlcontent,$url){
		
		$c = curl_init($url);

		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $xmlcontent);
		curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
		//curl_setopt($c, CURLOPT_CAINFO, "cacert.pem"); //We can provide this certfile if needed (We use GeoTrust and/or GoDaddy)
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false); //(if you are having problems with the https cert, you can disable checking)
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		
		$result = curl_exec($c);
		curl_close($c);
		
		echo $result;
		exit;
		/*if(strpos($result, "Error") !== 'false'){
			echo $result;
			exit;
		}*/
		
	}
	
	function GetAssignedUserId($user_email){
		
		$security_users = array(
			"damir@impulsealarm.com" => "1082",
			"greg@impulsealarm.com" => "1021",
			"vanessa@impulsealarm.com" => "1032",
			"mbuchanan@impulsealarm.com" => "1002",
			"blake@impulsealarm.com" => "1006",
			"barbara@impulsealarm.com" => "1036",
			"tina@impulsealarm.com" => "1045",
			"cameron@impulsealarm.com" => "1028",
			"may@impulsealarm.com" => "1056",
			"mary@impulsealarm.com" => "1081",
			"sdewberry@impulsealarm.com" => "1010",
			"vance@impulsealarm.com" => "1024",
			"vgolin@gmail.com" => "1072",
			"erika@impulsealarm.com" => "1076",
			"cindy@impulsealarm.com" => "1041",
			"bjorn@impulsealarm.com" => "1053",
			"david@impulsealarm.com" => "1000",
			"nicole@impulsealarm.com" => "1044",
			"daniel@impulsealarm.com" => "1052",
			"jimbohinojosa@verizon.net" => "1073",
			"brian@impulsealarm.com" => "1015",
			"jerome@impulsealarm.com" => "1008",
			"sandy@impulsealarm.com" => "1062",
			"ravi@impulsealarm.com" => "1068",
			"careen@impulsealarm.com" => "1029",
			"larry@impulsealarm.com" => "1012",
			"masum@impulsealarm.com" => "1055",
			"golfnutrm2@gmail.com" => "1078",
			"sammy_montejano@yahoo.com" => "1069",
			"steven@impulsealarm.com" => "1049",
			"august@impulsealarm.com" => "1048",
			"tracey@impulsealarm.com" => "1084",
			"nthomas@impulsealarm.com" => "1019",
			"lawrence@impulsealarm.com" => "1040"
		);
		
		if(isset($security_users[$user_email]))
			return $security_users[$user_email];
		else
			return false;
	}	
}
	