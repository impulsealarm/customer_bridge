<?php
/*
Plugin Name: Security Trax
Description: Create Contact or Lead.
Author: Manish Goyal
Author Email: manish@devitechnosolutions.com
Company: Devi Techno Solutions
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define( 'SECURITY_TRAX__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SECURITY_TRAX__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

add_action( 'gform_loaded', array( 'GF_Security_Trax_Bootstrap', 'load' ), 5 );

class GF_Security_Trax_Bootstrap {

	public static function load(){

		if ( ! method_exists( 'GFForms', 'include_feed_addon_framework' ) ) {
			return;
		}

		require_once( 'class.security-trax.php' );

		GFAddOn::register( 'GFSecurityTrax' );
	}
}

function gf_security_trax(){
	return GFSecurityTrax::get_instance();
}