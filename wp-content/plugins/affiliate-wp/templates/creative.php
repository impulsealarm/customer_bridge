<?php global $affwp_creative_atts;?>
<div class="affwp-creative<?php echo esc_attr( $affwp_creative_atts['id_class'] ); ?>">

	<?php if ( ! empty( $affwp_creative_atts['desc'] ) ) : ?>
		<p class="affwp-creative-desc"><?php echo $affwp_creative_atts['desc']; ?></p>
	<?php endif; ?>

	<?php if ( $affwp_creative_atts['preview'] != 'no' ) : ?>

		<?php 
		// Image preview - using ID of image from media library
		if ( $affwp_creative_atts['image_attributes'] ) : ?> 
		<p>
			<a href="<?php echo esc_url( affwp_get_affiliate_referral_url( array( 'base_url' => $affwp_creative_atts['url'] ) ) ); ?>" title="<?php echo esc_attr( $affwp_creative_atts['text'] ); ?>">
				<img src="<?php echo esc_attr( $affwp_creative_atts['image_attributes'][0] ); ?>" width="<?php echo esc_attr( $affwp_creative_atts['image_attributes'][1] ); ?>" height="<?php echo esc_attr( $image_attributes[2] ); ?>" alt="<?php echo esc_attr( $text ); ?>">
			</a>
		</p>
		
		<?php
		// Image preview - External image URL or picked from media library
		elseif ( $affwp_creative_atts['image_link'] ) :
			$image      = $affwp_creative_atts['image_link'];
			$image_size = getimagesize( $image ); // get the image's dimensions
		?>
			<p>
				<a href="<?php echo esc_url( affwp_get_affiliate_referral_url( array( 'base_url' => $affwp_creative_atts['url'] ) ) ); ?>" title="<?php echo esc_attr( $affwp_creative_atts['text'] ); ?>">
					<img src="<?php echo esc_attr( $affwp_creative_atts['image_link'] ); ?>" <?php echo $image_size[3]; ?> alt="<?php echo esc_attr( $affwp_creative_atts['text'] ); ?>">
				</a>
			</p>

		<?php else : // text link preview ?>
			<p>
				<a href="<?php echo esc_url( affwp_get_affiliate_referral_url( array( 'base_url' => $affwp_creative_atts['url'] ) ) ); ?>" title="<?php echo esc_attr( $affwp_creative_atts['text'] ); ?>"><?php echo esc_attr( $affwp_creative_atts['text'] ); ?></a>
			</p>
		<?php endif; ?>

	<?php endif; ?>

	<?php
		echo apply_filters( 'affwp_affiliate_creative_text', '<p>' . __( 'Copy and paste the following:', 'affiliate-wp' ) . '</p>' );

		// Image - media library
		if ( $affwp_creative_atts['image_attributes'] ) {
			$image_or_text = '<img src="' . esc_attr( $affwp_creative_atts['image_attributes'][0] ) . '" alt="' . esc_attr( $affwp_creative_atts['text'] ) .'" />';
		}
		// Image - External URL
		elseif ( $affwp_creative_atts['image_link'] ) {
			$image_or_text = '<img src="' . esc_attr( $affwp_creative_atts['image_link'] ) . '" alt="' . esc_attr( $affwp_creative_atts['text'] ) .'" />';
		}
		// Show site name when no image
		else {
			$image_or_text = esc_attr( $affwp_creative_atts['text'] );
		}
	?>
	
	<?php 
		$creative = '<a href="' . esc_url( affwp_get_affiliate_referral_url( array( 'base_url' => $affwp_creative_atts['url'] ) ) ) .'" title="' . esc_attr( $affwp_creative_atts['text'] ) . '">' . $image_or_text . '</a>';
		echo '<pre id="copyTarget"><code>' . esc_html( $creative ) . '</code></pre><button id="copyButton">Copy</button>'; 
	?>
	
</div>

<script type="text/javascript">
document.getElementById("copyButton").addEventListener("click", function() {
    copyToClipboardMsg(document.getElementById("copyTarget"), "msg");
});

document.getElementById("copyButton2").addEventListener("click", function() {
    copyToClipboardMsg(document.getElementById("copyTarget2"), "msg");
});

document.getElementById("pasteTarget").addEventListener("mousedown", function() {
    this.value = "";
});


function copyToClipboardMsg(elem, msgElem) {
    var succeed = copyToClipboard(elem);
    var msg;
    if (!succeed) {
        msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
    } else {
        msg = "Text copied to the clipboard."
    }
    if (typeof msgElem === "string") {
        msgElem = document.getElementById(msgElem);
    }
    msgElem.innerHTML = msg;
    setTimeout(function() {
        msgElem.innerHTML = "";
    }, 2000);
}

function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}
</script>	