<?php
global $affwp_login_redirect;


if ( wp_is_mobile() ) {
	?>
    <style>
        html {
            overflow: hidden!important;
        }
        body {
            background-image: url("https://impulsealarm.com/wp-content/uploads/2016/02/mobile-login-bg.png")!important;
            background-size: contain!important;
            background-repeat: no-repeat!important;
        }
        .affwp-errors {
            position: absolute;
            width: 100%;
            top: -180px;
        }
        .affwp-errors, .affwp-notice {
            border: 1px solid rgba(208, 46, 34, 0.33);
            background: rgba(251, 132, 123, 0.9);
            color: #790909;
        }
    </style>
	<div class="wrapBridgeLogin">
		<div class="titleBrdigeLogin">
            <h1>Welcome</h1>
        </div>
        <div class="formBridgeLogin">
            <?php affiliate_wp()->login->print_errors(); ?>
            <form class="formBrdigeLoginCss" id="affwp-login-form" class="affwp-form" action="" method="post">
                <?php do_action( 'affwp_affiliate_login_form_top' ); ?>
                <?php do_action( 'affwp_login_fields_before' ); ?>
                <p class="inputWrapBrdige">
                    <i class="material-icons">perm_identity</i>
                    <input class="bridgeuser" id="affwp-login-user-login" class="required" type="text" name="affwp_user_login" placeholder="<?php esc_attr_e( 'Username', 'affiliate-wp' ); ?>" />
                </p>
                <p class="inputWrapBrdige">
                    <i class="material-icons">lock_outline</i>
                    <input class="bridgepassword" id="affwp-login-user-pass" class="password required" type="password" name="affwp_user_pass" placeholder="<?php esc_attr_e( 'Password', 'affiliate-wp' ); ?>" />
                </p>
                <input type="hidden" name="affwp_redirect" value="<?php echo esc_url( $affwp_login_redirect ); ?>"/>
                <input type="hidden" name="affwp_login_nonce" value="<?php echo wp_create_nonce( 'affwp-login-nonce' ); ?>" />
                <input type="hidden" name="affwp_action" value="user_login" />
                <input class="bridgesubmit blue raised" type="submit" class="button" value="<?php esc_attr_e( 'Login', 'affiliate-wp' ); ?>" />
                <?php do_action( 'affwp_login_fields_after' ); ?>
                <?php do_action( 'affwp_affiliate_login_form_bottom' ); ?>
            </form>
            <div class="logoWrapBottom">
                <img src="https://impulsealarm.com/wp-content/uploads/2016/02/bridge-logo-login.png" />
            </div>
        </div>
	</div>
	<?php
} else {
	affiliate_wp()->login->print_errors();
	?>
	<form id="affwp-login-form" class="affwp-form" action="" method="post">
		<?php do_action( 'affwp_affiliate_login_form_top' ); ?>

		<fieldset>
			<legend><?php _e( 'Log into Your Account', 'affiliate-wp' ); ?></legend>

			<?php do_action( 'affwp_login_fields_before' ); ?>

			<p>
				<label for="affwp-login-user-login"><?php _e( 'Username', 'affiliate-wp' ); ?></label>
				<input id="affwp-login-user-login" class="required" type="text" name="affwp_user_login" title="<?php esc_attr_e( 'Username', 'affiliate-wp' ); ?>" />
			</p>

			<p>
				<label for="affwp-login-user-pass"><?php _e( 'Password', 'affiliate-wp' ); ?></label>
				<input id="affwp-login-user-pass" class="password required" type="password" name="affwp_user_pass" />
			</p>

			<p>
				<label class="affwp-user-remember" for="affwp-user-remember">
					<input id="affwp-user-remember" type="checkbox" name="affwp_user_remember" value="1" /> <?php _e( 'Remember Me', 'affiliate-wp' ); ?>
				</label>
			</p>

			<p>
				<input type="hidden" name="affwp_redirect" value="<?php echo esc_url( $affwp_login_redirect ); ?>"/>
				<input type="hidden" name="affwp_login_nonce" value="<?php echo wp_create_nonce( 'affwp-login-nonce' ); ?>" />
				<input type="hidden" name="affwp_action" value="user_login" />
				<input type="submit" class="button" value="<?php esc_attr_e( 'Login', 'affiliate-wp' ); ?>" />
			</p>

			<p class="affwp-lost-password">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost Password?', 'affiliate-wp' ); ?></a>
			</p>

			<?php do_action( 'affwp_login_fields_after' ); ?>
		</fieldset>

		<?php do_action( 'affwp_affiliate_login_form_bottom' ); ?>
	</form>
	<?php
}
?>


