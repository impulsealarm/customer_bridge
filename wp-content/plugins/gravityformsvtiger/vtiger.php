<?php
/**
 * Plugin Name: Gravity Forms Vtiger
 * Plugin URI: http://devitechnosolutions.com
 * Description: Plugin allows user to integrate all of the online forms developed through gravity forms with vtiger, user can easily map form fields with vtiger module fields that includes Leads, Organisation and Contacts and collect all the data submitted through form directly into vtiger
 * Author: Manish Goyal
 * Author URI: www.devitechnosolutions.com
*/

add_action( 'gform_loaded', array( 'GF_Vtiger_Bootstrap', 'load' ), 5 );

class GF_Vtiger_Bootstrap {

	public static function load(){

		if ( ! method_exists( 'GFForms', 'include_feed_addon_framework' ) ) {
			return;
		}

		require_once( 'class-gf-vtiger.php' );

		GFAddOn::register( 'GFVtiger' );
		
		$path = GFCommon::get_base_path();

		move_uploaded_file("images/ajax-loader.gif", $path.'\images\ajax-loader.gif');
		
	}
}

function gf_vtiger(){
	return GFVtiger::get_instance();
}
		
