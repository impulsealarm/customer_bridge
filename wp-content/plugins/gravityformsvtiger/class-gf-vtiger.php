<?php
GFForms::include_feed_addon_framework();

class GFVtiger extends GFFeedAddOn {

	protected $_slug = 'gravityformsvtiger';
	protected $_path = 'gravityformsvtiger/vtiger.php';
	protected $_full_path = __FILE__;
	protected $_url = 'http://www.gravityforms.com';
	protected $_title = 'Vtiger';
	protected $_short_title = 'Vtiger';

	// Members plugin integration
	protected $_capabilities = array( 'gravityforms_vtiger' );

	// Permissions
	protected $_capabilities_settings_page = 'gravityforms_vtiger';
	protected $_capabilities_form_settings = 'gravityforms_vtiger';
	//protected $_capabilities_uninstall = 'gravityforms_mailchimp_uninstall';
	protected $_enable_rg_autoupgrade = true;

	//private static $api;

	private static $_instance = null;

	public static function get_instance() {
		
		if ( self::$_instance == null ) {
			self::$_instance = new GFVtiger();
			
		}

		return self::$_instance;
	}
	
	public function init() {

		parent::init();

	}

	public function init_ajax() {
		parent::init_ajax();

		add_action('wp_ajax_get_Leads_Fields', array($this, 'get_Leads_Fields'));

		add_action('wp_ajax_save_vtiger_mapped_fields', array($this, 'save_vtiger_mapped_fields'));
                        
	}

	public function init_admin() {

		parent::init_admin();

	}
	
	public function plugin_settings_fields() {
		return array(
			array(
				'fields'      => array(
					array(
						'name'              => 'gforms_vtiger_url',
						'label'             => __( 'Website Url', 'gravityformsmailvtiger' ),
						'type'              => 'text',
						'class'             => 'medium',
					),
					array(
						'name'              => 'gforms_vtiger_username',
						'label'             => __( 'Username', 'gravityformsmailvtiger' ),
						'type'              => 'text',
						'class'             => 'medium',
					),
					array(
						'name'              => 'gforms_vtiger_accesskey',
						'label'             => __( 'Access Key', 'gravityformsmailvtiger' ),
						'type'              => 'text',
						'class'             => 'medium',
					),
				)
			),
		);
	}
	
	public function feed_list_page() {
		self::feed_edit_page();
	}
	
	public function feed_edit_page() {
		
		$module = 'Leads';
        
		require_once("includes/functions.php");
		
		$form_id = rgget('id');
		
		$mapped_fields = self::$_instance->get_feeds($form_id);
		
		if(!empty($mapped_fields)) {
			foreach($mapped_fields as $module_fields)
				$field_details = $module_fields['meta'];
			
			$selected_module = array_keys($field_details);
            $module = $selected_module[0];
            $mapped_fields = $field_details[$module];
		}
		   
		$module_fields = get_Leads_Fields($module);
		
        $form_field = GFFormsModel::get_form_meta($form_id);
        
		?>
		<style>
				#update_dialog_success, #update_dialog_error { display: none;}
				.wp-list-table tbody tr td { max-width: 220px;}
				#vtiger_module { font-size: 14px; }
		</style>
			
        <script type="text/javascript">
			jQuery(document).ready(function(){

				jQuery("#vtiger_module").on("change", function(){
					var selected_module = jQuery(this).val();
					if(selected_module != ''){
						get_selected_module_fields(selected_module);
						jQuery("#please_wait_container").show();
					}
				});

				jQuery("#gform_vtiger_field_mapping_form").submit(function(e){
					e.preventDefault();

					var FormData = jQuery("#gform_vtiger_field_mapping_form").serialize();

					jQuery.ajax({
				        type:'POST',
				        url : 'admin-ajax.php',
				        data: FormData,
						dataType:'json',
						success: function(data){
							if(data.success){
								location.reload();
								jQuery("#update_dialog_error").css("display", "none");
								jQuery("#update_dialog_success").css("display", "block");
							} else {
								jQuery("#update_dialog_error").css("display", "block");
								return;
							}
					    }
					});
				});
			});

			function get_selected_module_fields(selected_module){
				jQuery.ajax({
			        type:'POST',
			        url : 'admin-ajax.php',
			        data: {action: 'get_Leads_Fields', module: selected_module},
					dataType:'json',

			        success: function(data){

			        	jQuery("#please_wait_container").hide();
						   
				        var items = '';
				        items += '<option value="">Select</option>';
				        jQuery.each(data, function(i, val) {
					        if(val['type']['name'] == 'reference')
					        	return;
					        else
					        	items += '<option value="' + val['name'] + '">'+ val['label'] +'</option>';
				        });
				        jQuery(".vtiger_fields").html(items);
				    }
	        	});
			}
		
		</script>

       	<span id="please_wait_container" style="position: absolute; margin-top: 20%; right: 50%; opacity: 1.8; z-index: 1000; display: none;">
           	<img src="<?php echo GFCommon::get_base_url(); ?>/images/spinner.gif" class="gf_loader">
        </span>
		
		<div class="updated below-h2" id="update_dialog_success">
            <p>
               	<strong>Mapped successfully.</strong>
            </p>
       	</div>
		
		<div class="updated below-h2" id="update_dialog_error">
            <p>
               	<strong>Please select all required crm fields.</strong>
            </p>
       	</div>
						
		<form method="post" id="gform_vtiger_field_mapping_form" action="">
			<input type="hidden" name="action" value="save_vtiger_mapped_fields" /> 
			<input type="hidden" id="form_id" name="form_id" value="<?php echo $form_id; ?>">
			 	
			<h3>
	        	<span> 
	        		<?php _e("Vtiger Field Mapping", "gravityforms") ?>
	        	</span>
	        	<span>
	        		<select id = "vtiger_module" name = "vtiger_module" style="float: right;">
	        			<option value="Leads" <?php if(isset($module) && "Leads" == $module) echo 'selected';?>>Leads</option>
	        			<option value="Accounts" <?php if(isset($module) && "Accounts" == $module) echo 'selected';?>>Organizations</option>
	        			<option value="Contacts" <?php if(isset($module) && "Contacts" == $module) echo 'selected';?>>Contacts</option>
	        		</select>
	        	</span>
			</h3>
	        <?php 
				if($form_id == "23") { 
					self::showForm23Table($form_field, $module_fields, $mapped_fields); 
				} else {
			?> 
				<table class="form-table gforms_form_settings" cellspacing="0">

					<tbody id="the-list">
						
						<?php 	

							$z = 0;  
							$i = 0;          				
							foreach($form_field['fields'] as $form_Field):
								if($z == 0 || ($z % 2) == 0)
									echo '<tr>';
									
								if($i > 0 && $i > $z) 
									$z = $i;
							
								if(!empty($form_Field['inputs'])){
									$i = $z;
									foreach($form_Field['inputs'] as $adv_Field):	
										$form_Field = $adv_Field;
						?>
							<td>
								<strong>
								<?php 
										echo $form_Field['label'];
								?>
								</strong>
							</td>
							<td>
								<select name="vtiger_fields[<?php echo $form_Field['label']?>]" class="vtiger_fields">
									<option value="">Select</option>
							
								<?php 
									foreach($module_fields as $vtiger_Field): 
										if($vtiger_Field['type']['name'] == 'reference')
											continue;
								?>
							
									<option value="<?php echo $vtiger_Field['name']; ?>" 
										<?php 
											if(
												isset($mapped_fields[$form_Field['label']]) && 
												$mapped_fields[$form_Field['label']] != '' && 
												$mapped_fields[$form_Field['label']] == $vtiger_Field['name']
											)
												echo 'selected';
										?>>
										<?php echo $vtiger_Field['label'];?>
									</option>
							
								<?php endforeach; ?>
								
								</select>
							</td>
							<?php 
								if($i > 0 && ($i % 2) == 1) {
									echo "</tr>";
									$i++;
								} else{ 
									$i++;
								}
							?>
						
							<?php endforeach; ?>
							<?php } else { ?>
								<td>
								<strong>
								<?php 
										echo $form_Field['label'];
								?>
								</strong>
							</td>
							<td>
								<select name="vtiger_fields[<?php echo $form_Field['label']?>]" class="vtiger_fields">
									<option value="">Select</option>
							
								<?php 
									foreach($module_fields as $vtiger_Field): 
										if($vtiger_Field['type']['name'] == 'reference')
											continue;
								?>
							
									<option value="<?php echo $vtiger_Field['name']; ?>" 
										<?php 
											if(
												isset($mapped_fields[$form_Field['label']]) && 
												$mapped_fields[$form_Field['label']] != '' && 
												$mapped_fields[$form_Field['label']] == $vtiger_Field['name']
											)
												echo 'selected';
										?>>
										<?php echo $vtiger_Field['label'];?>
									</option>
							
								<?php endforeach; ?>
								
								</select>
							</td>
							<?php }?>
							<?php 
								if($z > 0 && ($z % 2) == 1) {
									echo "</tr>";
									$z++;
								} else{ 
									$z++;
								}
							?>
						<?php endforeach;?>
					</tbody>
			</table>
		<?php } ?>
		<p class="submit">
        	<?php
            	$button_label = __("Save Changes", "gravityforms");
                $vtiger_button = '<input class="button-primary" type="submit" value="' . $button_label . '" name="save"/>';
                echo apply_filters("gform_save_changes_button", $vtiger_button);
           ?>
        </p>
    </form>
	<?php //parent::feed_edit_page( $form, $feed_id );
	}
	
	public function feed_list_columns() {
		return array(
			'feedName'            => __( 'Form Field Name', 'gravityformsmailvtiger' ),
			'mailchimp_list_name' => __( 'Vtiger Field Name', 'gravityformsmailvtiger' )
		);
	}
	

	public static function get_Leads_Fields(){
		
		if(isset($_POST['module']) && $_POST['module'] != ''){
			require_once("includes/functions.php");
			
			$moduleName = $_POST['module'];
			
			$vtiger_settings = get_option("gravityformsaddon_gravityformsvtiger_settings");
			
			$url = $vtiger_settings['gforms_vtiger_url'];
		   
			$user_name = $vtiger_settings['gforms_vtiger_username'];
			$accessKey = $vtiger_settings['gforms_vtiger_accesskey'];
			
			$ws_url =  $url . '/webservice.php';
			   
			$loginObj = login($ws_url, $user_name, $accessKey);
			$sessionName = $loginObj->sessionName;
			
		
			$postParams = array(
		         'elementType'=> $moduleName,
				 'operation' => 'describe',
				 'sessionName' => $sessionName,
			);
			
			$response = getHttpRequest($ws_url, $postParams);
			
			$response = json_decode($response, true);
			
			if(!empty($response['result']))
				$response = $response['result']['fields'];
		   	
			print_r(json_encode($response));
			
			die();
			
		}
		die();
	}

	public static function save_vtiger_mapped_fields(){
		if(!empty($_POST)){
			if(isset($_POST['vtiger_module'])) {
				require_once("includes/functions.php");
				$module_fields = get_Leads_Fields($_POST['vtiger_module']);
				foreach($module_fields as $fields){
					if(isset($fields['mandatory']) && $fields['mandatory'] != '' && $fields['name'] != 'assigned_user_id')
						$mandatory_fields[$fields['name']] = $fields['label'];
				}
			}
			
			$vtiger_fields = $_POST['vtiger_fields'];
				
			if(!empty($mandatory_fields)) {
				
				foreach($mandatory_fields as $key => $mandatory_field){
					if(in_array($key, $vtiger_fields)) {
						unset($mandatory_fields[$key]); 
					}
				}
				if(count($mandatory_fields) > 0) {
					echo json_encode(array("error" => 'Please select all required crm fields'));
			    } else {
			    	$form = array();
		
					$form_id = $_POST['form_id'];
			           
			        $vtiger_module = $_POST["vtiger_module"];
			            
			        $form[$vtiger_module] = $_POST["vtiger_fields"];
			        
			        $form_feed = self::$_instance->get_feeds($form_id);
			       	
			        foreach($form_feed as $form_values) {
			        	$form_details = $form_values;
			        }
			        if ($form_details['form_id'] == $form_id) {
						self::$_instance->update_feed_meta($form_details['id'], $form);
					} else {
						$value = true;
						self::$_instance->insert_feed($form_id,$value , $form);
					}
				   	echo json_encode(array("success" => 'true'));
			    }
			    die();
			}
		}
	}
	
	public function maybe_process_feed($entry, $form){
		
		$module_data = array();
		
		$entry_data = parent::maybe_process_feed($entry, $form);
		
		$reference_id = $entry_data['id']; // This is a gravity form lead id which is used as reference while creating Referral th. gravity forms
		
		$form_id = $entry_data['form_id'];
			
		if ( $entry_data['status'] == 'active' && $form_id != '23'){
			
			$vtiger_module_fields = self::$_instance->get_feeds($form_id);
			
			foreach($vtiger_module_fields as $vtiger_fields){
				$module = array_keys($vtiger_fields['meta']);
				$module = $module[0];
				$module_fields = $vtiger_fields['meta'][$module];
			}
			foreach($form['fields'] as $form_field){
				if(!empty($form_field['inputs'])){
					foreach($form_field['inputs'] as $adv_value){
						$field_label = $adv_value['label'];
						$field_id = (string)$adv_value["id"];
						$field_value = $entry_data[$field_id];
						if(isset($module_fields[$field_label]) && $module_fields[$field_label] != '')
							$module_data[$module][$module_fields[$field_label]] = $field_value;
					}
				}else {
					$field_label = $form_field['label'];
					$field_id = $form_field['id'];
					$field_value = $entry_data[$field_id];
					if(isset($module_fields[$field_label]) && $module_fields[$field_label] != '')
						$module_data[$module][$module_fields[$field_label]] = $field_value;
				}
			}
			
			if($form_id == '35'){
				$module_data[$module]['source_url'] = $entry_data['source_url'];
				$module_data[$module]['leadsource'] = 'Landing Page';
			}
				
			self::create_Vtiger_Module_Entry($module_data, $form_id, $reference_id);
	    } else {
			if($entry_data['status'] == 'active')
				self::processForm23Feed($entry_data, $form);
		}
	    return $entry;
	}
	
	public function create_Vtiger_Module_Entry($module_data, $form_id = '0', $reference_id = '0'){
		
		require_once("includes/functions.php");
    	
		global $user_email;
		
    	$vtiger_settings = get_option("gravityformsaddon_gravityformsvtiger_settings");
			
		$url = $vtiger_settings['gforms_vtiger_url'];
	   
		$user_name = $vtiger_settings['gforms_vtiger_username'];
		
		$accessKey = $vtiger_settings['gforms_vtiger_accesskey'];
		
		$module_name = array_keys($module_data);
		
		$module_name = $module_name[0];
		
		$module_data = $module_data[$module_name];
		
		$affiliate_id = affiliate_wp()->tracking->get_affiliate_id();
		
		if(!isset($module_data['firstname']) && isset($module_data['lastname']) && $module_data['lastname'] != ''){

			$nameParts = explode(' ', $module_data['lastname']);
			
			if(count($nameParts) > 1){
				$firstname = trim($nameParts['0']);
				$module_data['firstname'] = $firstname;
				
				$last_name = '';
				foreach($nameParts as $key => $value){
					
					if($key != 0){
						$last_name .= $value . ' ';
					}
				}
				$module_data['lastname'] = $last_name;
				
				if($last_name == '') $module_data['lastname'] = 'N/A';
			
			}
		}
		
		//if referral is created against provided visit id and reference then set referral of the lead
		if($module_name == "Leads" && $reference_id > 0 && $affiliate_id){
			
			$args					= array();
			$args['affiliate_id']	= $affiliate_id;
			$args['reference']		= $reference_id;
			$args['context'] 		= "gravityforms";
			$args['number']       	= '-1';

			$referral = affiliate_wp()->referrals->get_referrals( $args );
			
			$total_referral = count($referral);
			
			if($total_referral == '1')
				$module_data['referral_id'] = $referral['0']->referral_id;
		}
		
		if(
			$affiliate_id && $form_id == '35' &&
			affiliate_wp()->tracking->is_valid_affiliate($affiliate_id) 
		){
			$realtor_data = read("affiliate_id", $affiliate_id, "Realtor", $url, $user_name, $accessKey);
			
			if(!empty($realtor_data->result['0'])){
				
				$realtor_info = $realtor_data->result['0'];
				
				$module_data['realtorid'] = $realtor_info->id;
				$module_data['assigned_user_id'] = $realtor_info->assigned_user_id;
				
			} else {
				if(isset($module_data['assigned_user_id']) && $module_data['assigned_user_id'] != ''){ 
					
					$user_email = $module_data['assigned_user_id'];
					
					$user_data = read("email1", $user_email, "Users", $url, $user_name, $accessKey);
				
					if(isset($user_data->result) && !empty($user_data->result)){
						
						$vtiger_user_info = $user_data->result['0'];
						
						$module_data['assigned_user_id'] = $vtiger_user_info->id;
						
					} else {
					
						unset($module_data['assigned_user_id']);
					
					}
				}
			}
		} else {
			if(isset($module_data['assigned_user_id']) && $module_data['assigned_user_id'] != ''){ 
				
				$user_email = $module_data['assigned_user_id'];
				
				$user_data = read("email1", $user_email, "Users", $url, $user_name, $accessKey);
			
				if(isset($user_data->result) && !empty($user_data->result)){
					
					$vtiger_user_info = $user_data->result['0'];
					
					$module_data['assigned_user_id'] = $vtiger_user_info->id;
					
				} else {
				
					unset($module_data['assigned_user_id']);
				
				}
			}
		}
		
		$response = CreateVtigerEntry($url, $user_name, $accessKey,$module_name, $module_data);
	}
	
	public function showForm23Table($form_field, $module_fields, $mapped_fields = array()){ ?>
		
		<table class="form-table gforms_form_settings" cellspacing="0">

			<tbody id="the-list">
					
				<?php 	

					$z = 0;  
					$i = 0;          				
					
					foreach($form_field['fields'] as $form_Field):
					
						if($form_Field['type'] == 'section') continue;
						
						if($z == 0 || ($z % 2) == 0)
							echo '<tr>';
							
						if($i > 0 && $i > $z) 
							$z = $i;
					
						if(!empty($form_Field['inputs']) && $form_Field['type'] != 'time'){
							$i = $z;
								
							$heading = $form_Field['label'];
							
							foreach($form_Field['inputs'] as $adv_Field):	
								$form_Field = $adv_Field;
				?>
					<td>
						<strong>
						<?php 
						
							if($heading != '')
								echo $heading." ".$form_Field['label'];
							else
								echo $form_Field['label'];
								
							if($heading != '')
								$form_Field['label'] = $heading." :: ".$form_Field['label'];
							
						?>
						</strong>
					</td>
					<td>
						<select name="vtiger_fields[<?php echo $form_Field['label']?>]" class="vtiger_fields">
							<option value="">Select</option>
					
						<?php 
							foreach($module_fields as $vtiger_Field): 
								if($vtiger_Field['type']['name'] == 'reference')
									continue;
						?>
					
							<option value="<?php echo $vtiger_Field['name']; ?>" 
								<?php 
									if(
										isset($mapped_fields[$form_Field['label']]) && 
										$mapped_fields[$form_Field['label']] != '' && 
										$mapped_fields[$form_Field['label']] == $vtiger_Field['name']
									)
										echo 'selected';
								?>>
								<?php echo $vtiger_Field['label'];?>
							</option>
					
						<?php endforeach; ?>
						
						</select>
					</td>
					<?php 
						if($i > 0 && ($i % 2) == 1) {
							echo "</tr>";
							$i++;
						} else{ 
							$i++;
						}
					?>
				
					<?php endforeach; ?>
					<?php } else { ?>
						<td>
						<strong>
						<?php 
								echo $form_Field['label'];
						?>
						</strong>
					</td>
					<td>
						<select name="vtiger_fields[<?php echo $form_Field['label']?>]" class="vtiger_fields">
							<option value="">Select</option>
					
						<?php 
							foreach($module_fields as $vtiger_Field): 
								if($vtiger_Field['type']['name'] == 'reference')
									continue;
						?>
					
							<option value="<?php echo $vtiger_Field['name']; ?>" 
								<?php 
									if(
										isset($mapped_fields[$form_Field['label']]) && 
										$mapped_fields[$form_Field['label']] != '' && 
										$mapped_fields[$form_Field['label']] == $vtiger_Field['name']
									)
										echo 'selected';
								?>>
								<?php echo $vtiger_Field['label'];?>
							</option>
					
						<?php endforeach; ?>
						
						</select>
					</td>
					<?php }?>
					<?php 
						if($z > 0 && ($z % 2) == 1) {
							echo "</tr>";
							$z++;
						} else{ 
							$z++;
						}
					?>
				<?php endforeach;?>
			</tbody>
        </table>
	<?php
	}
	
	public function processForm23Feed($entry_data, $form){
		
		$module_data = $form_data = array();
		
		if ($entry_data['status'] == 'active' ){
			
			$form_id = $entry_data['form_id'];
			
			$vtiger_module_fields = self::$_instance->get_feeds($form_id);
			
			foreach($vtiger_module_fields as $vtiger_fields){
				$module = array_keys($vtiger_fields['meta']);
				$module = $module[0];
				$module_fields = $vtiger_fields['meta'][$module];
			}
			
			foreach($form['fields'] as $form_field){
				
				if(!empty($form_field['inputs']) && $form_field['type'] != 'time'){
				
					$form_label = $form_field['label']." :: ";
				
					foreach($form_field['inputs'] as $adv_value){
						
						if($form_label != '')
							$adv_label = $form_label.$adv_value['label'];
						else
							$adv_label = $adv_value['label'];
						
						$field_label = $adv_label;
						
						$field_id = (string)$adv_value["id"];
						
						$field_value = $entry_data[$field_id];
						
						if(isset($module_fields[$field_label]) && $module_fields[$field_label] != '')
							$module_data[$module][$module_fields[$field_label]] = $field_value;
						
						$form_data[$field_label] = $field_value;
					}
				}else {
					$field_label = $form_field['label'];
					
					$field_id = $form_field['id'];
					
					$field_value = $entry_data[$field_id];
					
					if(isset($module_fields[$field_label]) && $module_fields[$field_label] != '')
						$module_data[$module][$module_fields[$field_label]] = $field_value;
					
					$form_data[$field_label] = $field_value;
				}
			}
			
			$module_data = self::get_form23_lead_data($module_data, $form_data);
			
			$module_data['Leads']['leadsource'] = 'New Lead Form';
			
			self::create_Vtiger_Module_Entry($module_data, $form_id);
		}
	}
	
	function get_form23_lead_data($lead_data, $form_data){
		
		require_once("includes/functions.php");
    	
		$realtor_fields = array();
			
		$phone = $form_data['Realtor Phone'];
    	
		$email = $form_data['Realtor Email'];
    	
    	$vtiger_settings = get_option("gravityformsaddon_gravityformsvtiger_settings");
			
		$url = $vtiger_settings['gforms_vtiger_url'];
	   
		$user_name = $vtiger_settings['gforms_vtiger_username'];
		
		$accessKey = $vtiger_settings['gforms_vtiger_accesskey'];
		
		if($phone)
			$realtor_fields['realtor_phone'] = $phone;
		
		if($email)
			$realtor_fields['realtor_email'] = $email;
			
		if(!empty($realtor_fields))	
			$realtor = GetRealtorId($url, $user_name, $accessKey, $realtor_fields);
		
		if(
			!empty($realtor) && isset($realtor['success']) && 
			isset($realtor['result']) && !empty($realtor['result'])
		){
			$realtor = $realtor['result'];
			
			if(isset($realtor['realtor_id']) && $realtor['realtor_id'] > 0)
				$lead_data['Leads']['realtorid'] =  $realtor['realtor_id'];
		}
		
		$lead_data = $lead_data['Leads'];
		
		if(isset($lead_data['phone']) && $lead_data['phone'] != ''){
				
			$filteredPhone = preg_replace('/\D/', '', $lead_data['phone']);
		
			$filteredPhone = substr($filteredPhone, -10);
			
			if(strlen($filteredPhone) > 6)
				$lead_data['phone'] = $filteredPhone;
		}
		
		if(isset($lead_data['callback_time']) && $lead_data['callback_time'] != '')
			$lead_data['callback_time'] = date("H:i:s", strtotime($lead_data['callback_time']));
		
		$lead_info = array();
		
		$lead_info['Leads'] = $lead_data;
		
		return $lead_info;
	}	
}
	