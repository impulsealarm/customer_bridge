<?php

GFForms::include_feed_addon_framework();

class GFCustomLogic extends GFFeedAddOn {

	protected $_slug = 'gravityformscustomconditionlogic';
	protected $_path = 'gravityformscustomconditionlogic/customconditionlogic.php';
	protected $_full_path = __FILE__;
	protected $_url = 'http://www.gravityforms.com';
	protected $_title = 'Custom Condition Logic';
	protected $_short_title = 'Custom Condition Logic';

	// Members plugin integration
	protected $_capabilities = array( 'gravityforms_customconditionlogic' );

	// Permissions
	protected $_capabilities_settings_page = 'gravityforms_customconditionlogic';
	protected $_capabilities_form_settings = 'gravityforms_customconditionlogic';
	protected $_enable_rg_autoupgrade = true;

	//private static $api;

	private static $_instance = null;

	public static function get_instance() {
		
		if ( self::$_instance == null ) {
			self::$_instance = new GFCustomLogic();
			
		}

		return self::$_instance;
	}
	
	public function init() {

		parent::init();
		
		add_action("gform_register_init_scripts", array($this, 'temporary_save_custom_rules'),2,10);
		//add_action("wp",array($this, 'temporary_save_custom_rules'));
		
		self::setup();
		
		wp_enqueue_script( 'gform_custom_conditional_js',  plugins_url( '', __FILE__ ) . '/js/custom_conditional_logic.js', array( 'jquery'), $version );
	}
	
	public function setup() {

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		global $wpdb;

		if ( ! empty( $wpdb->charset ) ) {
			$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
		}
		if ( ! empty( $wpdb->collate ) ) {
			$charset_collate .= " COLLATE $wpdb->collate";
		}
		
		$form_table_name = "wp_gf_custom_conditions";
		
		$sql = 'CREATE TABLE ' . $form_table_name . " (
              id INT NOT NULL AUTO_INCREMENT ,
              form_id INT NULL DEFAULT NULL ,
              custom_rule TEXT NULL DEFAULT NULL ,
              wp_gf_custom_conditions ADD  custom_rule_json_format TEXT NULL DEFAULT NULL,
              PRIMARY KEY  (id)
            ) $charset_collate;";
		
		dbDelta( $sql );

		// $wpdb->query("ALTER TABLE  `wp_gf_custom_conditions` ADD  `custom_rule_json_format` TEXT NULL DEFAULT NULL");
		
		
	}
	public function init_ajax() {

		parent::init_ajax();
		
		add_action('wp_ajax_save_custom_conditional_rules', array($this, 'save_custom_conditional_rules'));
		add_action('wp_ajax_reset_custom_conditions',array($this, 'reset_custom_conditions'));
	}

	public function init_admin() {

		parent::init_admin();

	}
	
	public function feed_list_page() {
		self::feed_edit_page();
	}
	
	public function feed_edit_page() {
		
		$form_id = rgget('id');
		
		$form_field = GFFormsModel::get_form_meta($form_id);

		$formFields = $form_field['fields'];
		
		$custom_conditions = self::getCustomConditions($form_id);
		
		$total_custom_rule = "1";
								
	?>
		<style>
			.add_another_rule, .delete_rule {
				padding-top:10px;
				margin-left: 6px;
			}
			.gf_conditional_logic_rules_container{
				width : 90%;
			}
			.hide_icon, #update_dialog_success, .hideRule, #error{
				display:none;
			}
			#custom_conditional_logic_container select, 
			#custom_conditional_logic_container input{
				margin-left: 5px;
				min-width: 80px;
			}
			.MTB10{
				margin-top: 10px;
				margin-bottom: 10px;
			}
			
			.sub_condition a{
			    margin-left: 5px;
			}
			
			.wp-admin input{
				padding: 2px;
			    line-height: 28px;
			    height: 28px;
			    vertical-align: middle;
			}
		</style>
		<form method="post" id="gform_custom_logic_form" action="">
			<input type="hidden" name="action" value="save_custom_conditional_rules" /> 
			<input type="hidden" id="form_id" name="form_id" value="<?php echo $form_id; ?>">
		 	
			<h3><span><?php _e("Custom Conditions", "gravityforms") ?></span></h3>
			
			<?php 
				if(empty($form_field)){
					echo "<div class='updated below-h2' id='update_dialog_error'><p><strong>This form doesn't have any fields yet. Please Add Some Fields to Set condition</strong></p></div>";
				} else {
			?>
			<div class='updated below-h2' id="update_dialog_success">
				<p>
					<strong>Save Successfully</strong>
				</p>
			</div>
			<div class='updated below-h2' id='error'>
				<p>
					<strong>Please set condition values</strong>
				</p>
			</div>
			
			<?php $hidden_div = self::getCustomConditionRule($form_id, "0", true);  echo $hidden_div; ?>
			
			<div class="custom_conditional_logic_setting ">
				
				<div id="custom_conditional_logic_container" style="padding-top: 10px;">
					<?php 
						if(!empty($custom_conditions)){
							
							$total_custom_rule = count($custom_conditions);
							
							foreach($custom_conditions as $key => $custom_condition){
								
								if(
									isset($custom_condition['if']) && !empty($custom_condition['if']) &&
									isset($custom_condition['then']) && !empty($custom_condition['then'])
								){ 			
					?>
									<div class="gf_conditional_logic_rules_container" id="index_<?php echo $key; ?>"> 
							
										<a class="add_another_rule pull-right" title="add another rule"><i class="gficon-add"></i></a>
										
										<a class="delete_rule pull-right <?php echo $total_custom_rule == '1' ? "hide_icon" : ""; ?>" title="remove this rule" onclick="deleteCustomRule(<?php echo $key; ?>);"><i class="gficon-subtract"></i></a>
										
										<div class="if_rule">
											
											<p>If</p>
											
											<?php 
												
												$if_conditions = $custom_condition['if'];
												
												$total_sub_conditions = count($if_conditions);
												
												foreach($if_conditions as $sub_rule_id => $if_condition){
											
													if(isset($if_condition['operation']) && $if_condition['operation'] != '') { ?>
													
														<input type="hidden" name="if_condition_sub_rule" value="<?php echo $total_sub_conditions; ?>" />
														
														<div class="sub_condition MTB10" id="rule_id_<?php echo $sub_rule_id; ?>">
															
															<select id="if_rule_operation_<?php echo $key; ?>_<?php echo $sub_rule_id; ?>" name="if_form_rules[<?php echo $key; ?>][if_sub_rule][<?php echo $sub_rule_id; ?>][operation]">
																<option value="and" <?php if($if_condition['operation'] == 'and') echo 'selected'; ?>>AND</option>
																<option value="or" <?php if($if_condition['operation'] == 'or') echo 'selected'; ?>>OR</option>
															</select>
															
															<select id="if_form_fields_<?php echo $key; ?>_<?php echo $sub_rule_id; ?>" name="if_form_rules[<?php echo $key; ?>][if_sub_rule][<?php echo $sub_rule_id; ?>][fieldid]">
																<?php 
																	foreach($formFields as $formField): 
																		if(!empty($formField->inputs)){
																			foreach($formField->inputs as $sub_fields){
																?>
																				<option value="<?php echo $sub_fields['id']; ?>" 
																					<?php if($if_condition['fieldid'] == $sub_fields['id']) echo 'selected';?>>
																				<?php echo $formField->label.' '.$sub_fields['label']; ?></option>
																<?php 		} 
																		} else { ?>
																			<option value="<?php echo $formField->id; ?>" 
																				<?php if($if_condition['fieldid'] == $formField->id) echo 'selected';?>>
																			<?php echo $formField->label; ?></option>
																<?php 	} 
																	endforeach; 
																?>																
															</select>
															
															<select id="if_rule_operator_<?php echo $key; ?>_<?php echo $sub_rule_id; ?>" name="if_form_rules[<?php echo $key; ?>][if_sub_rule][<?php echo $sub_rule_id; ?>][operator]">
																<option value="is" <?php if($if_condition['operator'] == 'is') echo 'selected';?>>is</option>
																<option value="isnot"<?php if($if_condition['operator'] == 'isnot') echo 'selected';?>>is not</option>
																<option value=">" <?php if($if_condition['operator'] == '>') echo 'selected';?>>greater than</option>
																<option value="<" <?php if($if_condition['operator'] == '<') echo 'selected';?>>less than</option>
															</select>
															
															<input type="text" placeholder="Enter a value" id="if_rule_value_<?php echo $key; ?>_<?php echo $sub_rule_id; ?>" name="if_form_rules[<?php echo $key; ?>][if_sub_rule][<?php echo $sub_rule_id; ?>][value]" value="<?php echo $if_condition['value']; ?>">
															
															<a class="delete_sub_rule" title="Delete Sub Condition"><i class="gficon-trash"></i></a>
														
														</div>
														
													<?php } else { ?>
													
															<select id="if_form_fields_<?php echo $key; ?>" name="if_form_rules[<?php echo $key; ?>][fieldid]">
														
																<?php 
																	foreach($formFields as $formField): 
																		if(!empty($formField->inputs)){
																			foreach($formField->inputs as $sub_fields){
																?>
																				<option value="<?php echo $sub_fields['id']; ?>" 
																					<?php if(isset($if_condition['fieldid']) && $if_condition['fieldid'] == $sub_fields['id']) echo 'selected';?>>
																				<?php echo $formField->label.' '.$sub_fields['label']; ?></option>
																<?php 		} 
																		} else { ?>
																			<option value="<?php echo $formField->id; ?>" 
																				<?php if(isset($if_condition['fieldid']) && $if_condition['fieldid'] == $formField->id) echo 'selected';?>>
																			<?php echo $formField->label; ?></option>
																<?php 	} 
																	endforeach; 
																?>
															</select>
															
															<select id="if_rule_operator_<?php echo $key; ?>" name="if_form_rules[<?php echo $key; ?>][operator]">
																<option value="is" <?php if(isset($if_condition['operator']) && $if_condition['operator'] == 'is') echo 'selected'; ?>>is</option>
																<option value="isnot" <?php if(isset($if_condition['operator']) && $if_condition['operator'] == 'isnot') echo 'selected'; ?>>is not</option>
																<option value=">" <?php if(isset($if_condition['operator']) && $if_condition['operator'] == '>') echo 'selected';?>>greater than</option>
																<option value="<" <?php if(isset($if_condition['operator']) && $if_condition['operator'] == '<') echo 'selected';?>>less than</option>
															</select>
											
															<input type="text" placeholder="Enter a value" id="if_rule_value_<?php echo $key; ?>" name="if_form_rules[<?php echo $key; ?>][value]" 
															value="<?php echo isset($if_condition['value'])?$if_condition['value']:''; ?>" />
															
															<input type="button" name="add_boolean_value" value="Add Boolean Value" class="add_boolean_value">
										
													<?php }
												}
											?>
											
										</div>
										
										<div class="then_rule">	
											<p>Then</p>
											
											<select id="then_form_fields_<?php echo $key; ?>" name="then_form_fields_<?php echo $key; ?>">
												<?php 
													foreach($formFields as $formField): 
														if(!empty($formField->inputs)){
															foreach($formField->inputs as $sub_fields){
												?>
																<option value="<?php echo $sub_fields['id']; ?>" 
																	<?php if($custom_condition['then']['fieldid'] == $sub_fields['id']) echo 'selected';?>>
																<?php echo $formField->label.' '.$sub_fields['label']; ?></option>
												<?php 		} 
														} else { ?>
															<option value="<?php echo $formField->id; ?>" 
																<?php if($custom_condition['then']['fieldid'] == $formField->id) echo 'selected';?>>
															<?php echo $formField->label; ?></option>
												<?php 	} 
													endforeach; 
												?>
											</select>
											
											<select id="then_rule_operator_<?php echo $key; ?>" name="then_rule_operator_<?php echo $key; ?>">
												<option value="is" <?php if($custom_condition['then']['operator'] == 'is') echo 'selected'; ?>>is</option>
												<option value="isnot" <?php if($custom_condition['then']['operator'] == 'isnot') echo 'selected'; ?>>is not</option>
												<option value=">" <?php if($custom_condition['then']['operator'] == '>') echo 'selected';?>>greater than</option>
												<option value="<" <?php if($custom_condition['then']['operator'] == '<') echo 'selected';?>>less than</option>
											</select>
											
											<input type="text" placeholder="Enter a value" id="then_rule_value_<?php echo $key; ?>" name="then_rule_value_<?php echo $key; ?>" 
											value="<?php echo $custom_condition['then']['value']; ?>" />
										</div>
								</div>
							<?php 
								} else{
									$custom_rule = self::getCustomConditionRule($form_id, "1");
									echo $custom_rule;
								}
							}
						} else {
							$custom_rule = self::getCustomConditionRule($form_id, "1");
							echo $custom_rule;
						}
					?>
				</div>	
			</div>
			<p class="submit">
				<input type="hidden" id = "total_custom_rule" name="total_custom_rule" value="<?php echo $total_custom_rule; ?>" />
        		
				<?php
            		$button_label = __("Save Changes", "gravityforms");
                	$rule_button = '<input class="button-primary" type="submit" value="' . $button_label . '" name="save"/>';
                	echo apply_filters("gform_save_changes_button", $rule_button);
           		?>
           		
           		<input type="button" id="reset" name="reset_<?php echo $form_id; ?>" class="button-primary" value="Reset Conditions" />
        	</p>
			<?php 	
				}
		echo '</form>';
	}
	
	function save_custom_conditional_rules(){
		
		global $wpdb;

		if(!empty($_POST) && isset($_POST['total_custom_rule']) && $_POST['total_custom_rule'] != ''){
			
			$total_rules = $_POST['total_custom_rule'];
			
			$custom_rule = array();
			
			for($i = 1; $i<=$total_rules; $i++){
				
				if(isset($_POST['if_form_rules'][$i]) && !empty($_POST['if_form_rules'][$i])){
				
					$if_cond = $_POST['if_form_rules'][$i];
					
					if(isset($if_cond['fieldid']) && $if_cond['fieldid'] === '' ||
					isset($if_cond['operator']) && $if_cond['operator'] === '' ||
					isset($if_cond['value']) && $if_cond['value'] === '')
						continue;
				
					$custom_rule[$i]['if'][] = array("fieldid" => $if_cond['fieldid'], "operator" => $if_cond['operator'], "value" => $if_cond['value']);
				
					if(isset($if_cond['if_sub_rule']) && !empty($if_cond['if_sub_rule'])){
						
						$sub_conds = $if_cond['if_sub_rule'];
						
						foreach($sub_conds as $sub_cond)
							$custom_rule[$i]['if'][] = array("fieldid" => $sub_cond['fieldid'] , "operator" => $sub_cond['operator'], "value" => $sub_cond['value'], "operation" => $sub_cond['operation']);
						
					}
					$custom_rule[$i]['then']['fieldid'] = $_POST['then_form_fields_'.$i]; 
					$custom_rule[$i]['then']['operator'] = $_POST['then_rule_operator_'.$i]; 
					$custom_rule[$i]['then']['value'] = $_POST['then_rule_value_'.$i];
				} 
			}
			
			if(!empty($custom_rule)){

				$final_conditions = array();
				
				foreach($custom_rule as $condition){
					
					$if_conditions = $condition['if'];
					
					$then_condition = $condition['then'];
					
					$javascript_con = "if( ";
					
					$and_condition = $or_condition = "";
					
					foreach($if_conditions as $if_condition){
						
						if(isset($if_condition['operation']) && $if_condition['operation'] != ''){
							
							if($if_condition['operation'] == 'or'){

								$or_condition .= " || ";
								
								$or_condition .= "gf_matches_operation(jQuery('";
								
								$or_condition .= '[name="input_'.$if_condition['fieldid'].'"]';
								
								$or_condition .= "').val(), '".$if_condition['value']."', '".$if_condition['operator']."')";
							
							} else if($if_condition['operation'] == 'and'){
								
								$and_condition .= " && ";
							
								$and_condition .= "gf_matches_operation(jQuery('";
								
								$and_condition .= '[name="input_'.$if_condition['fieldid'].'"]';
								
								$and_condition .= "').val(), '".$if_condition['value']."', '".$if_condition['operator']."')";
							}
						} else {
							$or_condition .= "gf_matches_operation(jQuery('";
							$or_condition .= '[name="input_'.$if_condition['fieldid'].'"]';
							$or_condition .= "').val(), '".$if_condition['value']."', '".$if_condition['operator']."')";
						}
					}
					
					if($or_condition != ''){
						$or_condition = ltrim($or_condition, " || ");
						$javascript_con .= " ($or_condition)";
					}
					if($and_condition != ''){
						$and_condition = ltrim($and_condition, " && ");
						$javascript_con .= " && ($and_condition)";
					}
					
					$javascript_con .= " ){";
					
					$javascript_con .= "if('".$then_condition['operator']."' == 'is'){ jQuery('";
					$javascript_con .= '[name="input_'.$then_condition['fieldid'].'"]';
					$javascript_con .= "').attr('value', '".$then_condition['value']."');";
					$javascript_con .= " } else if('".$then_condition['operator']."' == 'isnot'){";
					$javascript_con .= " var entered_value = jQuery('";
					$javascript_con .= '[name="input_'.$then_condition['fieldid'].'"]';
					$javascript_con .= "').val();";
					$javascript_con .= " if(entered_value == '".$then_condition['value']."' && entered_value != ''){";
					$javascript_con .= " jQuery('";
					$javascript_con .= '[name="input_'.$then_condition['fieldid'].'"]';
					$javascript_con .= "').val('');";
					$javascript_con .= "}";
					$javascript_con .= "} else if('".$then_condition['operator']."' == '>'){ ";
					$javascript_con .= " var entered_value = jQuery('";
					$javascript_con .= '[name="input_'.$then_condition['fieldid'].'"]';
					$javascript_con .= "').val();";
					$javascript_con .= " if(entered_value < '".$then_condition['value']."' && entered_value != ''){";
					$javascript_con .= " jQuery('";
					$javascript_con .= '[name="input_'.$then_condition['fieldid'].'"]';
					$javascript_con .= "').val('');";
					$javascript_con .= "}";
					$javascript_con .= "} else if('".$then_condition['operator']."' == '<'){";
					$javascript_con .= " var entered_value = jQuery('";
					$javascript_con .= '[name="input_'.$then_condition['fieldid'].'"]';
					$javascript_con .= "').val();";
					$javascript_con .= " if(entered_value > '".$then_condition['value']."' && entered_value != ''){";
					$javascript_con .= " jQuery('";
					$javascript_con .= '[name="input_'.$then_condition['fieldid'].'"]';
					$javascript_con .= "').val('');";
					$javascript_con .= "}";
					$javascript_con .= "}";
					
					
					$javascript_con .= "}";
					
					$final_conditions[] = $javascript_con;
					
				}
				
				if (!empty($final_conditions))
					$final_conditions = json_encode($final_conditions);
				
				$custom_rule = json_encode($custom_rule);
				
				$form_id = $_POST['form_id'];
				
				$rule_data['form_id'] = $form_id;
				
				$rule_data['custom_rule'] = $custom_rule;
				
				$rule_data['custom_rule_json_format'] = $final_conditions;
				
				$result = $wpdb->get_results("select * from wp_gf_custom_conditions where form_id = $form_id");
			
				if(empty($result)) {
					$data = $wpdb->insert('wp_gf_custom_conditions',$rule_data);
				} else {
					unset($rule_data['form_id']);
					$data = $wpdb->update("wp_gf_custom_conditions",  $rule_data, array("form_id" => $form_id));
				}
				
				echo json_encode(array("success" => true));
			} else {
				echo json_encode(array("success" => false));
			}	
		} 
	  	die();
	}
	
	function getCustomConditions($form_id){
		
		global $wpdb;
		
		$custom_rules = $wpdb->get_var("select custom_rule from wp_gf_custom_conditions where form_id = $form_id");
		
		if($custom_rules){
			$custom_rules = json_decode($custom_rules, true);
			return $custom_rules;
		}
		
		return false;
	}
	
	function getCustomConditionRule($form_id, $index, $hidden = false){
		
		$form_field = GFFormsModel::get_form_meta($form_id);

		$formFields = $form_field['fields'];
		
		$custom_cond = "";
		
		if($hidden)
			$custom_cond .= '<div class="CustomRuleCloneCopy hideRule" id="index_'.$index.'">';
		else
			$custom_cond .= '<div class="gf_conditional_logic_rules_container" id="index_'.$index.'">';
			
		$custom_cond .= '<a class="add_another_rule pull-right" title="add another rule"><i class="gficon-add"></i></a>';
		
		if($index == '1')
			$custom_cond .= '<a class="delete_rule pull-right hide_icon" ';
		else
			$custom_cond .= '<a class="delete_rule pull-right" ';
		
		$custom_cond .= 'title="remove this rule" onclick="deleteCustomRule('.$index.');"><i class="gficon-subtract"></i></a>'.
				'<div class="if_rule"><p>If</p>'.
				'<select id="if_form_fields_'.$index.'" name="if_form_rules['.$index.'][fieldid]">';
		
		foreach($formFields as $formField){
		
			if(!empty($formField->inputs)){
				foreach($formField->inputs as $sub_fields){
					$custom_cond .= '<option value="'.$sub_fields['id'].'">'.$formField->label. " ".$sub_fields['label'].'</option>';
				}
			} else {
				$custom_cond .= '<option value="'.$formField->id.'">'.$formField->label.'</option>';
			}
		}
		$custom_cond .= '</select>';
	
		$custom_cond .= '<select id="if_rule_operator_'.$index.'" name="if_form_rules['.$index.'][operator]">'. 
					'<option value="is">is</option>'.
					'<option value="isnot">is not</option>'.
					'<option value=">">greater than</option>'.
					'<option value="<">less than</option>'.
				'</select>';
		
		$custom_cond .= '<input type="text" placeholder="Enter a value" id="if_rule_value_'.$index.'" name="if_form_rules['.$index.'][value]" value="" />';
		
		$custom_cond .= '<input type="button" name="add_boolean_value" value="Add Boolean Value" class="add_boolean_value" />';

		$custom_cond .= '<input type="hidden" name="if_condition_sub_rule" value ="1" />';
		
		$custom_cond .= '</div>';
		
		$custom_cond .= '<div class="then_rule"><p>Then</p><select id="then_form_fields_'.$index.'" name="then_form_fields_'.$index.'">';
		
		foreach($formFields as $formField){
		
			if(!empty($formField->inputs)){
				foreach($formField->inputs as $sub_fields){
					$custom_cond .= '<option value="'.$sub_fields['id'].'">'.$formField->label. " ".$sub_fields['label'].'</option>';
				}
			} else {
				$custom_cond .= '<option value="'.$formField->id.'">'.$formField->label.'</option>';
			}
		}
		$custom_cond .= '</select>';
		
		$custom_cond .= '<select id="then_rule_operator_'.$index.'" name="then_rule_operator_'.$index.'">'.
					'<option value="is">is</option>'.
					'<option value="isnot">is not</option>'.
					'<option value=">">greater than</option>'.
					'<option value="<">less than</option>'.
				'</select>'.
				'<input type="text" placeholder="Enter a value" id="then_rule_value_'.$index.'" name="then_rule_value_'.$index.'" value="" />'.
			'</div>';
		$custom_cond .= '</div>';
		
		return $custom_cond;
	}
	
	function temporary_save_custom_rules($form){
		
		global $wpdb;

		$custom_rules = $wpdb->get_var("select custom_rule_json_format from wp_gf_custom_conditions where form_id = ".$form['id']);
		
		if($custom_rules){
			echo '<script type="text/javascript">var gf_form_custom_conditional_rules = '.$custom_rules.'; </script>';
		}
	}
	
	function reset_custom_conditions(){
		
		global $wpdb;
		
		if(isset($_POST['formid']) && $_POST['formid'] != ''){
			$wpdb->query("update wp_gf_custom_conditions set custom_rule = NULL, custom_rule_json_format = NULL where form_id = ".$_POST['formid']);	
		}
		
		echo json_encode(array("success" => true));
	
		die();		
	}
}