jQuery(document).ready(function(){
	
	jQuery(".add_another_rule").on("click", function(e){
		
		e.stopPropagation();
		
		var newRule = jQuery('.CustomRuleCloneCopy').clone(true,true);
		
		newRule.removeClass('hideRule CustomRuleCloneCopy').addClass("gf_conditional_logic_rules_container");
		
		var newIndex = jQuery(".gf_conditional_logic_rules_container").length;
		
		newIndex = newIndex + 1;
		
		newRule = updateCurrentElementWithSequenceNumber(newRule, newIndex);
		
		jQuery("#custom_conditional_logic_container").append(newRule);
		
		updateTotalCount();
	});
	
	jQuery(".ginput_container").bind("change keyup", function(){
		
		var field_data = jQuery(this).parent("li").attr("id");
		
		field_data = field_data.split("_");
		
		var formId = field_data['1'];
		
		var fieldId = field_data['2'];
				
		if(typeof gf_form_custom_conditional_rules != 'undefined') {
			if(!jQuery.isEmptyObject(gf_form_custom_conditional_rules)){
				
				jQuery.each(gf_form_custom_conditional_rules,function(i,condition){
					eval(condition);
				});
			}
		}
	});
	
	jQuery("#gform_custom_logic_form").submit(function(e){
		e.preventDefault();

		var FormData = jQuery("#gform_custom_logic_form").serialize();

		jQuery.ajax({
	        type:'POST',
	        url : 'admin-ajax.php',
	        data: FormData,
			dataType:'json',
			success: function(data){
				if(data.success){
					location.reload();
					jQuery("#error").css("display", "none");
					jQuery("#update_dialog_success").css("display", "block");
				} else {
					jQuery("#error").css("display", "block");
					return;
				}
		    }
		});
	});
	
	jQuery(".add_boolean_value").on("click",function(e){
		
		var element = e.currentTarget;
		
		var subRuleStr = "";
		
		var Container = jQuery(element).parents(".gf_conditional_logic_rules_container");
		
		var index = Container.attr("id");
		
		index = index.split("_");
		
		index = index['1'];
		
		var SubIndex = Container.find("[name='if_condition_sub_rule']").val();
		
		if(SubIndex == '' || typeof SubIndex == 'undefined') return false;
		
		subRuleStr = "<div class='sub_condition MTB10' id='rule_id_"+SubIndex+"'>";

		subRuleStr += getIfConditionSubRule(index, SubIndex);

		subRuleStr += "</div>";
		
		jQuery(element).parent().append(subRuleStr);
		
		SubIndex++;
		
		Container.find("[name='if_condition_sub_rule']").val(SubIndex);
	});
	
	jQuery("#reset").click(function(){
		
		var form_id = jQuery(this).attr("name");
		
		form_id = form_id.split("_");
		
		form_id = form_id['1'];
		
		var FormData = {};
		
		FormData.action = "reset_custom_conditions";
		
		FormData.formid = form_id;
		
		jQuery.ajax({
	        type:'POST',
	        url : 'admin-ajax.php',
	        data: FormData,
			dataType:'json',
			success: function(data){
				if(data.success){
					jQuery(".gf_conditional_logic_rules_container").remove();
					jQuery(".add_another_rule").trigger("click");
				}
		    }
		});
		
	});
});

jQuery(document).on("click",".delete_sub_rule", function(e){
	
	var CurrentElement = e.currentTarget;
	
	var currentContainer = jQuery(CurrentElement).parents(".gf_conditional_logic_rules_container");
	
	var RuleIndex = jQuery(currentContainer).attr("id");
	
	RuleIndex = RuleIndex.split("_");
	
	RuleIndex = RuleIndex['1'];

	jQuery(CurrentElement).parent().remove();
	
	updateSubConditionsSequence(currentContainer, RuleIndex);
	
});

function updateSubConditionsSequence(container, index){
	
	var SubConditionsCount = container.find(".sub_condition").length;
	
	if(SubConditionsCount > 0){
		jQuery(".sub_condition",container).each(function(index, ele){
			var subCondContainer = jQuery(ele);
			var expectedSubRuleId = (index+1);
			var actualSubRuleId = subCondContainer.attr("id");
			actualSubRuleId = actualSubRuleId.split("_");
			actualSubRuleId = actualSubRuleId['2'];
			if(expectedSubRuleId != actualSubRuleId){
				updateSubCondition(subCondContainer, index, expectedSubRuleId, actualSubRuleId);
			}
		});
		
		SubConditionsCount++;
		
		container.find("[name='if_condition_sub_rule']").val(SubConditionsCount);
		
	}
}

function updateSubCondition(container, rule_index, current_sub_rule_id, previous_sub_rule_id){
	
	container.attr("id", "rule_id_"+current_sub_rule_id);
	
	var ruleIdsArray = new Array("if_rule_operation_", "if_form_fields_", "if_rule_operator_");
	
	for(var key in ruleIdsArray) {
		
		var actualName = expectedName = "";
		
		var elementId = ruleIdsArray[key];
		
		var actualElementId = elementId + rule_index + '_' + previous_sub_rule_id;
		
		var expectedElementId = elementId + rule_index + '_' + current_sub_rule_id;
	
		if(elementId.indexOf("if_rule_operation_") == '0'){
			actualName = "if_form_rules["+rule_index+"][if_sub_rule]["+previous_sub_rule_id+"][operation]";
			expectedName = "if_form_rules["+rule_index+"][if_sub_rule]["+current_sub_rule_id+"][operation]";
		} else if(elementId.indexOf("if_form_fields_") == '0'){
			actualName = "if_form_rules["+rule_index+"][if_sub_rule]["+previous_sub_rule_id+"][fieldid]";
			expectedName = "if_form_rules["+rule_index+"][if_sub_rule]["+current_sub_rule_id+"][fieldid]";
		} else if(elementId.indexOf("if_rule_operator_") == '0'){
			actualName = "if_form_rules["+rule_index+"][if_sub_rule]["+previous_sub_rule_id+"][operator]";
			expectedName = "if_form_rules["+rule_index+"][if_sub_rule]["+current_sub_rule_id+"][operator]";
		} else if(elementId.indexOf("if_rule_value_") == '0'){
			actualName = "if_form_rules["+rule_index+"][if_sub_rule]["+previous_sub_rule_id+"][value]";
			expectedName = "if_form_rules["+rule_index+"][if_sub_rule]["+current_sub_rule_id+"][value]";
		}
		container.find('#'+actualElementId).attr('id',expectedElementId).filter('[name="'+actualName+'"]').attr('name',expectedName);
	}
}

function getIfConditionSubRule(key, subkey){
	
	var subRuleStr = "";

	var FormFields = jQuery("#if_form_fields_0").html();
	
	var FormOperators = jQuery("#if_rule_operator_0").html();
	
	subRuleStr += '<select id="if_rule_operation_'+key+'_'+subkey+'" name="if_form_rules['+key+'][if_sub_rule]['+subkey+'][operation]"><option value="and">AND</option><option value="or">OR</option></select>';
	
	subRuleStr += '<select id="if_form_fields_'+key+'_'+subkey+'" name="if_form_rules['+key+'][if_sub_rule]['+subkey+'][fieldid]">'+FormFields+'</select>';
	
	subRuleStr += '<select id="if_rule_operator_'+key+'_'+subkey+'" name="if_form_rules['+key+'][if_sub_rule]['+subkey+'][operator]">'+FormOperators+'</select>';
	
	subRuleStr += '<input type="text" placeholder="Enter a value" id="if_rule_value_'+key+'_'+subkey+'" name="if_form_rules['+key+'][if_sub_rule]['+subkey+'][value]" value="" />';
	
	subRuleStr += '<a class="delete_sub_rule" title="Delete Sub Condition"><i class="gficon-trash"></i></a>';
	
	return subRuleStr;
}

function updateCurrentElementWithSequenceNumber(wpRow, currentSequenceNumber, previousSequenceNumber){
	
	if(typeof previousSequenceNumber == 'undefined')
		previousSequenceNumber = '0';
	
	var idFields = new Array('then_form_fields_','then_rule_operator_', 'then_rule_value_');
	
	for(var idIndex in idFields ) {
		
		var elementId = idFields[idIndex];
		
		var actualElementId = elementId + previousSequenceNumber
		
		var expectedElementId = elementId + currentSequenceNumber;
		
		wpRow.find('#'+actualElementId).attr('id',expectedElementId).filter('[name="'+actualElementId+'"]').attr('name',expectedElementId);
	}
	
	var ifRuleIds = new Array('if_form_fields_','if_rule_operator_','if_rule_value_');

	for(var index in ifRuleIds ) {
		
		var ruleIndexId = ifRuleIds[index];
		
		var actulaRuleId = ruleIndexId + previousSequenceNumber;
		
		var expectedRuleId = ruleIndexId + currentSequenceNumber;
		
		var actualName = '';
		
		var expectedName = '';
		
		if(ruleIndexId.indexOf("if_form_fields_") == '0'){
			actualName = "if_form_rules["+previousSequenceNumber+"][fieldid]";
			expectedName = "if_form_rules["+currentSequenceNumber+"][fieldid]";
		} else if(ruleIndexId.indexOf("if_rule_operator_") == '0'){
			actualName = "if_form_rules["+previousSequenceNumber+"][operator]";
			expectedName = "if_form_rules["+currentSequenceNumber+"][operator]";
		} else if(ruleIndexId.indexOf("if_rule_value_") == '0'){
			actualName = "if_form_rules["+previousSequenceNumber+"][value]";
			expectedName = "if_form_rules["+currentSequenceNumber+"][value]";
		}
		
		wpRow.find('#'+actulaRuleId).attr('id',expectedRuleId).filter('[name="'+actualName+'"]').attr('name',expectedName);
		
	}
	
	wpRow.attr("id",'index_'+currentSequenceNumber);
	
	wpRow.find(".delete_rule").attr("onclick", "deleteCustomRule("+currentSequenceNumber+");");
	
	if(currentSequenceNumber > 1){
		
		wpRow.find(".delete_rule").removeClass("hide_icon");
		
		if(jQuery("#index_1").find(".delete_rule").hasClass("hide_icon"))
			jQuery("#index_1").find(".delete_rule").removeClass("hide_icon");
	}
	return wpRow;
}

function deleteCustomRule(index){
	
	jQuery("#index_"+index).remove();
	
	updateCustomRuleElementByOrder();
	
	updateTotalCount();
}

function updateCustomRuleElementByOrder(){
	
	var CustomRuleContainer = jQuery("#custom_conditional_logic_container");
	
	if(jQuery(".gf_conditional_logic_rules_container" ,CustomRuleContainer).length == '1')
		jQuery("#index_1").find(".delete_rule").addClass("hide_icon");
	
	jQuery(".gf_conditional_logic_rules_container" ,CustomRuleContainer).each(function(index,domElement){
		
		var CustomRuleRow = jQuery(domElement);
		var expectedRowIndex = (index+1);
		var actualRuleId = CustomRuleRow.attr('id');
		actualRuleId = actualRuleId.split("_");
		actualRuleId = actualRuleId['1'];
		
		if(expectedRowIndex != actualRuleId) {
			updateCurrentElementWithSequenceNumber(CustomRuleRow, expectedRowIndex, actualRuleId);
			if(CustomRuleRow.find(".sub_condition").length > 0){
				jQuery(".sub_condition",CustomRuleRow).each(function(i, ele){
					var subCondContainer = jQuery(ele);
					var subRuleId = subCondContainer.attr("id");
					subRuleId = subRuleId.split("_");
					subRuleId = subRuleId['2'];
					updateSubConditionsRuleIndex(subCondContainer, subRuleId, expectedRowIndex, actualRuleId);
				});
			}
		}
	});	
}

function updateSubConditionsRuleIndex(wpRow, sub_rule_id, CurrentRuleIndex, OldRuleIndex){
	
	var SubRuleIds = new Array('if_rule_operation_', 'if_form_fields_','if_rule_operator_','if_rule_value_');

	for(var index in SubRuleIds ) {
		
		var ruleIndexId = SubRuleIds[index];
		
		var actulaRuleId = ruleIndexId + OldRuleIndex + '_' + sub_rule_id;
		
		var expectedRuleId = ruleIndexId + CurrentRuleIndex + '_' + sub_rule_id;
		
		var actualName = '';
		
		var expectedName = '';
		
		if(ruleIndexId.indexOf("if_rule_operation_") == '0'){
			actualName = "if_form_rules["+OldRuleIndex+"][if_sub_rule]["+sub_rule_id+"][operation]";
			expectedName = "if_form_rules["+CurrentRuleIndex+"][if_sub_rule]["+sub_rule_id+"][operation]";
		} else if(ruleIndexId.indexOf("if_form_fields_") == '0'){
			actualName = "if_form_rules["+OldRuleIndex+"][if_sub_rule]["+sub_rule_id+"][fieldid]";
			expectedName = "if_form_rules["+CurrentRuleIndex+"][if_sub_rule]["+sub_rule_id+"][fieldid]";
		} else if(ruleIndexId.indexOf("if_rule_operator_") == '0'){
			actualName = "if_form_rules["+OldRuleIndex+"][if_sub_rule]["+sub_rule_id+"][operator]";
			expectedName = "if_form_rules["+CurrentRuleIndex+"][if_sub_rule]["+sub_rule_id+"][operator]";
		} else if(ruleIndexId.indexOf("if_rule_value_") == '0'){
			actualName = "if_form_rules["+OldRuleIndex+"][if_sub_rule]["+sub_rule_id+"][value]";
			expectedName = "if_form_rules["+CurrentRuleIndex+"][if_sub_rule]["+sub_rule_id+"][value]";
		}
		
		wpRow.find('#'+actulaRuleId).attr('id',expectedRuleId).filter('[name="'+actualName+'"]').attr('name',expectedName);
		
	}
	
}

function updateTotalCount(){
	
	var totalNumOfRule = jQuery(".gf_conditional_logic_rules_container", jQuery("#custom_conditional_logic_container")).length;
	
	jQuery("#total_custom_rule").val(totalNumOfRule);
	
	if(totalNumOfRule == '1')
		jQuery("#index_1").find(".delete_rule").addClass("hide_icon");
	
}

function gf_matches_operation(val1, val2, operation){

	switch(operation){
        case "is" :
            return val1 == val2;
        break;

        case "isnot" :
            return val1 != val2;
        break;

        case ">" :
            val1 = gf_try_convert_float(val1);
            val2 = gf_try_convert_float(val2);

            return gformIsNumber(val1) && gformIsNumber(val2) ? val1 > val2 : false;
            break;

        case "<" :
            val1 = gf_try_convert_float(val1);
            val2 = gf_try_convert_float(val2);

            return gformIsNumber(val1) && gformIsNumber(val2) ? val1 < val2 : false;
            break;

    }
    return false;
}

function gf_try_convert_float(text){
    var format = window["gf_number_format"] == "decimal_comma" ? "decimal_comma" : "decimal_dot";

    if(gformIsNumeric(text, format)){
        var decimal_separator = format == "decimal_comma" ? "," : ".";
        return gformCleanNumber(text, "", "", decimal_separator);
    }

    return text;
}

function gformCleanNumber(text, symbol_right, symbol_left, decimal_separator){
    text = text + " ";

    text = text.replace(/&.*?;/, "", text);

    text = text.replace(symbol_right, "");
    text = text.replace(symbol_left, "");

    var clean_number = "";
    var is_negative = false;
    for(var i=0; i<text.length; i++){
        var digit = text.substr(i,1);
        if( (parseInt(digit) >= 0 && parseInt(digit) <= 9) || digit == decimal_separator )
            clean_number += digit;
        else if(digit == '-')
            is_negative = true;
    }

    var float_number = "";

    for(var i=0; i<clean_number.length; i++)
    {
        var char = clean_number.substr(i,1);
        if (char >= '0' && char <= '9')
            float_number += char;
        else if(char == decimal_separator){
            float_number += ".";
        }
    }

    if(is_negative)
        float_number = "-" + float_number;

    return gformIsNumber(float_number) ? parseFloat(float_number) : false;
}


function gformIsNumeric(value, number_format){

    switch(number_format){
        case "decimal_dot" :
            var r = new RegExp("^(-?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]+)?)$");
            return r.test(value);
        break;

        case "decimal_comma" :
            var r = new RegExp("^(-?[0-9]{1,3}(?:\.?[0-9]{3})*(?:,[0-9]+)?)$");
            return r.test(value);
        break;
    }
    return false;
}

function gformIsNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
