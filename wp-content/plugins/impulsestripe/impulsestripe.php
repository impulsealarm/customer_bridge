<?php
/*
Plugin Name: Impulse Stripe
Description: Integration With Stripe and Vtiger
Author: Manish Goyal
Author URI: manish@devitechnosolutions.com
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}


define( 'IMPULSE_STRIPE__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'IMPULSE_STRIPE__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( IMPULSE_STRIPE__PLUGIN_DIR . 'class.impulse-stripe.php' );

$obj = new ImpulseStripe();

//Shortcode to show the form [bankdetailform]