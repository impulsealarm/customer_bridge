<?php

class ImpulseStripe{
	
	protected $Errors;
	
	function __construct() {
	
		$this->Errors = new WP_Error();
		$this->init();
	}
	
	public function init() {
		add_shortcode('bankdetailform', array($this, 'show_form' ));
		add_action("wp_enqueue_scripts", array($this, "enqueue_script_style"));
		add_action("wp", array($this, "save_customer_payment_details"));
		
		add_action('wp_ajax_getRealterDetailFromVtiger', array($this, 'getRealterDetailFromVtiger'));	
		add_action('wp_ajax_nopriv_getRealterDetailFromVtiger', array($this, 'getRealterDetailFromVtiger'));

		add_action('wp_head', array($this,'add_my_script_in_head'));
		
	}
	
	public function enqueue_script_style(){
		//wp_enqueue_style('jquery-ui-css', IMPULSE_STRIPE__PLUGIN_URL. "css/jquery-ui.css");
		
		wp_enqueue_script("jquery-ui-js", IMPULSE_STRIPE__PLUGIN_URL ."js/jquery-ui.js", array( 'jquery' ));
		wp_enqueue_script("stripe-js", "https://js.stripe.com/v2/");
		wp_enqueue_script("custom-js", IMPULSE_STRIPE__PLUGIN_URL. 'js/main.js', array( 'jquery' ));
		wp_enqueue_script("validate-js", IMPULSE_STRIPE__PLUGIN_URL."js/jquery.validate.min.js");

		wp_enqueue_style('custom-css', IMPULSE_STRIPE__PLUGIN_URL . 'css/style.css');
		wp_enqueue_style('formsmain-min-css', IMPULSE_STRIPE__PLUGIN_URL . 'css/formsmain.min.css');
		wp_enqueue_style("jquery-ui-css" ,"https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css");
		
		
		wp_enqueue_style("loader-css", IMPULSE_STRIPE__PLUGIN_URL . "css/jquery.loader.css");
		wp_enqueue_script("loader-js", IMPULSE_STRIPE__PLUGIN_URL . "js/jquery.loader.js");
		
		wp_localize_script( 'custom-js', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ))); 
		
	}
	
	function show_form($att){
		
		$style = "";
	
		if($this->Errors->errors && $this->Errors->get_error_data("801") != '') {
			$content .= '<div class="success_message">'.$this->Errors->get_error_data("801").'</div>';
			return $content;
		}
				
		if($this->Errors->errors && $this->Errors->get_error_data("800") != '')
			$style .= 'style="display: block;"';
	
		$content = '';
	
		$content .= "<div class='gform_wrapper'>";

		$content .= "<form name='bank_details' action='' method='POST' id='bank_details' class='wpcf7-form'>";
			
		$content .= '<div class="validation_error" '.$style.'>'.$this->Errors->get_error_data("800").'</div>';
		
		$content .= "<input type='hidden' name='type' value='individual' />";
		
		$content .= '<input type="hidden" name="country" value="US" />';
		
		$content .= '<input type="hidden" name="currency" value="USD" />';
		
		$content .= "<div class='gform_body gform_page gform_page_fields'>";
		
		$content .= "<ul class='gform_fields top_label form_sublabel_below description_below'>";				
		
        $content .= '<li class="gfield">';
        
        $content .= '<label class="gfield_label">Email<span class="gfield_required">*</span></label>';
        
        $content .= '<div class="ginput_container ginput_container_email">';
                            
        $content .= "<input type='text' name='email' id='email' value='".$_POST['email']."' required>";
		               
        $content .= '</div></li>';
        
        $content .= '<li class="gfield">';
        
        $content .= '<label class="gfield_label">Date of Birth<span class="gfield_required">*</span></label>';
        
        $content .= '<div class="ginput_container ginput_container_dob">';
                            
        $content .= "<input type='text' name='dob' id='dob' value='".$_POST['dob']."' required >";
		               
        $content .= '</div></li>';
        
        $content .= '<li class="gfield">';
        
        $content .= '<label class="gfield_label">Payment Mode<span class="gfield_required">*</span></label>';
        
        $content .= '<div class="ginput_container ginput_container_radio">';
                            
        $content .= '<ul class="gfield_radio">';
        
        $content .= '<li>';

        $content .= "<input type='radio' name='payment_mode' id='bank' value='bank' class='payment_mode' ";
        
        $content .= isset($_POST['payment_mode']) && $_POST['payment_mode'] == 'bank' ? 'checked="checked"' : '';
        
        $content .= "/>";
		
        $content .= '<label>Bank Details</label>';
        
        $content .= '</li>';
        
        $content .= '<li>';
        
        $content .= '<input type="radio" name="payment_mode" id="card" value="card" class="payment_mode" ';
         
        $content .= isset($_POST['payment_mode']) && $_POST['payment_mode'] == 'card' ? 'checked="checked"':'';
        
        $content .= '/>';
		
        $content .= '<label>ATM Card Details</label>';
        
        $content .= '</li></ul></div></li>';

		$content .= "</ul></div></li>";
		
        $content .= '<li class="gfield bank_details">';
        
        $content .= '<label class="gfield_label">Bank Account Number<span class="gfield_required">*</span></label>';
        
        $content .= '<div class="ginput_container ginput_container_account_number">';
                            
        $content .= "<input type='text' name='bank_acc_no' id='bank_acc_no' value='".$_POST['bank_acc_no']."' class='ignore' required>";
		               
        $content .= '</div></li>';
        
        $content .= '<li class="gfield bank_details">';
        
        $content .= '<label class="gfield_label">Routing Number<span class="gfield_required">*</span></label>';
        
        $content .= '<div class="ginput_container ginput_container_routing_number">';
        
		$content .= "<input type='text' name='routing_no' id='routing_no' value='".$_POST['routing_no']."' class='ignore' required>";
		               
        $content .= '</div></li>';
        
        $content .= '<li class="gfield card_details">';
        
        $content .= '<label class="gfield_label">Card Number<span class="gfield_required">*</span></label>';
    
        $content .= '<div class="ginput_container ginput_container_number">';
                            
        $content .= "<input type='text' name='card_number' id='card_number' value='".$_POST['card_number']."' class='ignore' required>";
		               
        $content .= '</div></li>';
        
        $content .= '<li class="gfield card_details">';
        
        $content .= '<label class="gfield_label">CVC<span class="gfield_required">*</span></label>';
    
        $content .= '<div class="ginput_container ginput_container_cvc">';
                            
        $content .= "<input type='text' name='card_cvc' id='card_cvc' value='".$_POST['card_cvc']."' class='ignore' required>";
		               
        $content .= '</div></li>';

        $content .= '<li class="gfield card_details">';
        
        $content .= '<label class="gfield_label">Expiration<span class="gfield_required">*</span></label>';
    
        $content .= '<div class="ginput_container ginput_container_exp">';
                            
        $content .= "<input type='text' name='card_exp' id='card_exp' value='".$_POST['card_exp']."' class='ignore' required>";
		               
        $content .= '</div></li>';

        $content .= '<li class="gfield">';
        
        $content .= '<div class="ginput_container ginput_container_checkbox">';
        
        $content .= '<ul class="gfield_checkbox">';
        
        $content .= '<li>';

        $content .= '<input type="checkbox" name="tos_acceptance" id="tos_acceptance" required ';
        
        $content .= isset($_POST['tos_acceptance']) && $_POST['tos_acceptance'] != '' ? 'checked="checked"':'';
        
        $content .= ' />';
		
        $content .= '<label>I agree to terms and conditions of <a target = "_blank" href = "https://stripe.com/connect/terms">Stripe Connect Platform Agreement</a></label>';
        
        $content .= '</li></ul></div></li>';

		$content .= "</ul></div>";
		
		$content .= '<div class="gform_page_footer">';
		 
		$content .= '<input type="submit" name="submit_bank_detail_form" id="submit_bank_detail_form" class="gform_next_button button" value="Submit">';

		$content .= '</div>';
		
		$content .= '<div class="gform_page_footer" style = "text-align:center">';
		 
		$content .= '<img src = "https://impulsealarm.com/wp-content/uploads/solid.png"/>';

		$content .= '</div>';
		
		$content .= '<div class="gform_page_footer" style = "text-align:center">';
		 
		$content .= '<script type="text/javascript">TrustLogo("https://impulsealarm.com/wp-content/uploads/comodo_secure_113x59_white.png", "SCAS", "topright");</script>';

		$content .= '</div>';
		
		
		$content .= "</form>";

		$content .= '';
		$content .= '<script>';
		$content .= 'jQuery(document).ready(function($){';
		$content .= '	jQuery( "#submit_bank_detail_form" ).click(function() {';
		$content .= '		var email = $("#email").val();';
		$content .= '		var dob = $("#dob").val();';
		$content .= '		var method = $("input[name=payment_mode]:checked").val();';
		$content .= '		if (method == "bank") {';
		$content .= '			var bankNo = $("#bank_acc_no").val();';
		$content .= '			var routeNo = $("#routing_no").val();';
		$content .= '			woopra.track("payment_method_bank", {';
		$content .= '            	email: email,';
		$content .= '            	dob: dob,';
		$content .= '            	method: method,';
		$content .= '            	bank_no: bankNo,';
		$content .= '            	route_no: routeNo';
		$content .= '        	});';
		$content .= '		} else {';
		$content .= '			var cardNo = $("#card_number").val();';
		$content .= '			var exp = $("#card_exp").val();';
		$content .= '			woopra.track("payment_method_card", {';
		$content .= '            	email: email,';
		$content .= '            	dob: dob,';
		$content .= '            	method: method,';
		$content .= '            	card_no: cardNo,';
		$content .= '            	exp: exp';
		$content .= '        	});';
		$content .= '		}';
        $content .= '    });';
		$content .= '});';
		$content .= '</script>';
		
		return $content;
	}
	
	function save_customer_payment_details(){
		
		$data = $stripeError = array();
		
		if(
			isset($_POST['type']) && $_POST['type'] == 'individual' &&
			isset($_POST['country']) && $_POST['country'] == 'US' && 
			isset($_POST['currency']) && $_POST['currency'] == 'USD'
		) {
		
			require_once("includes/functions.php");

			$data = $_POST;
			
			$data['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
			
			$url = "https://crm.impulsealarm.com";
			$username = "admin";
			$accessKey = "du03iX8ngOe4lJN";
			
			$ws_url =  $url . '/webservice.php';
		   
			$loginObj = login($ws_url, $username, $accessKey);
		
			$sessionName = $loginObj->sessionName;
		
			$postParams = array(
				'operation' => 'update_stripe_account',
				'sessionName' => $sessionName,
			    'element' => json_encode($data)
			);
		
			$response = postHttpRequest($ws_url, $postParams);
			
			$response = json_decode($response, true);
			
			if(!empty($response['result'])){
				
				$result = $response['result'];
				
				if(isset($result['error']) && !empty($result['result'])){
					
					$error_result = $result['result'];
					
					$stripeError['code'] = "800";
			
					$stripeError['message'] = $error_result['type'];
					
					$stripeError['data'] = $error_result['message'];
					
				} else {
					
					$this->Errors->remove("801");
					$this->Errors->add("801", "success", "Thank you. Your Form is Successfully Submitted.");
				}
			}
			
			if(!empty($stripeError)){
				$this->Errors->remove("800");
				$this->Errors->add($stripeError['code'],$stripeError['message'], $stripeError['data']);
			}
		}		
	}
	
	function getRealterDetailFromVtiger(){
		
		if($_POST['email']){
			$response = $this->getDetailFromVtiger($_POST['email']);
			echo $response;
		}
		wp_die();
	}
	
	function getDetailFromVtiger($email){
		
		require_once("includes/functions.php");
		
		$url = "https://crm.impulsealarm.com";
		
		$user_name = "admin";
		
		$accesskey = "du03iX8ngOe4lJN";
				
		$ws_url =  $url . '/webservice.php';
		
		$loginObj = login($ws_url, $user_name, $accesskey);
		
		$sessionName = $loginObj->sessionName;
		
		$element = array("email" => $email);
		
		$postParams = array(
			'operation'=>'retrieve_stripe_account',
			'sessionName'=>$sessionName,
			'element'=>json_encode($element)
		);
		
		$response = postHttpRequest($ws_url, $postParams);
		
		return $response;
	}
	
	function add_my_script_in_head(){
	?>

		<script language="JavaScript" src="https://secure.comodo.net/trustlogo/javascript/trustlogo.js" type="text/javascript"></script>

	<?php
	}
	
}